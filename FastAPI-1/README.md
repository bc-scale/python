<h1>FastAPI: Fundamentos, Path Operations y Validaciones</h1>

<h3>Facundo García Martoni</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [Bienvenido al desarrollo backend con Python](#bienvenido-al-desarrollo-backend-con-python)
- [2. Fundamentos de FastAPI](#2-fundamentos-de-fastapi)
  - [¿Qué es FastAPI?](#qué-es-fastapi)
  - [Ubicación de FastAPI en el ecosistema de Python](#ubicación-de-fastapi-en-el-ecosistema-de-python)
  - [Hello World: creación del entorno de desarrollo](#hello-world-creación-del-entorno-de-desarrollo)
  - [Hello World: elaborando el código de nuestra primer API](#hello-world-elaborando-el-código-de-nuestra-primer-api)
  - [Documentación interactiva de una API](#documentación-interactiva-de-una-api)
- [3. Desarmando el framework](#3-desarmando-el-framework)
  - [Path Operations](#path-operations)
  - [Path Parameters](#path-parameters)
  - [Query Parameters](#query-parameters)
  - [Request Body y Response Body](#request-body-y-response-body)
  - [Models](#models)
- [4. Validaciones](#4-validaciones)
  - [Validaciones: Query Parameters](#validaciones-query-parameters)
  - [Validaciones: explorando más parameters](#validaciones-explorando-más-parameters)
  - [Validaciones: Path Parameters](#validaciones-path-parameters)
  - [Validaciones: Request Body](#validaciones-request-body)
  - [Validaciones: Models](#validaciones-models)
  - [Tipos de datos especiales](#tipos-de-datos-especiales)
- [5. Examples](#5-examples)
  - [Creando ejemplos de Request Body automáticos](#creando-ejemplos-de-request-body-automáticos)
  - [Creando ejemplos de Path y Query parameters automáticos](#creando-ejemplos-de-path-y-query-parameters-automáticos)
- [6. Conclusiones](#6-conclusiones)
  - [Continua aprendiendo](#continua-aprendiendo)


# 1. Introducción

## Bienvenido al desarrollo backend con Python

[Slide-fastAPI.pdf](https://drive.google.com/file/d/1ftWeXnLqXMh_UhY9yZqOuplSY32isirw/view?usp=sharing)

[Diagrama](https://drive.google.com/file/d/1gShMWIWUrXZpk2lDDfGuVvvTJBiNID3Q/view?usp=sharing)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/curso-fastapi-fundamentos-path-validaciones](https://github.com/platzi/curso-fastapi-fundamentos-path-validaciones)

# 2. Fundamentos de FastAPI

## ¿Qué es FastAPI?

El framework mas veloz para el desarrollo web con Python. Enfocado para realizar APIs, es el mas rápido en lo que respecta a la velocidad del servidor superando a Node.Js y a GO. Fue creado por [Sebastian Ramirez](https://twitter.com/tiangolo), es de código abierto y se encuentra en Github, es usado por empresas como Uber, Windows, Netflix y Office.

[FastAPI](https://fastapi.tiangolo.com/)

## Ubicación de FastAPI en el ecosistema de Python

FastAPI utiliza otros frameworks dentro de si para funcionar

- Uvicorn: es una librería de Python que funciona de servidor, es decir, permite que cualquier computadora se convierta en un servidor
- Starlette: es un framework de desarrollo web de bajo nivel, para desarrollar aplicaciones con este requieres un amplio conocimiento de Python, entonces FastAPI se encarga de añadirle funcionalidades por encima para que se pueda usar mas fácilmente
- Pydantic: Es un framework que permite trabajar con datos similar a pandas, pero este te permite usar modelos los cuales aprovechara FastAPI para crear la API.

> FastAPI utiliza:
>
> - Uvicorn: Es una librería de Python que funciona como servidor.
> - Starlette: Es un framework de desarrollo web de bajo nivel.
> - Pydantic: Es un framework que permite trabajar con datos. Te permite definir modelos.

[![img](https://www.google.com/s2/favicons?domain=https://www.uvicorn.org/assets/images/favicon.png)Uvicorn](https://www.uvicorn.org/)

[![img](https://www.google.com/s2/favicons?domain=https://www.starlette.io/assets/images/favicon.png)Starlette](https://www.starlette.io/)

[![img](https://www.google.com/s2/favicons?domain=https://pydantic-docs.helpmanual.io/favicon.png)pydantic](https://pydantic-docs.helpmanual.io/)

## Hello World: creación del entorno de desarrollo

Inicia como cualquier otro, creando la carpeta y el entorno virtual para el proyecto e instalas fastapi y uvicorn usando pip

```bash
$ pip install fastapi uvicorn
```

- Crear la carpeta del proyecto

```sh
mkdir fast-api-hello-world
```

- Movernos a la carpeta del proyecto

```sh
cd fast-api-hello-world
```

- Crear el entorno virtual

```sh
py -m venv venv
```

- Comprobar que se creo el entorno virtual

```sh
ls
```

- Activar el entorno virtual

```sh
source venv/bin/activate
```

- Instalar el framework

```sh
pip install fastapi uvicorn
```

[![img](https://www.google.com/s2/favicons?domain=https://fastapi.tiangolo.com/tutorial/first-steps/../../img/favicon.png)First Steps - FastAPI](https://fastapi.tiangolo.com/tutorial/first-steps/)

## Hello World: elaborando el código de nuestra primer API

Utilizando el tipado estático:

```python
#  Dependencias.
from typing import Dict
from fastapi import FastAPI;

#  Instancia de la clase.
mi_app:FastAPI = FastAPI();

#  Path Operator Decoration.
@mi_app.get("/")
def home() -> Dict:
   #  Return JSON.
   return {"Hello": "World"};
```

Comando para iniciar la aplicación:

```sh
uvicorn main:app --reload
```

Hay que aclarar que el parámetro **–reload** se especifica con el fin de que cada ves que ocurra un cambio dentro del código el servidor se reinicia automático.

```python
from fastapi import FastAPI

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}
```

```sh
uvicorn main:app --reload

INFO:     Uvicorn running on https://localhost:8000 (Press CTRL+C to quit)
INFO:     Started reloader process [28720]
INFO:     Started server process [28722]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
```



## Documentación interactiva de una API

FastAPI también está parado sobre los hombros de OpenAPI, el cual es un conjunto de reglas que permite definir cómo describir, crear y visualizar APIs. Es un conjunto de reglas que permiten decir que una API está bien definida.
ㅤ
OpenAPI necesita de un software, el cual es Swagger, que es un conjunto de softwares que permiten trabajar con APIs. FastAPI funciona sobre un programa de Swagger el cual es Swagger UI, que permite mostrar la API documentada.
ㅤ
Acceder a la documentación interactiva con Swagger UI:
`{localhost}/docs`
ㅤ
Acceder a la documentación interactiva con Redoc:
`{localhost}/redoc`

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)https://www.openapis.org/](https://www.openapis.org/)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - Redocly/redoc: 📘 OpenAPI/Swagger-generated API Reference Documentation](https://github.com/Redocly/redoc)

# 3. Desarmando el framework

## Path Operations

Un path es lo mismo que un route o endpoints y es todo aquello que vaya después de nuestro dominio a la derecha del mismo.

¿Que son las operations?
Un operations es exactamente lo mismo que un método http y tenemos las siguientes más populares: GET, POST, PUT y DELETE

Y otros métodos como OPTIONS, HEAD, PATCH …

**Metodos**:
• Get: Obtener
• Put: Modificar/Actualizar
• Patch: Modificar/Actualizar
• Post: Crear
• Delete: Eliminar

![img](https://rain-scabiosa-74f.notion.site/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2Fbd055c63-2966-4b9b-9ec4-da8ddf946ac2%2FUntitled.png?table=block&id=4aa09253-ca21-4ef8-b0c7-75301ada97ba&spaceId=d65016d5-4f34-47fe-becd-2bd00e2a9b8c&width=1890&userId=&cache=v2)

## Path Parameters

Los parámetros de ruta son partes variables de una ruta URL . Por lo general, se utilizan para señalar un recurso específico dentro de una colección, como un usuario identificado por ID. Una URL puede tener varios parámetros de ruta.

> Un path parameter es una variable incluida en la ruta url, con la cual especificamos la espera de un cierto dato para el envió hacia dicha ruta, una url puede contener varios path parameters

![img](https://rain-scabiosa-74f.notion.site/image/https%3A%2F%2Fi.imgur.com%2FqOIpgQn.jpg?table=block&id=34824eb6-307e-46c5-9275-3dc2eac0ccb5&spaceId=d65016d5-4f34-47fe-becd-2bd00e2a9b8c&width=1890&userId=&cache=v2)

![platzi.PNG](https://static.platzi.com/media/user_upload/platzi-90ae7913-9c54-4b47-9cf4-1fa2f5e9d181.jpg)

## Query Parameters

Un Query Patameter es un conjunto de elementos opcionales los cuales son añadidos al finalizar la ruta, con el objetivo de definir contenido o acciones en la url,
estos elementos se añaden despues de un ?
para agregar más query parameters utilizamos &

> **Query parameters:** son un conjunto definido de parámetros adjuntos al final de una URL . Son extensiones de la URL que se utilizan para ayudar a definir contenido o acciones específicos en función de los datos que se transmiten.
>
> [**Documentación Oficial:**](https://fastapi.tiangolo.com/tutorial/query-params/)

![img](https://rain-scabiosa-74f.notion.site/image/https%3A%2F%2Fi.imgur.com%2F8abmbDS.jpg?table=block&id=1beb8f89-57c7-49f3-990e-447d80a854d4&spaceId=d65016d5-4f34-47fe-becd-2bd00e2a9b8c&width=1890&userId=&cache=v2)

## Request Body y Response Body

Debes saber que bajo el protocolo **HTTP** existe una comunicación entre el usuario y el servidor. Esta comunicación está compuesta por cabeceras (**headers**) y un cuerpo (**body**). Por lo mismo, se tienen dos direcciones en la comunicación entre el cliente y el servidor y definen de la siguiente manera:

- **Request** : Cuando el cliente solicita/pide datos al servidor.
- **Response** : Cuando el servidor responde al cliente.

#### Request Body

Con lo anterior mencionado, **Request Body** viene a ser el cuerpo (**body**) de una **solicitud** del cliente al servidor.
`\`

#### Response Body

Con lo anterior mencionado, **Response Body** viene a ser el cuerpo (**body**) de una **respuesta** del servidor al cliente.

Un **Request Body** son datos enviados por el cliente a su API.

Un **Response Body** son los datos que su API envía al cliente.

![img](https://rain-scabiosa-74f.notion.site/image/https%3A%2F%2Fi.imgur.com%2FjZMtBoQ.jpg?table=block&id=bfaed536-c381-4840-81d2-6aa437970d8a&spaceId=d65016d5-4f34-47fe-becd-2bd00e2a9b8c&width=1890&userId=&cache=v2)

## Models

**Documentacion Oficial:** https://fastapi.tiangolo.com/tutorial/sql-databases/

Un modelo es la representacion de una entidad en codigo, al menos de una manera descriptiva.

**¿Como luce un modelo dentro de FastAPI?**

Modelo pydantic para validar datos:

```
from typing import List, Optional

from pydantic import BaseModel


class ItemBase(BaseModel):
    title: str
    description: Optional[str] = None


class ItemCreate(ItemBase):
    pass


class Item(ItemBase):
    id: int
    owner_id: int

    class Config:
        orm_mode = True


class UserBase(BaseModel):
    email: str


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    is_active: bool
    items: List[Item] = []

    class Config:
        orm_mode = True
```

Un modelo para mapear los datos a la base de datos (ORM, SQLalchemy)

```python
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from .database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)

    items = relationship("Item", back_populates="owner")


class Item(Base):
    __tablename__ = "items"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    description = Column(String, index=True)
    owner_id = Column(Integer, ForeignKey("users.id"))

    owner = relationship("User", back_populates="items")
```

# 4. Validaciones

## Validaciones: Query Parameters

```python
from typing import Optional

from fastapi import FastAPI
from fastapi import Query

app = FastApi()

@app.get('/person/detail')
def show_person(
    name: Optional[str] = Query(
        default=None,
        min_length=1,
        max_length=50
    ),

    # El ... para hacerlo obligatorio, no recomendado en un Query parameter
    age: int = Query(...)
):
    '''
    Funcion para probar la validacion en los query parameter.
    El Age esta obligatorio, no se recomienda hacer esto, si necesitas un
    parametro obligatorio se recomienda hacerlo en un path parameter
    '''
    return {name: age}
```

Para validar strings:

- max_length
- min_length
- regex

Para validar números:

- ge - greater or equal than >= (Mayor igual que)
- le - less or equal than <= (Menor igual que)
- gt - greater than > (Mayor)
- lt - less than < (Menor)

## Validaciones

Las validaciones tal como se definen, nos sirven para comprobar si son correctos los parámetros entregados en cada una de las peticiones. Estas validaciones funcionan restringiendo o indicando el formato de entrega en cada una de las peticiones.

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/curso-fastapi-fundamentos-path-validaciones at validations_query](https://github.com/platzi/curso-fastapi-fundamentos-path-validaciones/tree/validations_query)

[![img](https://www.google.com/s2/favicons?domain=https://ssl.gstatic.com/images/branding/product/1x/drive_2020q4_32dp.png)12. Validaciones_ Query Parameters.pdf - Google Drive](https://drive.google.com/file/d/1CS0Kl8tJbVgPKCdise58Q4ZE4dEcy1q3/view?usp=sharing)

## Validaciones: explorando más parameters

### Query parameters 😎

Entonces, se definen las validaciones para las Query Parameters para definir un estándar de consulta y especificar cómo se deben entregar los datos.

**EJ:**

Se define la siguiente consulta (QUERY):

**https://`domain.com`/`user`/`detail?nombre=Manolo&edad=20`**

Como validaciones tendríamos que limitar el tamaño de caracteres que puede tener el atributo “nombre” (**50 > largo > 1**), además de limitar los valores para “edad” (**300 > edad > 0**).

```python
# Validaciones.
@mi_app.get("/user/detail") # Ruta para realizar la consulta.
def mostrar_usuario(
	#Opcional.
	nombre: Optional[str] = Query(None, min_length=1, max_length=50),
	# Obligatorio.
	edad: int = Query(...)
   ):

   return {nombre: edad};
```

Para validar strings:
max_length
min_length
regex

Para validar números
ge - greater or equal than >= (Mayor igual que)
le - less or equal than <= (Menor igual que)
gt - greater than > (Mayor)
lt - less than < (Menor)

### Más sobre validaciones

Para especificar las validaciones, debemos pasarle como parámetros a la función **Query** lo que necesitemos validar.

Para tipos de datos **`str`**:

- **max_length** *: Para especificar el tamaño máximo de la cadena.*
- **min_length** *: Para especificar el tamaño minimo de la cadena.*
- **regex** *: Para especificar expresiones regulares.*


Para tipos de datos **`int`**:

- **ge** *: (greater or equal than **≥**) Para especificar que el valor debe ser mayor o igual.*

- **le** *: (less or equal than **≤**) Para especificar que el valor debe ser menor o igual.*

- **gt** *: (greater than **>**) Para especificar que el valor debe ser mayor.*

- **lt** *: (less than **<**) Para especificar que el valor debe ser menor.*

  **Ejemplo de uso:**

```python
# Validaciones de un nombre de usuario.
Query(None, min_length=1, max_length=50)):
```


Es posible dotar de mayor contexto a nuestra documentación. Se deben usar los parámetros **title** y **description**.

- **title** *: Para definir un título al parámetro.*
- **description** *: Para especificar una descripción al parámetro.*


**Ejemplo de uso:**

```python
# Validaciones para un Identificador.
Query(
	None, 
	title="ID del usuario", 
	description="El ID se consigue entrando a las configuraciones del perfil");
```

> To Strings:

```sh
min_length
max_length
```

> To Numbers:

```python
ge greather or equal than
le less or equal than
gt greather than
lt lesser than
```

[![img](https://www.google.com/s2/favicons?domain=https://fastapi.tiangolo.com/tutorial/query-params-str-validations../../img/favicon.png)Query Parameters and String Validations - FastAPI](https://fastapi.tiangolo.com/tutorial/query-params-str-validations)

[regex](https://www.rexegg.com/regex-quickstart.html)

## Validaciones: Path Parameters

Here we have how it’s work path parameters in code

```python
...
from fastapi import Path
...
@app.get("/person/detail/{person_id}")
def show_person(
	title: "Person",
	description: "Showing person with id",
	person_id: int = Path(ge=0)
):
	return {person_id: "It exist!"}
```

[Documentación Oficial](https://fastapi.tiangolo.com/tutorial/path-params/#order-matters)

## Validaciones: Request Body

Usar la built-in funcion dict() en lugar del metodo .dict() (el cual es propio de la clase BaseModel si no estoy mal). Esto se debe a que los programadores de Pydantic se ocuparon de que hacerle override al metodo __ dict __

```python
@app.put('/person/{person_id}')
def update_person(
    person_id: int = Path(
        ...,
        ge=1,
        title='Person id',
        description='Id of the person you want to update'
    ),
    person: Person = Body(...),
    location: Location = Body(...)
):
    result = dict(person)
    result.update(dict(location))

    return result
```

## Validaciones: Models

#### Diferencia Path, Query Parameters and Request Body

usamos **Path Parameters** cuando por ejemplo se trata de un id y esas cosas, como una variable etc, usamos los **Requests Body** para enviar información que tiene formato de un modelo y usamos los **Query Parameters** para solicitar infomación opcional del servidor.

### Validaciones Models

Para validar modelos tomamos uso de la clase de Pydantic Field, que funciona igual a las validaciones que ya hemos hecho con Path, Query y Body

```python
from pydantic import BaseModel, Field

class Person(BaseModel):
    first_name: str = Field(
        ...,
        min_length=1,
        max_length=50,
    )
    last_name: str = Field(
        ...,
        min_length=1,
        max_length=50,
    )
    age: int = Field(
        ...,
        gt=0,
        le=110
    )
    hair_color: Optional[HairColor] = Field(default=None)
    is_married: Optional[bool] = Field(default=None)
```

Puedes ver que en hair_color ya tenemos un tipo distinto a String esto lo hacemos para validar que tenga un campo permitido, haciendo otra clase usando Enum

```python
from enum import Enum

class HairColor(Enum):
    white: str = 'white'
    black: str = 'black'
    brown: str = 'brown'
    red: str = 'red'
    blonde: str = 'blonde'
    tinted: str = 'tinted'
```

Aquí tenemos enumerados algunos colores de pelo y ahora cada vez que alguien trate de ingresar un valor que no se encuentra en nuestra clase que hereda de Enum, le arrojará un error 422 Unprocessable Entity con el siguiente mensaje

```json
{
  "detail": [
    {
      "loc": [
        "body",
        "person",
        "hair_color"
      ],
      "msg": "value is not a valid enumeration member; permitted: 'white', 'black', 'brown', 'red', 'blonde', 'tinted'",
      "type": "type_error.enum",
      "ctx": {
        "enum_values": [
          "white",
          "black",
          "brown",
          "red",
          "blonde",
          "tinted"
        ]
      }
    }
  ]
}
```

**Codigo**

```python
# Comando para inicializar la api  uvicorn main:app --reload
# --reload es un comando que nos permite visualizar los cambios en la api de forma automatica, sin la necesidad de recargar
# Python
from typing import Optional
from enum import Enum

# Pydantic
from pydantic import BaseModel, Field

# FastAPI 
from fastapi import FastAPI
from fastapi import Body, Query, Path

app = FastAPI() # Almacenamos nuestro aplicativo en esta variable instanciando FastAPI

# Models
# Modelo de especificación de colores de cabello
class HairColor(Enum):
    white = 'white'
    black = 'black'
    brown = 'brown'
    blonde = 'blonde'


class Location(BaseModel):
    city: str
    state: str
    contry: str


class Person(BaseModel):
    first_name: str = Field(
        ...,
        min_length=1,
        max_length=50
    )
    last_name: str = Field(
        ...,
        min_length=1,
        max_length=50
    )
    age: int = Field(
        ...,
        gt=8,
        le=110
    )
    hair_color: Optional[HairColor] = Field(default=None)
    is_married: Optional[bool] = Field(default=None)


@app.get('/')
def home():
    return {'Hello':'World'} # Retornamos un valor JSON


# Request and Response body
@app.post('/person/new')
def create_person(person: Person = Body(...)):
    return person


# Validaciones: Query Parameters
@app.get('/person/detail')
def show_person(
    # Especificamos un limite de longitud para el ingreso de parametros
    name: Optional[str] = Query(
        None,
        min_length = 1,
        max_length = 40,
        title = 'Person Name',
        description = 'This is the person name. It\'s between 1 and 50 characters'
    ), 
    # No es recomendable, Los query parameteres deberian ser opcionales, pero en algun caso de la siguiente manera se puede hacer obligatorio
    age: str = Query(
        ...,
        title = 'Person Age',
        description = 'This is the person age. It\'s required'
    )
):
    return {name: age}


# Validaciones: Path Parameters
@app.get('/person/detail/{person_id}')
def show_person(
    person_id: int = Path(
        ...,
        gt=0,
        title = 'Person Id',
        description = 'This is the person Id, It\'s required'
    )
):
    return {person_id: 'It exists!'}


# Validaciones: Request body
@app.put('/person/{person_id}')
def update_person(
    person_id: int = Path(
        ...,
        gt = 0,
        title = 'Person Id',
        description = 'This is the person ID'
    ),
    # Combinacion de dos request body
    person: Person = Body(...),
    location: Location = Body(...)
):
    # Para retornar el valor de dos request body necesitamos convertir de formato json a diccionario e imprimirlos ahora si
    results = person.dict()
    results.update(location.dict())
    return results
```

## Tipos de datos especiales

[Field Types](https://pydantic-docs.helpmanual.io/usage/types/#pydantic-types)

### Clásicos

- str → Texto
- int → Enteros
- float → decimales
- bool → Booleanos

### Exóticos

- Enum → Valores que se encuentren en una lista enumerada
- HttpUrl → un URL con los siguentes argumentos - `strip_whitespace: bool = True` - `min_length: int = 1` - `max_length: int = 2 ** 16` - `tld_required: bool = True` - `allowed_schemes: Optional[Set[str]] = None`  , por ejemplo https://example.com o [www.example.com](http://www.example.com)
- FilePath → ruta absoluta a un archivo, ej: C:\Users\58412\Downloads\example.pdf
- DirectoryPath → ruta absoluta a un directorio, ej: C:\Users\58412\Downloads
- EmailStr → un correo electrónico, ej: [example@gmail.com](mailto:example@gmail.com)
- PaymentCardNumber → valida pagos por tarjetas de crédito o débito
- IPvAnyAddress → validar si nos ingresan una dirección IP
- NegativeFloat → valida si nos ingresa un flotante negativo
- PositiveFloat →  valida si nos ingresa un flotante positivo
- NegativeInt → valida si nos ingresa un entero negativo
- PositiveInt → valida si nos ingresa un entero positivo

## Reto

Tienda online. Co  dato “exótico”.

### Código

```python
"""
Program: Reto 1; uso de tipos de datos exóticos.

Description: Simulación de endpoint encargado de registrar una compra en una tienda online.

Author: Jose Noriega <josenoriega723@gmail.com>

Last Update: 2021-10-26 

"""

import re
from datetime import date
from typing import Dict
from typing import Any

# Pydantic
from pydantic import BaseModel
from pydantic import Field
from pydantic import EmailStr
from pydantic import PaymentCardNumber
from pydantic.validators import str_validator
from pydantic.types import PaymentCardBrand

# Fast API
from fastapi import FastAPI
from fastapi import Body


app = FastAPI()

PHONE_REGEXP = re.compile(r'^\+?[0-9]{1,3}?[0-9]{6,14}$')

# Custom Types


class PhoneNumber(str):
    """Phone number type"""

    @classmethod
    def __get_validators__(cls) -> Dict[str, Any]:
        yield str_validator
        yield cls.validate

    @classmethod
    def __modify_schema__(cls, field_schema: Dict[str, Any]) -> None:
        field_schema.update(
            pattern=r'^\+?[0-9]+$',
            example=['+541112345678'],
            format='phone-number',
        )

    @classmethod
    def validate(cls, value: str) -> str:
        if not isinstance(value, str):
            raise TypeError('Phone number must be a string')

        match = PHONE_REGEXP.search(value)

        if not match:
            raise ValueError('Phone number must be a valid phone number')

        return value

    def __repr__(self) -> str:
        return f'PhoneNumber({super().__repr__()})'


# Models


class Person(BaseModel):
    name: str = Field(...,
                      min_length=2,
                      max_length=50,
                      title='Name',
                      description='The name of the person that will receive the package.',
                      example='John Doe')

    email: EmailStr = Field(...,
                            title='Email',
                            description='The email of the person that will receive the package.')

    phone: PhoneNumber = Field(...,
                               title='Phone',
                               description='The phone number of the person that will receive the package.')


class Product(BaseModel):
    """Product model"""

    name: str = Field(...,
                      min_length=2,
                      max_length=50,
                      title='Name',
                      description='The name of the product.',
                      example='Laptop')


class PaymentMethod(BaseModel):
    """Payment method model"""

    card_number: PaymentCardNumber = Field(...,
                                      title='Number',
                                      description='The number of the payment card.',
                                      example='1234567890123456')

    expiration_month: int = Field(...,
                                  title='Expiration month',
                                  description='The expiration month of the payment card.',
                                  ge=1,
                                  le=12,
                                  example=12)

    expiration_year: int = Field(...,
                                 title='Expiration year',
                                 description='The expiration year of the payment card.')

    @property
    def brand(self) -> PaymentCardBrand:
        """Returns the brand of the payment card"""
        return self.card_number.brand

    @property
    def expired(self) -> bool:
        """Returns if the payment card is expired"""

        today = date.today()
        expiration_date = date(year=self.expiration_year,
                               month=self.expiration_month,
                               day=1)

        return today > expiration_date


class Address(BaseModel):
    """Address model"""

    street: str = Field(...,
                        min_length=2,
                        max_length=50,
                        title='Street',
                        description='The street of the address.')

    city: str = Field(...,
                      min_length=2,
                      max_length=50,
                      title='City',
                      description='The city of the address.')

    country: str = Field(...,
                         min_length=2,
                         max_length=50,
                         title='Country',
                         description='The country of the address.')

# Endpoints

@app.post('/order')
def add_order(
    person: Person = Body(...,),
    product: Product = Body(...,),
    address: Address = Body(...,),
    payment_method: PaymentMethod = Body(...,),
):
    """Registers a new order"""

    return {
        'person': person,
        'product': product,
        'address': address,
        'payment_method': {
            'brand': payment_method.brand,
            'last4': payment_method.card_number.last4,
            'mask': payment_method.card_number.masked,
            'expired': payment_method.expired,
        }
    }
```

### Request Body

```json
{
    "person": {
        "name": "John Doe",
        "email": "john@emai.com",
        "phone": "+52638107905"
    },
    "address": {
        "street": "Street",
        "city": "City",
        "country": "Country"
    },
    "product": {
        "name": "Macbook Pro"
    },
    "payment_method": {
        "card_number":"4242424242424242",
        "expiration_month": 12,
        "expiration_year": 2050
    }
 
}
```

### Response Body

```json
{
    "person": {
        "name": "John Doe",
        "email": "john@emai.com",
        "phone": "+52638107905"
    },
    "product": {
        "name": "Macbook Pro"
    },
    "address": {
        "street": "Street",
        "city": "City",
        "country": "Country"
    },
    "payment_method": {
        "brand": "Visa",
        "last4": "4242",
        "mask": "424242******4242",
        "expired": false
    }
}
```

[![img](https://www.google.com/s2/favicons?domain=https://pydantic-docs.helpmanual.io/usage/types/#pydantic-types../../favicon.png)Field Types - pydantic](https://pydantic-docs.helpmanual.io/usage/types/#pydantic-types)

# 5. Examples

## Creando ejemplos de Request Body automáticos

```python
#Python
from typing import Optional
from enum import Enum

#Pydantic
from pydantic import BaseModel
from pydantic import Field
from pydantic.networks import EmailStr

#FastAPI
from fastapi import FastAPI
from fastapi import Body, Query, Path

app = FastAPI()
```



## Models

```python
class HairColor(Enum):
white = "white"
brown = "brown"
black = "black"
blonde = "blonde"
red = “red”

class Location(BaseModel):
city: str = Field(
…,
min_length=1,
max_length=50,
example=“Puerto Montt”
)
state: str = Field(
…,
min_length=1,
max_length=50,
example=“Los Lagos”
)
country: str = Field(
…,
min_length=1,
max_length=50,
example=“Chile”
)

class Person(BaseModel):
first_name: str = Field(
…,
min_length=1,
max_length=50,
example=“Giocrisrai”
)
last_name: str = Field(
…,
min_length=1,
max_length=50,
example=“Godoy”
)
age: int = Field(
…,
gt=0,
le=115,
example=30
)
hair_color: Optional[HairColor] = Field(
default=None,
example=HairColor.black
)
is_married: Optional[bool] = Field(
default=None
)
email:str = EmailStr(
…
)
```



```python
#class Config:
#    schema_extra = {
#        "example": {
#            "first_name": "Giocrisrai",
#            "last_name": "Godoy Bonillo",
#            "age": 30,
#            "hair_color": "black",
#            "is_married": False,
#            "email": "contact@giocrisrai.com"
#        }
#    }
```

```python
@app.get("/")
def home():
return {“Hello”: “World”}
```

## Request and Response Body

```python
@app.post("/person/new")
def create_person(person: Person = Body(…)):
return person 
```

## Validations: Query Parameters

```python
@app.get("/person/detail")
def show_person(
name: Optional[str] = Query(
None,
min_Length=1,
max_Length=50,
title=“Person Name”,
description=“This is the person name. It’s between 1 and 50 characters”
),
age: str = Query(
…,
title=“Person Age”,
description=“This is the person age. It’s required”
)
):
return {name: age}
```

## Validations : Path Parameters

```python
@app.get("/person/detail/{person_id}")
def show_person(
person_id: int = Path (
…,
gt=0,
title=“Person Id”,
description=“This is the person Id. It’s required and greater than 0”
)
):
return {person_id: “It exists!”}
```

## Validations: Request Body

```python
@app.put("/person/{person_id}")
def update_person(
person_id: int = Path(
…,
title=“Person ID”,
description=“This is the personID”,
gt=0
),
person: Person = Body(…),
\#Location: Location = Body(…)
):
\#results = person.dict()
\#results.update(Location.dict())
\#return results
return person
```

## Creando ejemplos de Path y Query parameters automáticos

**Código**

```python
# Python
from typing import Optional
from enum import Enum

# Pydantic
from pydantic import BaseModel
from pydantic import Field

# FastAPI
from fastapi import FastAPI
from fastapi import Body, Query, Path

app = FastAPI()

# Models

class HairColor(Enum):
    white = "white"
    brown = "brown"
    black = "black"
    blonde = "blonde"
    red = "red"

class Location(BaseModel):
    city: str = Field(
        ...,
        min_length=1,
        max_length=50,
        description="The city where the person lives",
        example="New York",
    )
    state: str = Field(
        ...,
        min_length=1,
        max_length=50,
        description="The state where the person lives",
        example="New York",
    )
    country: str = Field(
        ...,
        min_length=1,
        max_length=50,
        description="The country where the person lives",
        example="United States",
    )

class Person(BaseModel):
    first_name: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example="Miguel"
        )
    last_name: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example="Gonzalez"
        )
    age: int = Field(
        ...,
        gt=0,
        le=115,
        example=25
    )
    hair_color: Optional[HairColor  ] = Field(default=None, example=HairColor.brown)
    is_married: Optional[bool] = Field(default=None, example=False)

    #class Config:
    #    schema_extra = {
    #        "example": {
    #            "first_name": "Facundo",
    #            "last_name": "García Martoni",
    #            "age": 29,
    #            "hair_color": "blonde",
    #            "is_married": False,
    #        }
    #    }

@app.get("/")
def home():
    return {"Hello": "World!"}

# Request and Response Body

@app.post("/person/new")
def create_person(person: Person = Body(...)):
    return person

# Validaciones: Query Parameters

@app.get("/person/detail")
def show_person(
    name: Optional[str] = Query(
        None, min_length=1,
        max_length=50,
        title="Person Name",
        description="This is the person name,  It's between 1 and 50 characters",
        example="Rocio"
        ),
    age: str = Query(
        ...,
        title="Person Age",
        description="This is the person age, It's required",
        example="25"
        )
):
    return {name: age}

# Validaciones Path Paremeters

@app.get("/person/detail/{person_id}")
def show_person(
    person_id: int = Path(
        ...,
        gt=0,
        example=123
    )
):
    return {person_id: "It exists!"}

# Validaciones: Request Body

@app.put("/person/{person_id}")
def update_person(
    person_id: int = Path(
        ...,
        title="Person ID",
        description="This is the person ID",
        gt=0,
        example=123
    ),
    person: Person = Body(...),
    location: Location = Body(...)
):
    #results = person.dict()
    #results.update(location.dict())
    #return results
    return person
```

# 6. Conclusiones


## Continua aprendiendo

Nunca pares de Aprender!
