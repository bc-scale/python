<h1>Estructuras de Datos Lineales con Python</h1>

<h2>Héctor Vega</h2>

<h1>Table of Contents</h1>

- [1. Introducción a las estructuras de datos](#1-introducción-a-las-estructuras-de-datos)
  - [Python como base de programación](#python-como-base-de-programación)
  - [Elementos de la programación en Python](#elementos-de-la-programación-en-python)
  - [Tipos de colecciones](#tipos-de-colecciones)
  - [Operaciones esenciales en colecciones](#operaciones-esenciales-en-colecciones)
  - [Colecciones incorporadas en Python](#colecciones-incorporadas-en-python)
- [2. Arrays](#2-arrays)
  - [Arrays](#arrays)
  - [Crear un array](#crear-un-array)
  - [Arrays de dos dimensiones](#arrays-de-dos-dimensiones)
- [3. Linked lists](#3-linked-lists)
  - [Nodos y singly linked list](#nodos-y-singly-linked-list)
  - [Crear nodos](#crear-nodos)
  - [Crear singly linked list](#crear-singly-linked-list)
  - [Operaciones en single linked structures](#operaciones-en-single-linked-structures)
  - [Operaciones a detalle](#operaciones-a-detalle)
  - [Circular linked list](#circular-linked-list)
  - [Double linked list](#double-linked-list)
- [4. Stacks](#4-stacks)
  - [¿Qué son stacks?](#qué-son-stacks)
  - [Crear un stack](#crear-un-stack)
- [5. Queues](#5-queues)
  - [¿Qué son las queues?](#qué-son-las-queues)
  - [Queue basada en listas](#queue-basada-en-listas)
  - [Queue basada en dos stacks](#queue-basada-en-dos-stacks)
  - [Queue basada en nodos](#queue-basada-en-nodos)
  - [Reto: simulador de playlist musical](#reto-simulador-de-playlist-musical)
- [6. Próximos pasos](#6-próximos-pasos)
  - [Más allá de las estructuras lineales](#más-allá-de-las-estructuras-lineales)

# 1. Introducción a las estructuras de datos

## Python como base de programación

- Enterder el concepto e importancia de estructuras de datos.

- Comprender el comportamiento, uso e implementaci&oacute;n de estructuras de datos lineales con Python.
- Poner el pr&aacute;ctica lo aprendido.

**Lenguaje genial**

- Sintacis clara y simple
- Sem&aacute;ntica segura
- Escalable
- Interactivo
- Prop&oacute;sito general
- Gatis y popular

Vídeos de YouTube que hablan sobre estas estructuras de datos en Python. Son de diferentes canales, así pueden conocer a más plataformas que le enseñen tecnología y complementar lo que aprendan en el curso 😃

- [Arrays](https://www.youtube.com/watch?v=phRshQSU-xA)
- [Nodes](https://www.youtube.com/watch?v=652GaaDZPMw)
- [Linked Lists](https://www.youtube.com/watch?v=FU6I-VtjOes)
- [Stacks](https://www.youtube.com/watch?v=NKmasqr_Xkw)
- [Queues](https://www.youtube.com/watch?v=6zmI_BU18xk)

## Elementos de la programación en Python

En esta clase se hizo una puntualización de lo que se debe saber en sí mismo para trabajar en este curso:

**Requisitos Mínimos:**

- Elementos léxicos de python o [Keywords](https://realpython.com/python-keywords/)
- Convenciones de estilo [PEP8](https://www.python.org/dev/peps/pep-0008/)
- Operadores, Manejo de Strings y Literals.
- Entender sobre Listas, Tuplas, Conjuntos, Diccionarios.

**Tener claro:**

- Declaración de funciones
- Recursividad
- Funciones anidades
- High Order Functions
- Funciones lambda.
- Programación Orientada a Objetos

**Nice to Have:**

- Manejo de excepciones
- Manipulación de archivos.

**Python tiene sus propias estructuras:**

- Listas -> `[]`
- Tuplas -> `()`
- Conjuntos (sets) -> `{}`
- Diccionarios -> `{key: value}`

**Para continuar con el curso Tener en cuenta**

- Elementos léxicos
  if, while, def…
- Convenciones
  variables, CONSTANTES, nombre_funciones, nombreClases
- Sintaxis
- Estructuras de datos propias de python
  tuplas, diccionarios, listas, conjuntos
- Funciones
  plus:Recursivas, Anidadas, high order functions, Lambas
  Adicional
- Manejo de errores

## Tipos de colecciones

### Tipos de colecciones

Nos referimos a las estructuras de datos. Una colección es un grupo de cero o más elementos que pueden tratarse como una unidad conceptual.

Tipos de datos.

- Non-zeo Value
- Cero
- null
- undefined

Estos tipos de dato también pueden formar parte de una colección. Existen colecciones de tipo **Dinámicas** que son aquelas que pueden variar su tamaño y las **Inmutables** que no cambian su tamaño.

#### Estructuras Lineales

De forma general encontramos estructuras de datos lineales que están definidas por sus índices. Es decir puedo encotnrarme estrcuturas de datos lineales que sean dinámicas o inmutables, de ello variarán sus propiedades, como poner un elemento al final, (sucesor) o no.

Te encontrarás con Listas, Listas Ordenadas, Cola, Cola de prioridad y más.

- Es decir está ordenadas por posición.
- Solo el primer elemento`no`tiene predecesor

Ej:

- Una fila para un concierto
- Una pila de platos por lavar, o una pila de libros por leer.
- Checklist, una lista de mercado, la lista de Schindler

------

#### Estructuras Jerárquicas

Estructuras basadas en una jerarquia definida.
Los árboles pueden tener n números de nieleves hacia abajo o adyacentes. Te encotnrarás con árboles Binarios, Montículos.

- Ordendas como árbol invertido (raices)
- Solo el primer nodo `no` tiene predecesores, pero si sucesores.
- Es un sistema de padres e Hijos.

Ej:

- Libros, Capítulos, Temas.
- Abuelos, Madres, Hijos.

------

#### Estructuras Grafos:

- Cada dato puede tener varios predecesores y sucesores, se les llama vecinos
- Los elementos se relecionan entre si con n relaciones.

Ej:

- Vuelos aéreos, sistemas de recomendación
- La mismísima interntet es un grafo

------

#### Estructuras Desordenadas:

Estructuras como Bolsa, Bolsa ordenada, Conjuntos, DIccionarios, Diccionario ordenados.

- No tienen orden en particular
- No hay predecesores o sucesores.

Ej:

- Una bolsa de gomitas, no sabe de qué color te va a tocar.

------

#### Estructuras Ordenadas:

Son estructuras que imponen un orden con una regla. Generalmente una regla de orden.
`item <= item(i+1)` Es decir que el tiem que sigue es el primer elemento +1.

Ej:

- Los directorios telefónicos, los catálogos,

------

#### Conclusión:

Suponga que tiene un dataset con muchos datos, una colección de libros, música, fotos, y desea ordenar esta colección, ante esta situación siempre existe el Trade Off entre `rapidez/costo/memoria` El conocimeinto de las propiedades de las colecciones te facilitará la selección de estructura de datos según sea el caso y velar por un software eficiente.

![ed1.jpg](https://static.platzi.com/media/user_upload/ed1-ca54b039-9574-4ac2-9075-79de80998d37.jpg)

- [Algorithms Course - Graph Theory Tutorial from a Google Engineer](https://www.youtube.com/watch?v=09_LlHjoEiY)
- [HackerRank](https://www.youtube.com/c/HackerrankOfficial/videos?view=0&sort=p&flow=grid) este último enlace tiene los vídeos más populares donde explican los algoritmos de ciencias de la computación 😃

## Operaciones esenciales en colecciones

![Captura de pantalla 2021-05-12 131237.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-05-12%20131237-d69f9e1b-9254-46ba-9a84-48b495c8c525.jpg)

![gif](https://media.giphy.com/media/teiVe42etuAgV4W0vN/giphy.gif?cid=790b7611e8ac2c6b9f22a36156a58fc648f367610c81c06f&rid=giphy.gif&ct=g)

### Asi es como funciona amigos

si alguien tiene una duda con el codigo del profe bueno espero que esto les de una idea de como funciona la recursividad

lo hice asta tres, es que hacer un gif es difícil además no debía pasarme de los 20s, ignoren lo que esta debajo del `if __name__ : "__main__"` `piramide(1,4)` no me di cuenta asta que termine de editarlo XD.

#### Operaciones Esenciales en Colecciones:

Algunas operaciones básicas responden necesidades puntuales como saber:

- Tamaño: Las dimensiones
- Pertenencia: Si un elemento pertenece o no.
- Recorrido: Pasar por los elementos
- String: Converir la colección a un string.
- Igualdad: Comparar colecciones
- Concatenación: Unir o sumar listas
- Conversión de tipo: Convertir tipos de datos entre si
- Insertar, Remover, Reemplazar, Acceder a n elementos en n posición.

------

Aquí se hace referencia a los métodos nativos de python para hacer ordenamientos, insertar, acceder y remover elementos. `sort(), remove(), pop()` Estos métodos son útiles para determinadas tareas. Para sacar todo el provecho de estas se usarán junto a funciones cómo esta:

```python
'''
Crea espacios hacia la derecha si el número lower es menor que el número upper, cuando son diferentes, en cada llamada el número lower base aumenta en 1 y la margen en 4 espacios.

El propósito de este  snipet  de código es ejemplificar qué tipo de conceptos (funciones, condicionales, recursividad) veremos en las estructuras de datos como métodos de clase. Si no entiendes la función no te rindas :), repasa y regresa.

'''


def pyramid_sum(lower, upper, margin =0):
	blanks = " " * margin
	print(blanks, lower, upper)
	if lower > upper:
		print(blanks, 0)
		return 0
	else:
		# Llamada recursiva
		result = lower+ pyramid_sum(lower + 1, upper, margin +4)
		print(blanks, result)
		return result

  
 
pyramid_sum(1,10)
```

`Code`

```python
def pyramid_sum(lower, upper, margin=0):
    blanks = " " * margin
    print(blanks, lower, upper)
    if lower > upper:
            print(blanks, 0)
            return 0
    else:
            result = lower + pyramid_sum(lower + 1, upper, margin +4)
            print(blanks, result)
            return result
```

## Colecciones incorporadas en Python

> Es muy importante saber cuando usar una cierta colección, ya que de ello depende tanto el tamaño que ocupará en memoría como la velocidad en ciertas tareas.
> De forma general es recomendable usar tuplas en lugar de listas siempre que no se requiera estar cambiando los valores, ya que ocupan menos espacio en memoría.
> Así como usar sets o diccionarios para busqueda de un elemento, ya que son más rapidos.
>
> Les comparto un código de cuanto pesan las diferentes estructuras de python
>
> ```python
> import sys
> 
> colecciones = {"list": list(), "tuple": tuple(), "dict": dict(), "set": set()}
> 
> for name, value in colecciones.items():
>     print(f'{name} = {sys.getsizeof(value)} bytes')
> list = 56 bytes
> tuple = 40 bytes
> dict = 232 bytes
> set = 216 bytes
> ```

#### Colecciones Incorporadas en Python

- **Listas**: Propósito general, de índices con tamaños dinámicos. Ordenables `lista =[]`.
- Usaria las listas para almacenar una serie de números, una lista de palabras,y básicamente cualquier cosa.

------

- **Tuplas**: Inmutables, no se pueden añadir más elementos. Utiles para constantes por ejemplo coordenadas, direcciones. Es de tipo secuencial. `tupla =()`
- Las usuaría cuando sé exactamente el tamaño que tendrán mis datos.

------

- **Conjuntos**: Almacenan objetos no duplicados.(Teoría de conjuntos), son de acceso rápido, aceptan operaciones lógicas, son desordenados.`set()` `conjunto={1,2,3,4}`
- Usaría un casteo entre conjuntos y listas cuando quiero eliminar duplicados de una lista.

------

- **Diccionarios**: Pares de llaver valor, arrays asociativos (hash maps), son desordenados, y muy rápidos para hacer consultas. `diccionario ={'Llave':"Valor"}`
- Los usaría para almacenar datos, listas, objetos que perfectamente pueden volverse un `dataframe`, o un `defaultdict`

> Lista: Listas para organizar las responsabilidades del día.
> Tupla: Almacenar el Documento de Identidad del cliente.
> Sets: Cursos que voy llevando.
> Diccionario: Glosario con los nuevos términos que voy aprendiendo.

# 2. Arrays

## Arrays

Es una estructura de datos lineal, las estructuras de datos son representaciones internas de una colección de información, por lo que un array puede representarme de una forma particular y con unas características puntuales.

- Elemento: Valor almacenado en las posiciones del array
- Indice: Referencia a la posición del elemento.

------

En a memoria los **arrays** se almacenan de manera consecutiva, **los bits se guardan casilla por casilla** consecutivamente.

------

El array tiene una capacidad de almacenamiento. Puedo tener **arrays** en 1,2 y/o 3 dimensiones. A mayor complejidad dimensional, es decir, si aumenta la dimensión se hace más complicado acceder a estos datos, se recomienda en python trabajar con `dimensiones <2`

------

**NOTA:** Los arrays son un tipo de listas, pero las listas no son arrays. Los arrays son diferentes y poseen las siguientes **restricciones:**

No pueden:

- Agregar posiciones
- Remover Posiciones
- Modificar su tamaño
- Su capacidad define al crearse

Los arrays se usan en los sprites de los videojuegos, o en un menú de opciones. Son opciones definidas.

El módulo array de python solo almacena números y caracteres, está basado en listas. Sin embargo tiene funciones reducidas, pero podemos crear nuestros propios arrays.

Como ya se mesionó los arreglo o Arrys son estructuras de datos lineales que representa y colecciona información, en informatica y los Arrays comienza en la posicion 0 y termina en la posicion N-1, siendo este N el tamaño del Array.

![array.PNG](https://static.platzi.com/media/user_upload/array-dfd4990b-c1bf-4048-83d7-14d36dfd61e1.jpg)

La matrices son arrays de dos dimesiones y de tres se denominan tensores, aunque estos conceptos son de matematicas avanzada, pero la forma de trabajar con ellos sigue siendo la misma, cada punto esta representado por una coordenada a la cual llamamos indice y podemos acceder a los elementos atraves el.

#### ¿Cómo funciona la memoria en la computadora?

Se guarda la información en bits, de forma consecutiva. Si eliminamos esos datos o la memoria es liberada, se genera un espacio vacío. Alterando el flujo consecutivo. Si no existe el espacio de memoria necesario la información no es guardada.

#### Arrays de 1D, 2D o 3D

- Elemento(valores almacenados)
- índice(posición)
  Los arrays guardan información de forma consecutiva.
  Tamaño y capacidad determinada. No se agrega ni se remueve posiciones.
  Python tiene un módulo "array"
  Solo almacena números y caracteres.

> No se recomienda usar mas de 2D ya que aumenta la complejidad de computo para acceder a los datos y el código es más complejo de leer.
> Arrays son un tipo de Listas pero Listas 

Conceptos clave:

- Elemento -> Valor almacenado en las posiciones del array.
- Índice -> referencia a la posición del elemento.

Podemos hacer arrays de 1D, 2D y 3D pero no se recomienda hacer más de 2D, porque se vuelve muy complejo el computo. Los array son listas, pero las listas no son arrays.

Los arrays se usan por ejemplo en los mapas de bits, son arrays 2D.

## Crear un array

Solucion al reto:
Clase Array:
![reto1_dataStructures_arrayClass.png](https://static.platzi.com/media/user_upload/reto1_dataStructures_arrayClass-2d591480-21bc-42a4-bcec-ca12fc1c09e3.jpg)

Programa principal

![reto1_dataStructures_main.png](https://static.platzi.com/media/user_upload/reto1_dataStructures_main-0961cebf-e6c7-4129-8450-4b4b075c0737.jpg)

Solución al reto

```python
import random
from functools import reduce


class Array:
    def __init__(self, capacity, fill_value=None):
        self.items = list()

        for i in range(capacity):
            self.items.append(fill_value)

    def __len__(self):
        return len(self.items)

    def __str__(self):
        return str(self.items)

    def __iter__(self):
        return iter(self.items)

    def __getitem__(self, index):
        return self.items[index]

    def __setitem__(self, index, new_item):
        self.items[index] = new_item

    def __addRandomItems__(self, lower, upper):
        self.items = [random.randint(lower, upper)
                      for i in range(len(self.items))]

    def __sumItems__(self):
        return reduce(lambda a, b: a+b, self.items)


if __name__ == "__main__":
    menu = Array(5)
    print(menu)
    menu.__addRandomItems__(1, 5)
    print(menu)
    print(menu.__sumItems__())
```

método de reemplazo no pido *lower* ni *upper* y hago 

```python
random.randint(0, self.__len__())
```

![Captura1.PNG](https://static.platzi.com/media/user_upload/Captura1-865a209f-ec1c-4a8a-b898-7bd7ba5dfb3b.jpg)

A diferencia de otros lenguajes de programación, en Python el Array y ArrayList forman una sola [estructura llamada Lista](https://www.youtube.com/watch?v=zg9ih6SVACc&t=8152s)

Al terminar el reto,

```python
class Array(object):
    "Represents an array."

    def __init__(self, capacity, fill_value = None):
        """
        Args:
            capacity (int): static size of the array.
            fill_value (any, optional): value at each position. Defaults to None.
        """
        self.items = list()
        for i in range(capacity):
            self.items.append(fill_value)

    def __len__(self):
        """Returns capacity of the array."""
        return len(self.items)

    def __str__(self):
        """Returns string representation of the array"""
        return str(self.items)

    def __iter__(self):
        """Supports traversal with a for loop."""
        return iter(self.items)

    def __getitem__(self, index):
        """Subscrit operator for access at index."""
        return self.items[index]

    def __setitem__(self, index, new_item):
        """Subscript operator for replacement at index."""
        self.items[index] = new_item
```

## Arrays de dos dimensiones

Solucion al reto 2 sin aplicar la clase Grid:

clase Cube:
![reto2_dataStructures_CubeClass.png](https://static.platzi.com/media/user_upload/reto2_dataStructures_CubeClass-f9b286bc-5c6d-4ce0-9780-ab2efcdeb78b.jpg)

main:
![reto2_dataStructures_main.png](https://static.platzi.com/media/user_upload/reto2_dataStructures_main-1ae754a9-0dce-4913-b005-cf5f5991a4e6.jpg)

**soluciones al reto.**

1ra Parte - Incorpora un método para poblar sus slots

```python
from array_clase import Array
import random


class Grid():
    def __init__(self, rows, columns, fill_value=None):
        self.data = Array(rows)
        for row in range(rows):
            self.data[row] = Array(columns, fill_value=fill_value)

    def get_height(self):
        return len(self.data)

    def get_width(self):
        return len(self.data[0])

    def __getitem__(self, index):
        return self.data[index]

    def __str__(self):
        result = ""

        for row in range(self.get_height()):
            for col in range(self.get_width()):
                result += str(self.data[row][col]) + " "

            result += "\n"

        return str(result)

    def random_fill(self, min, max):
        for i in range(self.get_height()):
            for j in range(self.get_width()):
                self[i][j] = random.randint(min, max)
```

Prueba del reto

```python
from array2D import Grid
matrix = Grid(3,3)
matrix.random_fill(1,100)
print(matrix)
```

Salida

```sh
72 97 45 
33 97 65
18 11 100
```

**2° parte del reto**
**Array 3D**

```python
from array2D import Grid
from array_clase import Array
import random


class Array3D():
    def __init__(self,depth, rows, columns,fill_value=None):
        self.data = Grid(rows, columns)
        for row in range(rows):
            for column in range(columns):
                self.data[row][column] = Array(depth, fill_value=fill_value)

    def get_rows(self):
        return self.data.get_height()

    def get_columns(self):
        return self.data.get_width()

    def get_depth(self):
        return len(self.data[0][0])
    
    def __getitem__(self, index):
        return self.data[index]

    def __str__(self):
        result = ""

        for depth in range(self.get_depth()):
            result += f'index depth: [{depth}] \n'
            for row in range(self.get_rows()):
                for column in range(self.get_columns()):
                    result += f'   {self.data[row][column][depth]} '
                result += "\n"
            result += "\n"
        return str(result)

    def random_fill3D(self, min, max):
        for i in range(self.get_rows()):
            for j in range(self.get_columns()):
                for x in range(self.get_depth()):
                    self[i][j][x] = random.randint(min, max)
```

Prueba de la clase

```python
from array3D import Array3D

tensor3D =  Array3D(depth=2,rows=4,columns=3)
print(tensor3D.get_rows())
print(tensor3D.get_columns())
print(tensor3D.get_depth())
print()
print(tensor3D)

tensor3D.random_fill3D(0,9)
print(tensor3D)
```

Salida

```sh
4
3
2

index depth: [0]
   None    None    None
   None    None    None
   None    None    None
   None    None    None

index depth: [1]
   None    None    None
   None    None    None
   None    None    None
   None    None    None


index depth: [0]
   7    0    7
   9    0    5
   9    4    6
   1    5    0

index depth: [1]
   4    2    2
   6    4    5
   7    7    2
   5    5    4
```

# 3. Linked lists

## Nodos y singly linked list

Las estructuras linked consisten en nodos conectados a otros, los más comunes son sencillos o dobles. No se accede por índice sino por recorrido. Es decir se busca en la lista de nodos hasta encontrar un valor.

- **Data**: Será el valor albergado en un nodo.

- **Next**: Es la referencia al siguiente nodo en la lista

- **Previous**: Será el nodo anterior.

- **Head**: Hace referencia al primer nodo en la lista

- **Tail**: Hace referencia al último nodo.

**¿Cómo funciona en memoria los Linked Estructures?**

Estas estructuras de datos hablan de nodos/datos repartidos en memoria, diferentes a los arrays que son contiguos. Los nodos se conectan a diferentes espacios en memoria, podemos acceder a los datos saltando en memoria, siendo mucho más ágil. Los nodos nos sirven para crear otras estructuras más complejas, como Stacks, Queues, las llamadas pilas y colas. Es posible optimizar partes del código usando nodos.

**Double Linked Structure**

Estos hacen que el nodo haga referencia al siguiente nodo y al anterior, es decir nos va a permitir ir en ambas direcciones. También nos permitirá realizar “formas” y contextos circulares.

- El ejemplo clave aquí será función de ctrl+z y ctrl+y Estas opciones nos permiten hacer y deshacer un proceso  en Windows.

- El historial del navegador también es un buen ejemplo al permitirnos navegar entre el pasado y el presente.

> Los linked list tienen una desventaja importante, si la lista crece mucho será más costoso computacionalmente recorrer toda la lista.
> Es importante saber cuando usarlas y cuando no.

>  A tomar en cuenta: Si una lista es muy larga, tomará mucho tiempo recorrerla, esto por O(n).

son usadas como el apoyo o soporte de otras estructura de datos como los Stacks y Queues, o se puede evidenciar su uso en el historial de navegador.
Otros ejemplos de uso:

- Lista de Spotify: que  apunta automáticamente a la siguiente canción de la lista de reproducción.

![Capture.PNG](https://static.platzi.com/media/user_upload/Capture-2d2d5116-f8ab-457e-96f0-4b12ec22a346.jpg)

- Software de visualización de fotos: donde cada nodo es una imagen y el puntero apunta simplemente a la siguiente foto de la lista.

## Crear nodos

Cada nodo almacenará un valor y cada nodo tiene un puntero que llevará a otro nodo con otro valor  y así obtener los datos allí almacenados.
Es muy útil al tener infromación dispersa en memoria y cuando queremos que sean consultas ágiles, es importante entender que los nodos son la base para implementaciones más elaboradas de estructuras de datos, Stacks, Qeues, Deque,

**Doubly, Singly List, Circular list, Graphs .**

Cada estructura de datos servirá para un propósito dentro de un contexto, por ejemplo los grafos acíclicos, donde se usan para sistemas de recomendaciones al mostrar las relaciones entre objetos o representar los tipos de redes que se forman entre nodos.  Para crear un nodo:

**Creamos una clase Node**

Referimos valores mediante argumentos de instancias.
Unimos los nodos iterando entre referencias.

Este script tiene como propósito crear nodos.

**Constructor:**

- data= El dato del nodo.
- next= está por defecto en None, porque en una serie de nodos el +ultimo te lleva a ninguna parte

```python
class Node():
    def __init__(self, data, _next=None):
        # Atributos
        self.data = data
        self.next = _next
```

A continuación la instancia de la clase Node

```python

from node import Node


node1 = None
node2 = Node("A", None)
# Los nodos al ser secuanciales permiten refrencias a cualquier lugar.
node3 = Node("B", node2)



def show_relations():
    '''
    Este script perfectamente puede ser una función que recibe al nodo como parámetro pythony llamo para mostrar las
    relaciones.

    '''
    print("Esto es la ubicación en memoria de los nodos")
    print(node2) #<node.Node object at 0x0000022017FEAD30>
    print(node3) #<node.Node object at 0x0000022017FEAD90>
    print("Esto es el dato y muestra la relación entre nodos")
    print(node2.data,"-->", node2.next) #A --> None
    print(node3.data,"-->", node3.next) #B --> <node.Node object at 0x0000015356D6AD30>
    print("El siguiente dato del nodo es:")
    # Se refiere al nodo que está conectado y luego al dato que este contiene.
    print(node3.next.data) #'A'
    print("Creando el nodo1 y mostrando datos y relacion con nodo3 obtenemos: ")
    # Asignar una propiedad a un elemento para volverlo nodo.
    # Al intanciar la clase con una relación estamos ligando los nodos.
    node1= Node("C", node3)
    print(node1.data,"-->", node1.next)#C --> <node.Node object at 0x0000022017FEAD90>



def create_nodes():
    print("Creo nodos que se asignan a un solo valor en memoria, en este caso a node2: ")
    for node in range(5): #n --> <node.Node object at 0x000001B2AD991FD0>
        head = Node(node, node2)
        print(head.data,"-->", head.next)


def run():
    show_relations()
    create_nodes()

if __name__ == '__main__':
    run()
```

Una forma mas clara de ver la composicion de los nodos es usando dataclasses, que son una forma un poco mas sencilla de declarar clases en Python :
[Data Classes in Python 3.7+ (Guide)](https://realpython.com/python-data-classes/)

**Clase Node**:
![dataClass_Node.png](https://static.platzi.com/media/user_upload/dataClass_Node-f6af173c-6466-4342-b15b-3a75e347c5d2.jpg)

**Main**:
![dataClass_Node_main.png](https://static.platzi.com/media/user_upload/dataClass_Node_main-2f314287-b66e-420d-a1e9-de372d0de562.jpg)

**Output**:
![dataClass_Node_output.png](https://static.platzi.com/media/user_upload/dataClass_Node_output-c18d072a-4d7b-47d3-9aff-9e2cabb5b61f.jpg)

## Crear singly linked list

![img](https://programacionycacharreo.files.wordpress.com/2018/10/singly_linked_list.jpeg)

una lista enlazada (linked list en inglés), es un tipo de estructura de datos compuesta de nodos. Cada nodo contiene los datos de ese nodo y enlaces a otros nodos.

Se pueden implementar distintos tipos de listas enlazadas. En este post vamos a implementar una lista enlazada lineal simple (singly linked list). En este tipo de listas, cada nodo contiene sus datos y un enlace al siguiente nodo. Además la lista tendrá un método para contar el número de elementos de la lista, un método para insertar un elemento en la lista y un método para eliminar un elemento de la lista.

En primer lugar, definimos una clase que va a ser la clase Node. Los objetos de esta contendrán sus propios datos y un enlace al siguiente elemento de la lista:
class Node:

```python
    def __init__(self, data):
​        self.data = data
​        self.next = None


A continuación definimos la clase de la lista SinglyLinkedList, que contiene el primer elemento de la lista:
class SinglyLinkedList:
    def __init__(self, head):
        self.head = head

El método que cuenta los elementos de la lista, length(), primero comprueba que la lista no esté vacía, y luego recorre todos los elementos de la lista incrementando un contador por cada elemento. Al final devuelve el contador:
    def length(self) -> int: 
        current = self.head
        if current is not None:
            count = 1

​        while current.next is not None:
​            count += 1
​            current = current.next
​        return count
​    else:
​        return 0


```

El siguiente método, `insert(datos, posición)`, inserta un elemento   tras la posición indicada. Si se indica la posición 0, el nuevo elemento pasa a ser la cabecera de la lista. En esta implementación,  si la posición que se pasa como argumento excede el tamaño de la lista,el elemento se inserta al final:

```python
def insert(self, data, position):
       new_node = Node(data)

   if position == 0:
       new_node.next = linked_list.head
       linked_list.head = new_node
   else:
       current = linked_list.head
       k = 1
       while current.next is not None and k < position:
           current = current.next
           k += 1
       new_node.next = current.next
       current.next = new_node
```

El método delete(posición) borra el elemento en la posición pasada como parámetro. Si es el primer elemento la lista de la cabeza pasa a ser el  segundo elemento. Si se encuentra el elemento en la lista y se borra devolvemos True, en caso contrario devolvemos False:

```python
 def delete(self, position):
       if position != 1:
           current = self.head
           k = 1
           while current.next is not None and k < position - 1:
               current = current.next
               k += 1
           if current.next is not None:
               current.next = current.next.next
               return True
           else:
               return False
       else:
           self.head = self.head.next
           return True

Creamos la lista
linked_list = SinglyLinkedList(Node(1))

Rellenamos la lista
 for i in range(2,10):
        linked_list.insert(i, i-1)

Insertamos un elemento
    linked_list.insert(999,3)

Eliminamos un elemento
    linked_list.delete(6)

Mostramos la lista
    current = linked_list.head
    while current is not None:
	print(current.data)
        current = current.next
```

`reto.py`

```python
# Reto
    array = [2, 4, 6]
    datos = SinglyLinkedList()

    for i in array:
        datos.append(i)
    current = datos.tail
    
    while current:
        print(current.data)
        current = current.next
ResponderDAVID IBARRA BETANZOShace 9 meses1ExcelenteLeslorhace 8 meses5from node import Node
class LinkedList:
	def __init__(self):
		self.head=None

	def insert_at_begining(self, data):
		node=Node(data,self.head)
		self.head=node

	def insert_at_end(self, data):
		#caso de que sea el primero
		if self.head is None:
			self.head=Node(data, None)
			return
		itr =self.head
		while itr.next:
			itr=itr.next

		itr.next=Node(data, None)

	#de una Lista a una LinkedList
	def insert_value_from_list(self, data_list):
		self.head=None
		for data in data_list:
			self.insert_at_end(data)
def print(self):
		if self.head is None:
			print("Linked list is empty")
			return

		itr=self.head
		llstr=''
		
		while itr:
			llstr+=str(itr.data) + '-->'
			itr =itr.next

		print(llstr)
	
if __name__=="__main__":
	ll=LinkedList()
	ll.insert_value_from_list(["banana","mango","fresa"])
	ll.print()


Ver másResponderAlejandro Sengerhace 2 meses4Dejo un video que me ayudó a entender el tema del “current” y self.tail
https://www.youtube.com/watch?v=oXuKUkIlv_oResponderAxel Coronahace 25 días1Dios?Jorge Salinashace 22 días1Amigo muchas gracias, el video me sirvió mucho. Muy recomendado que lo vean para las personas que no les quedó claro como a mi.Sebastian Penagoshace 9 meses4Lista enlazada lineal simple (singly linked list) – Implementación en Python

una lista enlazada (linked list en inglés), es un tipo de estructura de datos compuesta de nodos. Cada nodo contiene los datos de ese nodo y enlaces a otros nodos.
Se pueden implementar distintos tipos de listas enlazadas. En este post vamos a implementar una lista enlazada lineal simple (singly linked list). En este tipo de listas, cada nodo contiene sus datos y un enlace al siguiente nodo. Además la lista tendrá un método para contar el número de elementos de la lista, un método para insertar un elemento en la lista y un método para eliminar un elemento de la lista.
En primer lugar, definimos una clase que va a ser la clase Node. Los objetos de esta contendrán sus propios datos y un enlace al siguiente elemento de la lista:
class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


A continuación definimos la clase de la lista SinglyLinkedList, que contiene el primer elemento de la lista:
class SinglyLinkedList:
    def __init__(self, head):
        self.head = head

El método que cuenta los elementos de la lista, length(), primero comprueba que la lista no esté vacía, y luego recorre todos los elementos de la lista incrementando un contador por cada elemento. Al final devuelve el contador:
    def length(self) -> int: 
        current = self.head
        if current is not None:
            count = 1

            while current.next is not None:
                count += 1
                current = current.next
            return count
        else:
            return 0

El siguiente método, insert(datos, posición), inserta un elemento   tras la posición indicada. Si se indica la posición 0, el nuevo elemento pasa a ser la cabecera de la lista. En esta implementación,  si la posición que se pasa como argumento excede el tamaño de la lista,el elemento se inserta al final:
   def insert(self, data, position):
       new_node = Node(data)

       if position == 0:
           new_node.next = linked_list.head
           linked_list.head = new_node
       else:
           current = linked_list.head
           k = 1
           while current.next is not None and k < position:
               current = current.next
               k += 1
           new_node.next = current.next
           current.next = new_node

El método delete(posición) borra el elemento en la posición pasada como parámetro. Si es el primer elemento la lista de la cabeza pasa a ser el  segundo elemento. Si se encuentra el elemento en la lista y se borra devolvemos True, en caso contrario devolvemos False:
    def delete(self, position):
       if position != 1:
           current = self.head
           k = 1
           while current.next is not None and k < position - 1:
               current = current.next
               k += 1
           if current.next is not None:
               current.next = current.next.next
               return True
           else:
               return False
       else:
           self.head = self.head.next
           return True

Creamos la lista
linked_list = SinglyLinkedList(Node(1))

Rellenamos la lista
 for i in range(2,10):
        linked_list.insert(i, i-1)

Insertamos un elemento
    linked_list.insert(999,3)

Eliminamos un elemento
    linked_list.delete(6)

Mostramos la lista
    current = linked_list.head
    while current is not None:
	print(current.data)
        current = current.next
ResponderMiguel Ángel Reyes Morenohace 5 meses3Pues logré hacer que mostrará el mensaje cuando no encuentra un dato. Además logré implementar head & tail en esta lista:
from node import Node

class SinglyLinkedList:
  def __init__(self):
    self.head = None
    self.tail = None
    self.size = 0


  def append(self, data):
    node = Node(data)

    if self.tail == None and self.head == None:
      self.head = node
      self.tail = node
    else:
      current = self.tail

      while current.next:
        current = current.next

      current.next = node
      self.tail = current.next

    self.size += 1


  def size(self):
    return str(self.size)


  def iter(self):
    current = self.tail

    while current:
      value = current.data
      current = current.next
      yield value #* Genera valores pero NO los almacena


  def delete(self, data):
    current = self.tail
    previous = self.tail

    while current:
      if current.data == data:
        if current == self.tail:
          self.tail = current.next
        else:
          previous.next = current.next
          self.size -= 1
          return current.data

      previous = current
      current = current.next


  def search(self, data):
    flag = False
    for node in self.iter():
      if data == node:
        flag = True
        print(f'Data {data} found 😎')
    if not flag:
      print(f'Data {data} not found 😞')


  def clear(self):
    self.tail = None
    self.head = None
    self.size = 0


games = SinglyLinkedList()
games.append('GTA V')
print(f'head: {games.head.data} & tail: {games.tail.data}')
games.append('Fifa')
print(f'head: {games.head.data} & tail: {games.tail.data}')
games.append('Mario')
print(f'head: {games.head.data} & tail: {games.tail.data}')

Ver másRespondermarco antoniohace 6 meses2
En momentos como este es cuando detesto mi dislexia, me confundí con el reto no se si era hacer que los nodos se comportaran como un array de dos dimensiones o convertir el array de tipo matriz de las clases pasadas a nodos usando un método de conversión, o reconvertir la clase nodo para que fuera una matriz T^T
Bueno aquí mi código
class Nodo:
    def __init__(self, valor, siguiente_nodo = None):
        self.valor = valor
        self.siguiente_nodo = siguiente_nodo


class Lista_nodo:

    def __init__(self):
        self.limpiar()



    def limpiar(self):
        self.nodo_actual = None
        self.__SEGURO__ = False
        self.nodo_tmp = None
        self.tam = 0



    def append(self,valor):
        nodo_nuevo = Nodo(valor)
        self.tam += 1

        if not self.nodo_actual:
            self.nodo_actual = nodo_nuevo
            return

        nodo_siguiente = self.nodo_actual

        while nodo_siguiente.siguiente_nodo:
            nodo_siguiente = nodo_siguiente.siguiente_nodo

        nodo_siguiente.siguiente_nodo = nodo_nuevo



    def index(self,indice):
        if not self.tam:
            raise AssertionError("Error sin elementos")

        def INDEX(INDICE):
            if INDICE > self.tam - 1 or INDICE < 0:
                raise AssertionError("Error de indice fuera de los limites")
            i = 0
            nodo = self.nodo_actual
            while i != INDICE:
                self.nodo_tmp = nodo
                nodo = nodo.siguiente_nodo
                i += 1
            if self.__SEGURO__:
                return nodo.siguiente_nodo
            return self.nodo_tmp.valor

        if indice < 0:
            return INDEX(self.tam + indice)
        else:
            return INDEX(indice)
            
                

    def pop(self,indice = -1):
        self.__SEGURO__ = True
        try:
            self.nodo_tmp.siguiente_nodo = self.index(indice)
            self.tam -= 1
        except AssertionError:
            raise AssertionError("Indice inexistente")
        finally:
            self.__SEGURO__ = False
            


    def find(self,valor):
        for val , indice in self.iter_items():
            if val == valor:
                print("valor encontrado")
                return indice
        print("el valor no existe")
        return None



    def printing(self,elementos_mostrados_en_fila = 10):
        print("|",end="")
        salto = elementos_mostrados_en_fila - 1
        for val, i in self.iter_items():
            if i < salto:
                print(f" {val} |",end="")
            else:
                salto += elementos_mostrados_en_fila - 1
                print("")
        print("")



    def iter(self):
        for val, i in self.iter_items():
            yield val



    def iter_items(self):
        nodo_siguiente = self.nodo_actual
        if not nodo_siguiente:
            return
        i = 0
        while nodo_siguiente.siguiente_nodo:
            val = nodo_siguiente.valor
            nodo_siguiente = nodo_siguiente.siguiente_nodo
            yield (val, i)
            i += 1
        yield (nodo_siguiente.valor,i)



    def absorver(self, estructura_de_datos):
        # Eto... la neta no entendi el reto XD
        pass

if __name__ == "__main__":
    from random import randint
    lista = Lista_nodo()

    print("\nmetodo append [ Nodo.append(",end="")
    for i in range(1,8):
        val = randint(1,20)
        print(f"{val},",end="")
        lista.append(val)
    print(") ]\n")
    print("metodo pop [Nodo.pop(\"con o si indice\")]\n")
    lista.printing()
    i = -2
    lista.pop(i)
    print(f"Nodo.pop({i})")
    lista.printing()
    print("\nmetodo index [Nodo.index(int)]\n")
    print(f"Nodo[{i}] = {lista.index(i)}")
    print("\nmetodo iter [Nodo.iter()]\n")
    for val in lista.iter():
        print(val,end= ",")
    print("\n\nmetodo iter_items [Nodo.iter_items()]\n")
    for val, idx in lista.iter_items():
        print(f"[{idx}] = {val}",end=" | ")
    print("\n\nmetodo find [Nodo.find(\"valor\")]\n")
    j = 11
    print(f"indice = {lista.find(j)} Nodo.find({j})")
Ver másResponderAaron Contreras Garibayhace 6 meses2llamen al metodo size de otra forma, ya que existe un atributo llamado size.ResponderChristian Molina Vázquezhace 10 meses2Espero que platzi agregue una clase de generadores, creo que hasta ahora en los cursos que he tomado de python no lo han explicado.
Les recomiendo aprender sobre ellos ya que son muy importantes cuando se van a iterar los datos pero no queremos guardarlos en memoria.
En el método iter de nuestra clase se usa el keyword yield que permite crear un generador.
Si les gusta leer más sobre ellos:
https://wiki.python.org/moin/Generators
https://es.stackoverflow.com/questions/6048/cuál-es-el-funcionamiento-de-yield-en-python/6084Ver másResponderVictor Inojosahace 10 meses6aqui te dejo un par de clases respecto a iteradores y generadores

https://platzi.com/clases/1378-python-practico/14334-iterators-and-generators/
https://platzi.com/clases/1909-scrapy/29441-recordando-generadores-e-iteradores/
Héctor Daniel Vega QuiñonesProfe Platzihace 10 meses6Ya viene un curso avanzado de Python donde se aborda el tema 😃Levi Nuñezhace 3 días1Creo que pasa mucho en varias clases de los cursos que los profesores o saben exactamente como explicar un tema que solo van tirando código como locos si explicar de forma entendible que va haciendo cada línea, y creo que este es el caso de esta claseResponderLevi Nuñezhace 3 días1Les comparto una imagen que lo explica de forma grafica como es la secuencia en la lista
![](ResponderJorge Salinashace 22 días1Espero les ayude.
Reto:
from arreglo import Arreglo
from node import Node
from linked_list import SinglyLinkedList

size = 5
#.__replaceitems__() Función para replazar los valores del arreglo por numeros aleatorios
lista = Arreglo(size).__replaceitems__()
print(lista)
nodes = SinglyLinkedList()
for i in range(size):
    nodes.append(data=lista[i])

current = nodes.tail
while current:
    print(current.data)
    current = current.next

Salida:
[80, 37, 49, 80, 52]
80
37
49
80
Ver másResponderLuis Eduardo Lerena Pereirahace un mes1Reto:
from MiArray import Array
from linked_list import SinglyLinkedList

def run():
    array = Array(6)
    array.__populate__(0, 100)
    print(array)
    node = SinglyLinkedList()
    for data in array.__iter__():
        node.append(data)

    del array

    current = node.tail
    while current:
        print(current.data)
        current = current.next

if __name__ == '__main__':
    run()
Ver másResponderJulián Escobarhace un mes1Hola, creo que la función “Delete” del profesor está mala. Me corrigen si estoy equivocado pero creo que la forma correcta es la siguiente:
def delete(self, data):
    current = self.tail
    prev =current

    while current:

        if current.data == data:

            if current == self.tail:

                self.tail = current.next
            
            else:

                prev.next = current.next
                current = current.next
                self.size -= 1
            
            return current.data

        else:

            prev = current
            current = current.nextVer másResponderGuillermo De Felicehace un mes1Acá mismo abandono el curso, la explicación del profesor no se alinea para NADA con la ruta de aprendizaje que sugieren en “Desarrollo Backend con Python”. Una lástimaResponderJulian Alejandro Sarmiento Linareshace un mes1Anotación: el metodo setvalues se creo en el reto de llenar el array con numeros random
from Array import Array
from linked_list import SinglyLinkedList

list = Array(5)

list.__setvalues__()
print(list.__str__())

data = SinglyLinkedList()

for i in list:
    data.append(i)
current = data.tail

while current:
    print(current.data)
    current = current.next
Ver másResponderEmanuel Schembergerhace 2 meses1pienso que me gustaría que las armara desde el principio y no utilizara cosas ya hechas, ya se que no hay que reinventar la rueda, pero me siento incompleto, alguien tiene material para complementar?ResponderJunior Peveshace 2 meses1Agregar por indice:
ResponderJunior Peveshace 2 meses1Borrar un dato segun su indice:
ResponderJunior Peveshace 2 meses1Una forma para insertar data desde atras:
ResponderJose Noriegahace 4 meses1Aporte | Reto
Les comparto mi segunda versión del reto, después de un tiempo de dejar abandonado el curso 😅
Hice los features para que la linked list pueda manipularse bajo las mismas características de un list.
Uso
from linked_list import SinglyLinkedList

# Se puede crear una linked list apartir de list, set o tuple.
singly_linked_list = SinglyLinkedList([1, 2, 3, 4, 5])  # 1 -> 2 -> 3 -> 4 -> 5

# Podemos acceder a los elementos de la linked list a traves de sus indices.
linked_list_slice = singly_linked_list[0:-2]  # 1 -> 2 -> 3 -> 4

print(singly_linked_list[3])  # 4

for item in singly_linked_list[::2]:
    print(item)  # 1 -> 3 -> 5


# Comparación de listas
linked_list_2 = SinglyLinkedList(1, 2, 3, 4)

print(singly_linked_list == linked_list_2)  # False
print(singly_linked_list >= linked_list_2)  # True
print(singly_linked_list <= linked_list_2)  # False
print(singly_linked_list > linked_list_2)  # True
print(singly_linked_list < linked_list_2)  # False

Código
from pdb import set_trace
from typing import Any
from typing import Optional
from typing import Iterator
from typing import Union

from node import Node


class SinglyLinkedList:
    """Singly Linked List"""

    _tail: Optional[Node]
    _size: int

    def __init__(self, *items) -> None:
        """Initialize an empty list

        Args:
            *items: Items to be added to the list.
        """

        self._head = None
        self._size = 0

        if len(items) == 1 and isinstance(items[0], (list, tuple, set)):
            items = items[0]

        for item in items:
            self.append(item)

    def append(self, value: Any):
        """Appends a new value at the end of the list.

        Time complexity: O(n) where n is the length of the nodes.
        """

        node = Node(value)

        if self._head is None:
            self._head = node

        else:

            pointer = self._head

            while pointer.next is not None:
                pointer = pointer.next

            pointer.next = node

        self._size += 1


    @property
    def size(self):
        """Returns the size of the linked list.

        Time complexity: O(1)
        """

        return self._size

    def delete(self, target: object):
        """Deletes an item from the list.

        Time complexity: O(n)
        """

        current = self._head
        previous = current

        while current is not None:
            if current.value == target:
                if current == self._head:
                    self._head = current.next
                else:
                    previous.next = current.next

                self.size -= 1
                break

            previous = current
            current = current.next

    def search(self, target: object) -> bool:
        """Check if the provided value is in the list.

        Time complexity: O(n) where n is the length of the nodes.

        Args:
            target: Value to be searched inside the list.

        Returns:
            bool: True if found. False otherwise.
        """

        return target in self

    def clear(self):
        """Clears the list.

        Removes all the items inside the list.
        """

        self._head = None
        self.size = 0

    def __len__(self):
        """Returns the size of the linked list when using the built-in function len."""
        return self._size

    def __iter__(self) -> Iterator[Node]:
        """Allows to iterate over the list using a generator.

        Time complexity: O(n)
        """

        if self._head is not None:
            pointer = self._head

            while pointer is not None:
                val = pointer
                pointer = pointer.next

                yield val

        return None

    def __contains__(self, target: object) -> bool:
        """Check if a value is in the list when using the 'in' operator.

        Time complexity: O(n) where n is the length of the nodes.
        """

        output = False

        pointer = self._head

        while pointer is not None and pointer.value != target and pointer.next is not None:
            pointer = pointer.next

        if pointer is not None and pointer.value == target:
            return target

        return output

    def __reversed__(self) -> Iterator[Node]:
        """Returns a new instance of the linked list but reversed when using the 'reversed' built-in function.

        Time complexity: O(n) where n is the length of the nodes.
        """

        new_list = SinglyLinkedList()

        if self._head is not None:

            current_node: None = self._head
            remaining_values: Optional[Node] = self._head.next

            # Set the head next value to None beacuse now it will be the tail
            current_node.next = None

            while remaining_values is not None:

                temp_ref = current_node
                current_node: Node = remaining_values
                remaining_values = current_node.next

                current_node.next = temp_ref

            new_list._head = current_node

        return new_list

    def __getitem__(self, index: Union[int, slice]):
        """Get item with the index or slice from the linked list.

        Time Complexity: O(n) where n is the length of the nodes.

        Returns:
            Any: Item with the index or slice.
        """

        if isinstance(index, slice):

            items = list(self)[index]
            new_list = SinglyLinkedList(items)

            return new_list

        if index > self.size - 1:
            raise IndexError('Index out of range')

        if index < 0:
            index = self._size + index

        for idx, value in enumerate(self):
            if index == idx:
                return value

        return None


    def __lt__(self, other: 'SinglyLinkedList') -> bool:
        """Check if the list is less than the other list.

        Time Complexity: O(n) where n is the length of the nodes of the list with less nodes.

        Args:
            other (SinglyLinkedList): Other list to be compared.

        Returns:
            bool: True if the list is equals to the other list. False otherwise.
        """

        if self.size >= other.size:
            return False


        for self_item, other_item in zip(self, other):
            if self_item.value != other_item.value:
                return False

        return True

    def __le__(self, other: 'SinglyLinkedList') -> bool:
        """Check if the list is less than or equals to the other list.

        Time Complexity: O(n) where n is the length of the nodes of the list with less nodes.

        Args:
            other (SinglyLinkedList): Other list to be compared.

        Returns:
            bool: True if the list is equals to the other list. False otherwise.
        """

        if self.size > other.size:
            return False

        for self_item, other_item in zip(self, other):
            if self_item.value != other_item.value:
                return False

        return True

    def __eq__(self, other: 'SinglyLinkedList') -> bool:
        """Check if the list is equal to the other list.

        Time Complexity: O(n) where n is the length of the nodes of the list with less nodes.

        Args:
            other (SinglyLinkedList): Other list to be compared.

        Returns:
            bool: True if the list is equals to the other list. False otherwise.
        """

        if self._size != other.size:
            return False

        for self_item, other_item in zip(self, other):
            if self_item.value != other_item.value:
                return False

        return True


    def __ge__(self, other: 'SinglyLinkedList') -> bool:
        """
        Check if the list is greater or equal than the other list.

        Time Complexity: O(n) where n is the length of the nodes of the list with less nodes.

        Args:
            other (SinglyLinkedList): Other list to be compared.

        Returns:
            bool: True if the list is greater or equal than the other list. False otherwise.
        """

        if self._size < other._size:
            return False

        for self_item, other_item in zip(self, other):
            if self_item.value != other_item.value:
                return False

        return True

    def __gt__(self, other: 'SinglyLinkedList') -> bool:
        """
        Check if the list is greater than the other list.

        Time Complexity: O(n) where n is the length of the nodes of the list with less nodes.

        Args:
            other (SinglyLinkedList): Other list to be compared.

        Returns:
            bool: True if the list is greater than the other list. False otherwise.
        """

        if self.size <= other.size:
            return False

        for self_item, other_item in zip(self, other):
            if self_item.value != other_item.value:
                return False

        return True

Propuesta anterior: https://platzi.com/comentario/2616441/Ver másResponderMANUEL G. PINEDAhace 5 meses1Reto…
from linkedList import SinglyLinkedList
from customArray import Array

def arrayToList(array: Array) -> SinglyLinkedList:
    linkedList = SinglyLinkedList()

    for item in iter(array):
        linkedList.append(item)
    return linkedList


def run():
    array = Array(10) #Crates an Array object
    array.randomitems(0,10) # Fill the array with random int numbers between 0 ans 10
    print(str(array)) # Prints the str format representation of the array
    numbers = arrayToList(array) #Turn the array into a SinglyLinkedList
    numbers.str() # Print the str format representation of the SinglyLinkedList

if __name__=="__main__":
    run()

Implementación del método str de la clase SinlyLinkedList
    def str(self):
        current = self.tail
        items = ""

        while current:        
            items += str(current.data) + " "
            current = current.next
        print(items)
Ver másResponderMANUEL G. PINEDAhace 5 meses1Implementación de caso de uso para def search(self, data)
Para en caso de no encontrar el elemnto.
    def search(self, data):
        found=False
        for node in self.iter():
            if data == node:
                print(f'Data: "{data}" found!')
                found = True
        if found==False:
            print(f'Sorry...Data: "{data}" NOT found!')   
Ver másResponderAaron Contreras Garibayhace 6 meses1Reto:
# Reto
    print("RETO")
    values = [i for i in range(1, 15)]
    values = [Node(i) for i in values]
    numbers = SingleLinkedList()
    for value in values:
        numbers.append(value.data)
    for number in numbers.iter():
        print(number)
    print(f"size {numbers.list_size()=}")
    print(numbers.search(5))
    print(numbers.search("potato"))
Ver másResponderAaron Contreras Garibayhace 6 meses1Una buena forma de usar un format-string para ver los datos es:
data = 'spam'
print(f"{data=} found)
# imprime
data='spam' found
ResponderAaron Contreras Garibayhace 6 meses1El zen de python 🐱‍👤
>>> import this
The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
Ver másResponderPedro Alvarado Garciahace 6 meses1Mi implementación:
ResponderEdinson Requenahace 7 meses1Mi código de la clase, hice algunas cosas ligeramente distintas:
from node import Node


class SingleLinkedList:
    """Create a Single Linked List """

    def __init__(self):
        """Initialize a single linked list"""

        self.head = None
        self.tail = None
        self.size = 0

    def append(self, data):
        """Add a new node to the end of the list"""

        new_node = Node(data)
        if self.head is None:
            self.head = new_node
            self.tail = new_node
        else:
            self.tail.next = new_node
            self.tail = new_node
        self.size += 1

    def size(self):
        """Return the size of the list"""

        return str(self.size)

    def iter(self):
        """Iterate over the list"""

        current = self.head
        while current:
            yield current
            current = current.next

    def delete(self, data):
        """Delete a node from the list"""

        current = self.head
        previous = None
        found = False
        while current and found is False:
            if current.data == data:
                found = True
            else:
                previous = current
                current = current.next
        if current is None:
            raise ValueError("Data not in list")
        if previous is None:
            self.head = current.next
        else:
            previous.next = current.next
        self.size -= 1

    def search(self, data):
        """Search for a node in the list"""

        current = self.head
        found = False
        while current and found is False:
            if current.data == data:
                print(f'Data {data} was found')
                found = True
            else:
                current = current.next
        if current is None:
            raise ValueError("Data not in list")
        return current

    def clear(self):
        """Clear the list"""
        self.head = None
        self.tail = None
        self.size = 0


if __name__ == '__main__':

    sll = SingleLinkedList()
    sll.append(1)
    sll.append('hey')
    sll.append(3.14)
    sll.append('Hola')
    current = sll.tail

    for node in sll.iter():
        print(node.data)

    print(sll.size)
    sll.search('hey').data
    sll.search('545').data

Ver másResponderMitchell Miranohace 8 meses1Aqui cumpliendo con el reto
ResponderJulio Cardenashace 9 meses1mi version de search
def search(self, data):
        result = 'not found'
        for node in self.iter():
            if data == node:
                result = 'found' 
        print(f'Data {data} {result}!!')
Ver másResponderValentin Francisco Blancohace 9 meses1Mi solución al reto:
.
.
ResponderJose Noriegahace 9 meses1Aporte | Reto
Les comparto mi solución del problema, traté de hacer las mejores soluciones que se me ocurrieron al momento basado en la teoría de la complejidad computacional.

PD: El método de delete no me termina de gustar, me gustaría saber si tienen alguna sugerencia
Código
class Node:
    def __init__(self, value, next = None) -> None:
        self.value = value
        self.next = next


class SinglyLinkedList:
    """Singly Llinked list."""

    def __init__(self) -> None:
        """Initalize the linked list.
        
        Time complexity: O(1)
        """
        self._head = None
        self._tail = None
        self._size = 0

    @property
    def size(self):
        return self._size

    def append(self, value):
        """Adds a new item to the current linked list instance.

        Time complexity: O(1)
        """

        node = Node(value)

        if self._head is None:
            self._head = node

        elif self._tail is None:
            self._tail = Node(value)
            self._head.next = self._tail
        else:
            self._tail.next = Node(value)
            self._tail = self._tail.next

        self._size += 1

    def iter(self):
        """Returns an iterator object of the current instance.
        
        Time complexity: O(n)
        """
        currentRef = self._head

        while currentRef is not None:
            value = currentRef.value
            currentRef = currentRef.next
            yield value

    def __iter__(self):
        """Returns an iterator object.
        
        Time complexity: O(n)
        """
        return self.iter()

    def delete(self, data):
        """Removes the node that matches with the provided data.
        
        Time complexity: O(n)
        """

        deleted = False
        currentRef = self._head
        previousRef = None

        while currentRef is not None:
            if currentRef.value == data:
                if previousRef is not None:
                    previousRef.next = currentRef.next
                else:
                    self._head = currentRef.next

                deleted = True
                self._size -= 1
                break

            previousRef = currentRef
            currentRef = currentRef.next

        # Find the new tail
        currentRef = self._head
        while currentRef is not None and currentRef.next is not None:
            currentRef = currentRef.next
        self._tail = currentRef

        return deleted

    def search(self, value) -> bool:
        """Returns True if the provided value is found in the current linked list instance.
        
        Time complexity: O(n)
        """

        result = False

        for item in self:
            if item == value:
                result = True
                break
        return result

    def clear(self):
        """Resets the current linked list instance.
        
        Time complexity: O(1)
        """
        self._size = 0
        self._head = None
        self._tail = None

    @staticmethod
    def from_array(source: list):
        """Creates a new instance of a singly linked list from a simple python list.
        
        Time complexity: O(n)
        """

        _list = SinglyLinkedList()

        for item in source:
            _list.append(item)
        
        return _list

    def __str__(self) -> str:
        return repr(list(self))

    def __repr__(self) -> str:
        return str(self)

Ver másResponderKevin Tovarhace 9 meses1from linked_list import SinglyLinkedList
from array import Array


def run():
    matrix = Array(6)
    matrix.__remitem__(matrix)
    nodo = SinglyLinkedList()

    for i in range(len(matrix)):
        nodo.append(matrix[i])

    current = nodo.tail

    while current:
        print(current.data)
        current = current.next

    nodo.search(100)


if __name__ == '__main__':
    run()
Ver másResponderMario Castro Martínezhace 9 meses1Mi solución al reto 3:
ResponderVictor Parrahace 10 meses1Hice un método importando la clase array de las clases anteriores

Y le hice un cambio al metodo search
ResponderJheyshon Michel Vilchez Canchucajahace 10 meses1Reto:
from node import Node
from my_array import Array
from linked_list import SinglyLinkedList

# Metodo para agregar array en la clase SinglyLinkedList
#def addarray(self, arr):
#    for ar in arr.__iter__():
#        self.append(ar)

arr = Array(5)
arr.__setfullranitems__(max=50)
linked = SinglyLinkedList()
linked.addarray(arr)
for lin in linked.iter():
    print(lin)
Ver másResponderChristian Molina Vázquezhace 10 meses1Le cambie unas pequeñas cosas a la clase de SinglyLinkedList
Agregue la propiedad self.head para conocer su valor, así como también le agregue la condición al metodo search para cuando no encuentre el dato.
También le agregue el metodo str para imprimir los valores de la lista.
from node import Node


class SinglyLinkedList:
    def __init__(self):
        self.tail = None
        self.head = None
        self.size = 0

    def append(self, data):
        node = Node(data)

        if self.tail == None:
            self.tail = node
        else:
            current = self.tail

            while current.next:
                current = current.next

            current.next = node
            self.head = current.next

        self.size += 1

    def size(self):
        return str(self.size)

    def iter(self):
        current = self.tail

        while current:
            val = current.data
            current = current.next
            yield val

    def delete(self, data):
        current = self.tail
        previous = self.tail

        while current:
            if current.data == data:
                if current == self.tail:
                    self.tail = current.next
                else:
                    previous.next = current.next
                    self.size -= 1
                    return current.data

            previous = current
            current = current.next

    def search(self, data):
        for node in self.iter():
            if data == node:
                print(f"Data {data} found!")
                break
        else:
            print(f"Data {data} don't found!")

    def clear(self):
        self.tail = None
        self.head = None
        self.size = 0
    
    def __str__(self):
        result = ""
        for node in self.iter():
            result += f'{node} '
        
        return result


Probando los cambios
#Conocer el head
primero = words.head
print(primero.data)


# Don't found
words.search("egg")
words.search("juice")

#Imprimir la lista
print(words)

Salida
spam
Data egg found!        
Data juice don't found!
egg ham spam 

En cuanto al reto, utilice la clase array creada anteriormente, y la clase nodo, para pasar los datos del array a la linkedList
#Reto 
from array_clase import Array
from node import Node
array= Array(10,int())
array.__random_fill__(0,9)
linkedList = SinglyLinkedList()

head = None
for data in array:
    head = Node(data, head)

while head != None:
    linkedList.append(head.data)
    head = head.next

print(linkedList)

Salida
6 3 9 5 3 8 8 2 1 1 
Ver másResponderFranco Maximiliano Mirandahace 10 meses1comparto mi version del reto, si hay algun error porfavor me avisan
from array1 import Array
from linked_list import SinglyLinkedList

# RETO

if __name__ == "__main__":
    
    num_array = Array(5)

    for i in range(5):
        num_array.__setitem__(i,i)
    
    num_linked_list = SinglyLinkedList()

    for num in num_array.__iter__():
        num_linked_list.append(num)
    
    current = num_linked_list.tail

    while current:
        print(current.data)
        current = current.next
Ver másResponderEduardo Kiriakos Piazzahace 10 meses1Hice el reto agregando un método a la clase SinglyLinkedList
def add_array(self, array):
    for element in array:
        node = Node(element)

        if self.tail == None:
            self.tail = node
        else:
            current = self.tail
            while current.next:
                current = current.next
            current.next = node
            
        self.size += 1
```

## Operaciones en single linked structures

El código divido por comentarios, y con sus respectivas salidas.

```python
from node import Node

# * Creación de los nodos enlazados (linked list)
head = None
for count in range(1,6):
    head = Node(count, head)

# * Recorrer e imprimir valores de la lista
probe = head
print("Recorrido de la lista:")
while probe != None:
    print(probe.data)
    probe = probe.next
Recorrido de la lista:
5
4
3
2
1
# * Busqueda de un elemento
probe = head
target_item = 2
while probe != None and target_item != probe.data:
    probe = probe.next

if probe != None:
    print(f'Target item {target_item} has been found')
else:
    print(f'Target it
Target item 2 has been found
# * Remplazo de un elemento
probe = head
target_item = 3
new_item = "Z"

while probe != None and target_item != probe.data:
    probe = probe.next

if probe != None:
    probe.data = new_item
    print(f"{new_item} replace the old value in the node number {target_item}")
else:
    print(f"The target item {target_item} is not in the linked list")
Z replace the old value in the node number 3

Recorrido de la lista:
5
4
Z
2
1
# * Insertar un nuevo elemento/nodo al inicio(head)
head = Node("F", head)
Recorrido de la lista:
F
5
4
Z
2
1
# * Insertar un nuevo elemento/nodo al final(tail)
new_node = Node("K")
if head is None:
    head = new_node
else:
    probe = head
    while probe.next != None:
        probe = probe.next
    probe.next = new_node
Recorrido de la lista:
F
5
4
Z
2
1
K
# * Eliminar un elmento/nodo al inicio(head)
removed_item = head.data
head = head.next
print("Removed_item: ",end="")
print(removed_item)
Removed_item: F

Recorrido de la lista:
5
4
Z
2
1
K
# * Eliminar un elmento/nodo al final(tail)
removed_item = head.data
if head.next is None:
    head = None
else:
    probe = head
    while probe.next.next != None:
        probe = probe.next
    removed_item = probe.next.data
    probe.next = None

print("Removed_item: ",end="")
print(removed_item)
Removed_item: K

Recorrido de la lista:
5
4
Z
2
1
# * Agregar un nuevo elemento/nodo por "indice" inverso(Cuenta de Head - Tail)
# new_item = input("Enter new item: ")
# index = int(input("Enter the position to insert the new item: "))
new_item = "10"
index = 3

if head is None or index <= 0:
    head = Node(new_item, head)
else:
    probe = head
    while index > 1 and probe.next != None:
        probe = probe.next
        index -= 1
    probe.next = Node(new_item, probe.next)
# * Agregar un nuevo elemento/nodo por "indice" inverso(Cuenta de Head - Tail)

Recorrido de la lista:
5
4
Z
10
2
1
# * Eliminar un nuevo elemento/nodo por "indice" inverso(Cuenta de Head - Tail)
index = 3

if head is None or index <= 0:
    removed_item = head.data
    head = head.next
    print(removed_item)
else:
    probe = head
    while index > 1 and probe.next.next != None:
        probe = probe.next
        index -= 1
    removed_item = probe.next.data
    probe.next = probe.next.next

    print("Removed_item: ",end="")
    print(removed_item)
Removed_item: 10
Recorrido de la lista:
5
4
Z
2
1
```

**Reemplazo de un elemento por indice**

![SLL_replace_index.png](https://static.platzi.com/media/user_upload/SLL_replace_index-4df7dc8b-9ca7-49a2-83d3-5bdefbd5143d.jpg)

## Operaciones a detalle



LinkedList con todos los métodos de List.
Implementé todos los métodos de list a mi clase de linked list, de forma que pueden usar los mismos métodos y sintaxis que usan para manipular una lista normal.
Incluyendo el Código
**SinglyLinkedList**

```python
from typing import Any
from typing import Optional
from typing import Iterator
from typing import Union
from typing import Iterable

from node import Node


class SinglyLinkedList:
    """Singly Linked List"""
```

```python
_head: Optional[Node]
_tail: Optional[Node]
_size: int

def __init__(self, *items) -> None:
    """Initialize an empty list

    Args:
        *items: Items to be added to the list.
    """

    self._head = None
    self._tail = None
    self._size = 0

    if len(items) == 1 and isinstance(items[0], (Iterable, SinglyLinkedList)):
        items = items[0]

    for item in items:
        self.append(item)

@property
def size(self):
    """Returns the size of the linked list.

    Time complexity: O(1)
    """

    return self._size

def clear(self):
    """Clears the list.

    Removes all the items inside the list.
    """

    self._head = None
    self._size = 0

def copy(self):
    """Returns a copy of the list.

    Time Complexity: O(1)
    """

    new_list = SinglyLinkedList()

    if self._head is not None:
        # Caution: This is a shallow copy
        new_list._head = self._head
        new_list._size = self._size

    return new_list

def depthcopy(self) -> 'SinglyLinkedList':
    """Returns a copy of the list and its nodes.

    Time complexity: O(n)

    Returns:
        SinglyLinkedList: Copy of the list.
    """

    new_list = SinglyLinkedList()

    for item in self:
        new_list.append(item.value)

    return new_list

def append(self, value: Any):
    """Appends a new value at the end of the list.

    Time complexity: O(1)
    """

    node = Node(value)

    if self._head is None:
        self._head = node
        self._tail = self._head

    else:
        self._tail.next = Node(value)
        self._tail = self._tail.next

    self._size += 1

def extend(self, other: Iterable):
    """Extends the current list with the provided list.

    Time complexity: O(n) where n is the length of the list to extend.

    Args:
        other: List to be extended.
    """

    for item in other:
        if isinstance(other, Node):
            self.append(item.value)

        else:
            self.append(item)

def pop(self, index: int = -1) -> Any:
    """Removes an item from the list.

    Raises IndexError if the list is empty or index is out of range.

    Time complexity: O(n) where n is the length of the nodes.

    Args:
        index: Index of the item to be removed.

    Returns:
        Any: Item removed.
    """

    if index < 0:
        index = self._size + index

    if self._head is None or index > self._size - 1:
        raise IndexError('Index out of range.')

    prev_pointer = self._head

    value: Node
    for idx, value in enumerate(self):
        if idx == index:
            if value == self._head:
                self._head = None
                self._tail = None
            else:
                prev_pointer.next = value.next
                self._tail = prev_pointer
                self._size -= 1

            return value

        prev_pointer = value

def index(self, target: object):
    """Return first index of value.

    Raises ValueError if the value is not present.

    Time complexity: O(n) where n is the length of nodes.

    Args:
        target: Value to look up.

    Returns:
        int: Index of the value.
    """

    item: Node
    for index, item in enumerate(self):
        if item.value == target:
            return index

    raise ValueError(f'{target} is not in list')

def count(self, target: object) -> int:
    """Counts the number of occurrences of the provided value.

    Time complexity: O(n) where n is the length of the nodes.

    Args:
        target: Value to be counted.

    Returns:
        int: Number of occurrences.
    """

    count = 0

    item: Node
    for item in self:
        if item.value == target:
            count += 1

    return count

def insert(self, index: int, value: object):
    """Insert before index.

    Time complexity: O(n)

    Args:
        index: Index to insert before.
        value: Value to insert.
    """

    if index < 0:
        index = self._size + index

    if index > self._size - 1:
        raise IndexError('Index out of range.')

    if index == 0:
        self._head = Node(value, self._head)

    else:
        prev_pointer = self._head
        item: Node
        for idx, item in enumerate(self):
            if idx == index:
                new_node = Node(value, item)
                prev_pointer.next = new_node
                break;

            prev_pointer = item

    self._size += 1

def remove(self, value):
    """Removes the first occurrence of value.

    Raises ValueError if the value is not present.

    Args:
        value: Value to be removed.
    """

    common_error = ValueError('Value not found')

    if self._head is None:
        raise common_error

    if self._head.value == value:
        self._head = self._head.next

    else:
        prev_node: Optional[Node] = None
        item: Node
        for item in self:
            if item.value == value:
                prev_node.next = item.next

                if item == self._tail:
                    self._tail = prev_node

                found = True
                break

            prev_node = item
```


```python
        if not found:
            raise common_error

    self._size -= 1

def _sort_nodes(self, head: Node, reversed: bool = False):
    """Sorts the nodes of a linked list.

    Time complexity: O(n log n)

    Args:
        head: head node of the linked list.
        reversed: True to sort in descending order, False to sort in ascending order.

    Returns:
        tuple: (head, tail) of the sorted linked list.
    """

    # If there are only 1 or 0 nodes heads that the list is already sorted.
    if head is None or head.next is None:
        return head

    temp = head
    slow = head
    fast = head

    # We need to divide the list in half.
    # The fast node will be the last node when the loop ends,
    # and the slow node will be at the middle of the list beacause
    # the fast node is traversing the list two steps at the once and the slow 1 at once.

    # temp will be the end of the first half.
    # slow will be the head of the second half.
    # fast will be the end of the second half.

    # Example:
    #  head          temp   slow                 fast
    # [1,   2,   3,   4   , 5   ,  6,  7,   8,  9]

    while fast is not None and fast.next is not None:
        temp = slow
        slow = slow.next
        fast = fast.next.next

    # Set the end of the first half
    temp.next = None

    left_half = self._sort_nodes(head, reversed=reversed)
    if isinstance(left_half, tuple):
        left_half = left_half[0]

    right_half = self._sort_nodes(slow, reversed=reversed)
    if isinstance(right_half, tuple):
        right_half = right_half[0]

    # Merge
    sorted_temp = Node(Node)
    current_node = sorted_temp

    while left_half is not None and right_half is not None:

        if (not reversed and left_half.value < right_half.value) or (reversed and left_half.value > right_half.value):
            current_node.next = left_half
            left_half = left_half.next

        else:
            current_node.next = right_half
            right_half = right_half.next

        current_node = current_node.next

    if left_half is not None:
        current_node.next = left_half
        left_half = left_half.next

    if right_half is not None:
        current_node.next = right_half
        right_half = right_half.next

    tail = current_node
    while tail.next is not None:
        tail = tail.next

    return sorted_temp.next, tail

def sort(self, reversed: bool = False):
    """Sort the list in ascending order and return None.

    The reverse flag can be set to sort in descending order.

    Time complexity: O(n log n)

    Args:
        reversed: True to sort in descending order.

    Returns:
        None
    """

    self._head, self._tail = self._sort_nodes(self._head, reversed)

    return None

def search(self, target: object) -> bool:
    """Check if the provided value is in the list.

    Time complexity: O(n) where n is the length of the nodes.

    Args:
        target: Value to be searched inside the list.

    Returns:
        bool: True if found. False otherwise.
    """

    return target in self

def iter(self):
    """Allows to iterate over the list using a generator.

    Time complexity: O(n)

    Returns:
        Iterator[Node]: Iterator over the list.
    """

    if self._head is not None:
        pointer = self._head

        while pointer is not None:
            val = pointer
            pointer = pointer.next

            yield val

    return None

def reverse(self):
    """Reverses the list.

    Time complexity: O(n)

    Returns:
        None
    """

    if self._head is not None:

        current_node: None = self._head
        remaining_values: Optional[Node] = self._head.next

        # Set the head next value to None beacuse now it will be the tail
        current_node.next = None

        while remaining_values is not None:

            temp_ref = current_node
            current_node: Node = remaining_values
            remaining_values = current_node.next

            current_node.next = temp_ref

        self._tail = self._head
        self._head = current_node

# ==========================
# Dunders
# ==========================

def __delitem__(self, index: Union[int, slice]):
    """Deletes an item from the list.

    Time complexity: O(n)

    Args:
        index: Index of the item to be removed.

    Returns:
        None
    """

    if isinstance(index, slice):
        list_items = list(self)

        del list_items[index]

        self.clear()
        for item in list_items:
            self.append(item)

    else:

        if index > self._size - 1:
            raise IndexError('Index out of range.')

        if index == 0:
            self._head = self._head.next

        else:
            prev_pointer: Optional[Node] = None

            item: Node
            for idx, item in enumerate(self):
                if idx == index:
                    prev_pointer.next = item.next

                    if item == self._tail:
                        self._tail = prev_pointer

                    break
                prev_pointer = item

        self._size -= 1

    return None

def __setitem__(self, index: Union[int, slice], value: Union[object, Iterable]):
    """Set self[key] to value.

    Time complexity: O(n)

    Args:
        index: Index to be set.
        value: Value to assign.
    """

    if isinstance(index, slice):
        if not isinstance(value, Iterable):
            raise TypeError('can only assign an iterable')

        if len(value) > 0:
            list_items = list(self)
            list_items[index] = value

            self.clear()
            for item in list_items:
                self.append(item)

    else:

        if index < 0:
            index = self._size + index

        if index > self._size - 1:
            raise IndexError('list assignment index out of range')

        if index == 0:
            self._head.value = value

        else:
            item: Node
            for idx, item in enumerate(self):
                if idx == index:
                    item.value = value
                    break

def __add__(self, values: Iterable) -> 'SinglyLinkedList':
    """Returns a new list with the nodes of the current list a the values or nodes of the other list.

    Time complexity: O(n) where n is the length of values to add.

    Returns:
        SinglyLinkedList: Merged linked list.
    """

    new_list = self.depthcopy()

    for item in values:
        new_list.append(item)

    return new_list

def __iadd__(self, values: Iterable):
    """Appends values to the list when using the += operator.

    Time complexity: O(n) where n is the length of items to append.

    Returns:
        self
    """

    for item in values:
        self.append(item)

    return self

def __len__(self):
    """Returns the size of the linked list when using the built-in function len."""
    return self._size

def __mul__(self, times: int):
    """
    Returns a new list with the items duplicated by provided factor.

    Args:
        times: Number of times to duplicate the list.

    Returns:
        SinglyLinkedList: Duplicated list.
    """

    if times <= 0:
        return SinglyLinkedList()

    new_list = self.depthcopy()

    for _ in range(times - 1):
        new_list += self.depthcopy()

    return new_list

def __rmul__(self, times: int):
    """
    Returns a new list with the items duplicated by provided factor.

    Args:
        times: Number of times to duplicate the list.

    Returns:
        SinglyLinkedList: Duplicated list.
    """

    return self.__mul__(times)

def __imul__(self, times: int):
    """Duplicates the current list items by the provided factor and appends them to the end.

    Time complexity: O(a * b) where a is the number of duplicates to add and b is the length of the list.

    Args:
        times: Number of times to duplicate the list.

    Returns:
        self
    """

    if times <= 0:
        self.clear()

    else:

        base = self.depthcopy()

        for _ in range(times - 1):
            self.extend(base.depthcopy())

    return self

def __iter__(self) -> Iterator[Node]:
    """Allows to iterate over the list using a generator.

    Time complexity: O(n)

    Returns:
        Iterator[Node]: Iterator over the list.
    """
    return self.iter()

def __contains__(self, target: object) -> bool:
    """Check if a value is in the list when using the 'in' operator.

    Time complexity: O(n) where n is the length of the nodes.
    """

    output = False

    pointer = self._head

    while pointer is not None and pointer.value != target and pointer.next is not None:
        pointer = pointer.next

    if pointer is not None and pointer.value == target:
        return target

    return output

def __getitem__(self, index: Union[int, slice]):
    """Get item with the index or slice from the linked list.

    Time Complexity: O(n) where n is the length of the nodes.

    Returns:
        Any: Item with the index or slice.
    """

    if isinstance(index, slice):

        items = list(self)[index]
        new_list = SinglyLinkedList(items)

        return new_list

    if index > self.size - 1:
        raise IndexError('Index out of range')

    if index < 0:
        index = self._size + index

    for idx, value in enumerate(self):
        if index == idx:
            return value

    return None

def __lt__(self, other: 'SinglyLinkedList') -> bool:
    """Check if the list is less than the other list.

    Time Complexity: O(n) where n is the length of the nodes of the list with less nodes.

    Args:
        other (SinglyLinkedList): Other list to be compared.

    Returns:
        bool: True if the list is equals to the other list. False otherwise.
    """

    if len(self) >= len(other):
        return False

    for self_item, other_item in zip(self, other):
        if self_item.value != Node._parse_other_value(other_item):
            return False

    return True

def __le__(self, other: 'SinglyLinkedList') -> bool:
    """Check if the list is less than or equals to the other list.

    Time Complexity: O(n) where n is the length of the nodes of the list with less nodes.

    Args:
        other (SinglyLinkedList): Other list to be compared.

    Returns:
        bool: True if the list is equals to the other list. False otherwise.
    """

    if len(self) > len(other):
        return False

    for self_item, other_item in zip(self, other):
        if self_item.value != Node._parse_other_value(other_item):
            return False

    return True

def __eq__(self, other: 'SinglyLinkedList') -> bool:
    """Check if the list is equal to the other list.

    Time Complexity: O(n) where n is the length of the nodes of the list with less nodes.

    Args:
        other (SinglyLinkedList): Other list to be compared.

    Returns:
        bool: True if the list is equals to the other list. False otherwise.
    """

    if len(self) != len(other):
        return False

    for self_item, other_item in zip(self, other):
        if self_item.value != Node._parse_other_value(other_item):
            return False

    return True

def __ge__(self, other: 'SinglyLinkedList') -> bool:
    """
    Check if the list is greater or equal than the other list.

    Time Complexity: O(n) where n is the length of the nodes of the list with less nodes.

    Args:
        other (SinglyLinkedList): Other list to be compared.

    Returns:
        bool: True if the list is greater or equal than the other list. False otherwise.
    """

    if len(self) < len(other):
        return False

    for self_item, other_item in zip(self, other):
        if self_item.value != Node._parse_other_value(other_item):
            return False

    return True

def __gt__(self, other: 'SinglyLinkedList') -> bool:
    """
    Check if the list is greater than the other list.

    Time Complexity: O(n) where n is the length of the nodes of the list with less nodes.

    Args:
        other (SinglyLinkedList): Other list to be compared.

    Returns:
        bool: True if the list is greater than the other list. False otherwise.
    """

    if len(self) <= len(other):
        return False

    for self_item, other_item in zip(self, other):
        if self_item.value != Node._parse_other_value(other_item):
            return False

    return True
```

```python
# Node
class Node:
    """
    Node class for a linked list.
    """
def __init__(self, value, next=None) -> None:
    """
    Initialize a node with a value and a next node.

    Args:
        value: The value of the node.
        next: The next node.
    """

    self.value = value

    self.next = next

def copy(self) -> 'Node':
    """
    Return a copy of the node.

    Returns:
        A copy of the node.
    """

    return Node(self.value, self.next)

@staticmethod
def _parse_other_value(other):
    if isinstance(other, Node):
        return other.value

    return other

def __lt__(self, other: 'Node') -> bool:
    """Check if the current value is less than the other value.

    Args:
        other: Node to be compared.

    Returns:
        bool: True if the current value is less than the other one. False otherwise.
    """

    return self.value < self._parse_other_value(other)

def __le__(self, other: 'Node') -> bool:
    """Check if the current value is less than or equals to the other value.

    Args:
        other: Node to be compared.

    Returns:
        bool: True if the current value is less than or equals to the other one. False otherwise.
    """

    return self.value <= self._parse_other_value(other)

def __eq__(self, other: 'Node') -> bool:
    """Check if the current value is equals to the other value.

    Args:
        other: Node to be compared.

    Returns:
        bool: True if the current value is equals to the other one. False otherwise.
    """

    return self.value == self._parse_other_value(other)

def __ge__(self, other: 'Node') -> bool:
    """Check if the current value is greater than or equals to the other value.

    Args:
        other: Node to be compared.

    Returns:
        bool: True if the current value is greater than or equals to the other one. False otherwise.
    """

    return self.value >= self._parse_other_value(other)

def __ge__(self, other: 'Node') -> bool:
    """Check if the current value is greater than the other value.

    Args:
        other: Node to be compared.

    Returns:
        bool: True if the current value is greater than the other one. False otherwise.
    """

    return self.value > self._parse_other_value(other)

def __str__(self) -> str:
    return str(self.value)
```

## Circular linked list

Las listas enlazadas circulares son un tipo de lista enlazada en la que el último nodo apunta hacia el headde la lista en lugar de apuntar None. Esto es lo que los hace circulares.

Las listas enlazadas circulares tienen bastantes casos de uso interesantes:

- Dar la vuelta al turno de cada jugador en un juego multijugador
- Gestionar el ciclo de vida de la aplicación de un sistema operativo determinado
- Implementando un montón de Fibonacci

Así es como se ve una lista enlazada circular:

![img](https://files.realpython.com/media/Group_22.cee69a15dbe3.png)


Una de las ventajas de las listas enlazadas circulares es que puede recorrer toda la lista comenzando en cualquier nodo. Dado que el último nodo apunta al headde la lista, debe asegurarse de dejar de atravesar cuando llegue al punto de partida. De lo contrario, terminarás en un bucle infinito.

En términos de implementación, las listas enlazadas circulares son muy similares a la lista enlazada individualmente. La única diferencia es que puede definir el punto de partida cuando recorre la lista:

```python
class CircularLinkedList:
    def __init__(self):
        self.head = None
        
def traverse(self, starting_point=None):
    if starting_point is None:
        starting_point = self.head
    node = starting_point
    while node is not None and (node.next != starting_point):
        yield node
        node = node.next
    yield node

def print_list(self, starting_point=None):
    nodes = []
    for node in self.traverse(starting_point):
        nodes.append(str(node))
    print(" -> ".join(nodes))
```


Atravesar la lista ahora recibe un argumento adicional starting_point, que se usa para definir el inicio y (debido a que la lista es circular) el final del proceso de iteración. Aparte de eso, gran parte del código es el mismo que teníamos en nuestra LinkedListclase.

## Double linked list

Las listas doblemente enlazadas se diferencian de las listas enlazadas individualmente en que tienen dos referencias:

El previous campo hace referencia al nodo anterior.
El next campo hace referencia al siguiente nodo.

El resultado final se ve así:

![img](https://files.realpython.com/media/Group_23.a9df781f6087.png)

```python
class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.previous = None
```

Este tipo de implementación le permitiría atravesar una lista en ambas direcciones en lugar de solo atravesar usando next. Puede utilizar next para avanzar y previous retroceder.
En términos de estructura, así es como se vería una lista doblemente enlazada:

![img](https://files.realpython.com/media/Group_21.7139fd0c8abb.png)

# 4. Stacks

## ¿Qué son stacks?

Las pilas (stacks) son una estructura de datos donde tenemos una colección de elementos, y sólo podemos hacer dos cosas:

- añadir un elemento al final de la pila

- sacar el último elemento de la pila

Una manera común de visualizar una pila es imaginando una torre de panqueques, donde una vez que ponemos un panqueque encima de otro, no podemos sacar el anterior hasta que se hayan sacado todos los que están encima.

![img](https://miro.medium.com/max/700/1*qcjbuSJ4rf7VcFFFoDbc5Q.png)

A pesar de su simplicidad, las pilas son estructuras relativamente comunes en ciertas áreas de la computación, en especial para implementar o simular evaluación de expresiones, recursión, scope, …

**LIFO (Last In First Out)**

Las pilas son estructuras de tipo LIFO, lo cual quiere decir que el último elemento añadido es siempre el primero en salir.

De alguna forma, podemos decir que una pila es como si fuera una lista o array, en el sentido que es una colección, pero a diferencia de los arrays y otras colecciones, en las pilas solo accedemos al elemento que esté “encima de la pila”, el último elemento. Nunca manipulamos ni accedemos a valores por debajo del último elemento.

## Crear un stack

**Reto**

```python
class Array:
    def __init__(self, capacity, fill_value=None):
        self.items = list()
        for _ in range(capacity):
            self.items.append(fill_value)

    def __len__(self):
        return len(self.items)

    def __str__(self):
        return str(self.items)

    def __iter__(self):
        return iter(self.items)

    def __getitem__(self, index):
        return self.items[index]

    def __setitem__(self, index, new_item):
        self.items[index] = new_item


class StackArray(Array):
    def __init__(self, capacity, fill_value=None):
        super().__init__(capacity, fill_value)
        self.capacity = capacity
        self.top = None
        self.size = 0

    def __str__(self):
        result = ''
        if self.size <= 0:
            return 'Stack empty'
        for i in self.items:
            if i != None:
                result += f'{i} '
        return result

    def push(self, data):
        if self.size == self.capacity:
            print('Stack full')
            return -1

        for i, value in enumerate(self.items):
            if value == None:
                self.items[i] = data
                self.top = data
                self.size += 1
                break

    def pop(self):
        for i in range(self.capacity-1, -1, -1):
            if self.items[i] != None:
                self.items[i] = None
                self.size -= 1
                if i > 0:
                    self.top = self.items[i-1]
                else:
                    self.top = None
                break

    def peek(self):
        if self.top:
            return self.top
        else:
            return "The stack is empty"
    
    def clear(self):
        self.__init__(self.capacity)
    
    def search(self, target_item):
        if self.size <= 0:
            print("The stack is empty")
        else:
            if target_item in self.items:
                print(f'Target item {target_item} has been found')
                return self.items.index(target_item)
            else:
                print(f'Target item {target_item} is not in stack')
                return -1

    def iter(self):
        for element in self.items:
            if element != None:
                yield element
```

### Código

#### Stack con nodos

```python
from typing import Optional, Union
from typing import Iterable

from node import Node


class Stack:
    """A simple stack implementation."""

    _size: int
    _top: Optional[Node]

    def __init__(self, *items):
        """Initializes the stack."""

        self._top = None
        self._size = 0

        if len(items) > 0 and isinstance(items[0], Iterable):
            items = items[0]

        for item in items:
            self.push(item)

    def is_empty(self):
        """Checks if the stack is empty.

        Returns:
            bool: True if the stack is empty, False otherwise.
        """

        return self._size <= 0

    def clear(self):
        """Clears the stack."""

        self._top = None
        self._size = 0

    def copy(self):
        """Returns a copy of the stack.

        Caution: This method returns a shallow copy.

        Returns:
            Stack: A copy of the stack.
        """

        new_stack = Stack()

        new_stack._top = self._top

        return new_stack

    def depthcopy(self):
        """Returns a deep copy of the stack.

        Returns:
            Stack: A deep copy of the stack.
        """

        new_stack = Stack()

        new_stack._top = self._top.depthcopy()

        new_stack._size = self._size

        return new_stack

    def extend(self, other: 'Stack'):
        """Extends the current stack with the items of the other stack.

        Time complexity: O(n) where n is the length of the other stack.

        Args:
            other: The other stack to extend the current stack with.
        """

        if other._top is None:
            return None

        bottom_node = other._top

        while bottom_node.next is not None:
            bottom_node = bottom_node.next

        bottom_node.next = self._top

        self._top = other._top

        self._size += other._size

        return None

    def peek(self, raw: bool = False) -> Union[None, Node, object]:
        """Returns the top item of the stack.

        Raises IndexError if the stack is empty.

        Args:
            raw: If True returns the Node, otherwise it will return the node value.

        Returns:
            object: The top item of the stack.
        """

        if not self._top:
            raise IndexError('The stack is empty.')

        if raw:
            return self._top

        return self._top.value

    def push(self, value: object):
        """Pushes an item onto the stack.

        Time complexity: O(1)

        Args:
            item: The item to be pushed onto the stack.
        """

        self._top = Node(value, self._top)
        self._size += 1

    def pop(self, raw: bool = False):
        """Pops an item off the stack.

        Raises IndexError if the stack is empty.

        Time complexity: O(1)

        Args:
            raw: If True returns the Node, otherwise it will return the node value.

        Returns:
            object: The item that was popped off the stack.
        """

        if self._top is None:
            raise IndexError('The stack is empty.')

        temp = self._top

        self._top = temp.next

        self._size -= 1

        if raw:
            return temp

        return temp.value

    def __len__(self):
        """Returns the length of the stack.

        Time complexity: O(1)

        Returns:
            int: The length of the stack.
        """

        return self._size

    def __str__(self) -> str:
        """Returns a string representation of the stack.

        Time complexity: O(n)
        Space complexity: O(n)

        Returns:
            str: A string representation of the stack.
        """

        items = list(self)

        return str(items)

    def __contains__(self, item: object) -> bool:
        """Checks if the stack contains an item.

        Args:
            item: The item to check if the stack contains.

        Returns:
            bool: True if the stack contains the item, False otherwise.
        """

        pointer = self._top
        while pointer is not None:
            if pointer.value == item:
                return True

            pointer = pointer.next

        return False

    def itervalues(self) -> Iterable:
        """Returns an iterator over the values of the stack.

        Returns:
            iterator: An iterator over the values of the stack.
        """

        pointer = self._top

        while pointer is not None:
            yield pointer
            pointer = pointer.next

    def __iter__(self) -> Iterable:
        """Returns an iterator over the stack.

        Returns:
            iterator: An iterator over the stack.
        """

        return self.itervalues()

    def __add__(self, other):
        """Returns a new stack containing the items of both stacks.

        Args:
            other: The other stack to add to the current stack.

        Returns:
            Stack: A new stack containing the items of both stacks.
        """

        new_stack = self.depthcopy()
        new_stack.extend(other.depthcopy())

        return new_stack
```

#### Stack con listas

```python
from typing import Optional, Union
from typing import Iterable


class Stack:
    """A simple stack implementation with list."""

    _items: list

    def __init__(self, *items):
        self._items = list(*items)

    def is_empty(self) -> bool:
        return len(self._items) == 0

    def clear(self) -> None:
        self._items.clear()

    def copy(self) -> 'Stack':
        stack = Stack()

        for item in self._items:
            stack.push(item)

        return stack

    def extend(self, other: Iterable) -> None:

        for item in reversed(other):
            self.push(item)

    def peek(self) -> Optional[object]:
        if self.is_empty():
            raise ValueError('Stack is empty.')

        return self._items[-1]

    def pop(self) -> Optional[object]:
        return self._items.pop()

    def push(self, item: object) -> None:
        self._items.append(item)

    def itervalues(self) -> Iterable:
        return reversed(self._items)

    def __len__(self) -> int:
        return len(self._items)

    def __str__(self) -> str:
        return str(self._items)

    def __contains__(self, item: object) -> bool:
        return item in self._items

    def __iter__(self) -> Iterable:
        return self.itervalues()

    def __repr__(self) -> str:
        return f'Stack({self._items})'

    def __reversed__(self) -> Iterable:
        return self._items

    def __add__(self, other: Union['Stack', Iterable]) -> 'Stack':
        stack = self.copy()

        if isinstance(other, Stack):
            stack.extend(other)
        else:
            stack.extend(other)

        return stack
```

### Pruebas unitarias

#### Stack con nodos

```python
# Unitest
import unittest

# Utils
from stack_with_node import Stack


class StackWithNodeTestCase(unittest.TestCase):
    """Stack test cases."""


    def test_empty_instantiation(self):
        """Test instantiation of an empty stack."""
        stack = Stack()
        self.assertEqual(len(stack), 0)

    def test_instantiation_with_items(self):
        """Test instantiation of a stack with items."""
        stack = Stack(['a', 'b', 'c'])
        self.assertEqual(len(stack), 3)

    def test_is_empty(self):
        """Test is_empty method."""
        stack = Stack()
        self.assertTrue(stack.is_empty())

    def test_is_not_empty(self):
        """Test is_empty method."""
        stack = Stack(['a', 'b', 'c'])
        self.assertFalse(stack.is_empty())

    def test_push(self):
        """Test push method."""
        stack = Stack()
        stack.push('a')
        self.assertEqual(len(stack), 1)

    def test_peek(self):
        """Test peek method."""
        stack = Stack(['a', 'b', 'c'])
        self.assertEqual(stack.peek(), 'c')

    def test_pop(self):
        """Test pop method."""
        stack = Stack(['a', 'b', 'c'])
        self.assertEqual(stack.pop(), 'c')
        self.assertEqual(len(stack), 2)

        for a, b in zip(stack, ['b', 'a']):
            self.assertEqual(a, b)

    def test_pop_empty(self):
        """Test pop method."""
        stack = Stack()
        self.assertRaises(IndexError, stack.pop)

    def test_copy(self):
        """Test copy method."""
        stack = Stack(['a', 'b', 'c'])
        stack_copy = stack.copy()

        self.assertIsNot(stack_copy, stack)

    def test_depthcopy(self):
        """test depthcopy method."""

        stack_1 = Stack([1, 2, 3])
        stack_2 = stack_1.depthcopy()

        self.assertEqual(len(stack_2), len(stack_1))

        for a, b in zip(stack_1, stack_2):
            self.assertIsNot(a, b)

    def test_iter(self):
        """Test iter method."""
        stack = Stack(['a', 'b', 'c'])
        self.assertEqual(list(stack), ['c', 'b', 'a'])

    def test_extend(self):
        """Test extends method."""
        stack_1 = Stack([1, 2, 3])
        stack_2 = Stack([4, 5, 6])

        stack_1.extend(stack_2)
        self.assertEqual(len(stack_1), 6)

        for a, b in zip(stack_1, [6, 5, 4, 3, 2, 1]):
            self.assertEqual(a, b)

    def test_add_operator(self):
        """Test add operator."""
        stack_1 = Stack([1, 2, 3])
        stack_2 = Stack([4, 5, 6])

        stack_3 = stack_1 + stack_2

        self.assertEqual(len(stack_3), 6)

        for a, b in zip(stack_3, [6, 5, 4, 3, 2, 1]):
            self.assertEqual(a, b)
```

#### Stack con listas

```python
# Unitest
import unittest

# Utils
from stack_with_array import Stack


class StackWithNodeTestCase(unittest.TestCase):
    """Stack test cases."""


    def test_empty_instantiation(self):
        """Test instantiation of an empty stack."""
        stack = Stack()
        self.assertEqual(len(stack), 0)

    def test_instantiation_with_items(self):
        """Test instantiation of a stack with items."""
        stack = Stack(['a', 'b', 'c'])
        self.assertEqual(len(stack), 3)

    def test_is_empty(self):
        """Test is_empty method."""
        stack = Stack()
        self.assertTrue(stack.is_empty())

    def test_is_not_empty(self):
        """Test is_empty method."""
        stack = Stack(['a', 'b', 'c'])
        self.assertFalse(stack.is_empty())

    def test_push(self):
        """Test push method."""
        stack = Stack()
        stack.push('a')
        self.assertEqual(len(stack), 1)

    def test_push_multiple(self):
        """Test push method."""
        stack = Stack()
        stack.push('a')
        stack.push('b')
        stack.push('c')
        self.assertEqual(len(stack), 3)

        for a, b in zip(stack, ['c', 'b', 'a']):
            self.assertEqual(a, b)

    def test_peek(self):
        """Test peek method."""
        stack = Stack(['a', 'b', 'c'])
        self.assertEqual(stack.peek(), 'c')

    def test_pop(self):
        """Test pop method."""
        stack = Stack(['a', 'b', 'c'])

        self.assertEqual(stack.pop(), 'c')
        self.assertEqual(len(stack), 2)

        for a, b in zip(stack, ['b', 'a']):
            self.assertEqual(a, b)

    def test_pop_empty(self):
        """Test pop method."""
        stack = Stack()
        self.assertRaises(IndexError, stack.pop)

    def test_copy(self):
        """Test copy method."""
        stack = Stack(['a', 'b', 'c'])
        stack_copy = stack.copy()

        self.assertIsNot(stack_copy, stack)

    def test_iter(self):
        """Test iter method."""
        stack = Stack(['a', 'b', 'c'])
        self.assertEqual(list(stack), ['c', 'b', 'a'])

    def test_extend(self):
        """Test extends method."""
        stack_1 = Stack([1, 2, 3])
        stack_2 = Stack([4, 5, 6])

        stack_1.extend(stack_2)
        self.assertEqual(len(stack_1), 6)

        for a, b in zip(stack_1, [6, 5, 4, 3, 2, 1]):
            self.assertEqual(a, b)

    def test_add_operator(self):
        """Test add operator."""
        stack_1 = Stack([1, 2, 3])
        stack_2 = Stack([4, 5, 6])

        stack_3 = stack_1 + stack_2

        self.assertEqual(len(stack_3), 6)

        for a, b in zip(stack_3, [6, 5, 4, 3, 2, 1]):
            self.assertEqual(a, b)
```

# 5. Queues

## ¿Qué son las queues?

> En los métodos del queue hay errores en la descripción del método add y pop, lo que dice describe lo que hacen esos métodos en un Stack.

Me adelanto un poco y les comparto mi implementación de los métodos básicos que mencionó en la clase.

Código

```python
from typing import Any
from typing import Iterable
from typing import Optional
from node import Node


class Queue:
    """Represents a simple queue."""
_front: Optional[Node]
_rear: Optional[Node]

_size: int

def __init__(self, *items) -> None:
    """Initializes the Queue"""
    self._front = None
    self._rear = None
    self._size = 0

    if len(items) > 0 and isinstance(items[0], Iterable):
        items = items[0]

    for item in items:
        self.add(item)

def clear(self):
    """Clears the queue.

    Time complexity: O(1)
    """

    self._front = None
    self._rear = None
    self._size = 0

def peek(self):
    """Returns the first element of the queue.

    Raises IndexError if the queue is empty.

    Time complexity: O(1)

    Returns:
        Any: First element of the queue.
    """

    if self._front is None:
        raise IndexError('The queue is empty.')

    return self._front.value

def is_empty(self) -> bool:
    """Check if the queue is empty.

    Returns:
        bool: True if the queue is empty. False otherwise.
    """

    return self._front is None

def add(self, value: Any) -> None:
    """Adds a new value to the queue.

    Time Complexity: O(1)

    Args:
        value (Any): Value to be added.

    Returns:
        None
    """

    if self._front is None:
        self._front = Node(value)
        self._rear = self._front

    else:
        self._rear.next = Node(value)
        self._rear = self._rear.next

    self._size += 1

    return None

def pop(self):
    """Removes a value from the queue.

    Raises IndexError if the queue is empty.

    Time complexity: O(1)

    Returns:
        Any: Removed value.
    """

    if self._front is None:
        raise IndexError('The queue is empty')

    value = self._front.value
    self._front = self._front.next

    self._size -= 1

    return value

def copy(self):
    """Returns a copy of the current queue.

    Time complexity: O(1)

    Returns:
        Queue: Copy of the current queue.
    """

    new_queue = Queue()

    new_queue._front = self._front
    new_queue._rear = self._rear
    new_queue._size = self._size

    return new_queue

def depthcopy(self):
    """Returns a copy of the current queue.

    This function will create a copy of each node in the queue.

    Time Complexity: O(n)

    Returns:
        Queue: Copy of the queue.
    """

    new_queue = Queue()

    pointer = self._front

    while pointer is not None:
        new_queue.add(pointer.value)
        pointer = pointer.next

    return new_queue

def itervalues(self) -> Iterable[Any]:
    """Generates an iterable of the values in the queue.

    Time complexity: O(n)

    Returns:
        Iterable: Iterable of the values in the queue.
    """

    pointer = self._front
    while pointer is not None:
        yield pointer.value
        pointer = pointer.next

    return None

def iternodes(self) -> Iterable[Node]:
    """Generates an iterable of the nodes in the queue.

    Returns:
        Iterable: Iterable of the nodes in the queue.
    """

    pointer = self._front
    while pointer is not None:
        yield pointer
        pointer = pointer.next

    return None

def __len__(self) -> int:
    """Returns the length of the queue when using the `len` built-in function.

    Time complexity: O(1)

    Returns:
        int: Number of nodes into the queue.
    """

    return self._size

def __str__(self):
    """Returns a string representation of the queue.

    Time complexity: O(n)

    Args:
        str: Representation of the queue
    """

    return '->'.join(self.itervalues())

def __iter__(self) -> Iterable[Any]:
    """Generates an iterable of the values in the queue.

    Time complexity: O(n)

    Returns:
        Iterable: Iterable of the values in the queue.
    """

    return self.itervalues()

def __contains__(self, value: Any) -> bool:
    """Checks if a value is in the queue.

    Time complexity: O(n)

    Args:
        value: Value to be searched in the queue.

    Returns:
        bool: True if the value is in the queue. False otherwise.
    """

    for item in self:
        if item == value:
            return True

    return False

def __add__(self, other: Iterable) -> 'Queue':
    """Returns a new queue with the items of both queues.

    Time complexity: O(n)

    Args:
        other (Iterable): Items to be added to the current queue.

    Returns:
        Queue: New queue with the items of both queues.
    """

    new_queue = self.depthcopy()

    for item in other:
        new_queue.add(item)

    return new_queue
```

### 
```python
### Unittest

import unittest

### Utilities
from queue import Queue


class QueueTestCase(unittest.TestCase):
    """Queue test cases."""
def test_initialize_empty(self):
    """Test initialize empty queue."""
    _queue = Queue()
    self.assertEqual(len(_queue), 0)

def test_intialize_with_list(self):
    """Test initialize with list."""
    _queue = Queue([1, 2, 3])
    self.assertEqual(len(_queue), 3)

    for a, b in zip(_queue, [1, 2, 3]):
        self.assertEqual(a, b)

def test_initialize_with_tuple(self):
    """Test initialize with tuple."""
    _queue = Queue((1, 2, 3))
    self.assertEqual(len(_queue), 3)

    for a, b in zip(_queue, [1, 2, 3]):
        self.assertEqual(a, b)

def test_initialize_with_set(self):
    """Test initialize with set."""
    _queue = Queue({1, 2, 3})
    self.assertEqual(len(_queue), 3)

    for a, b in zip(_queue, [1, 2, 3]):
        self.assertEqual(a, b)

def test_initialize_with_iterable(self):
    """Test initialize with iterable."""
    _queue = Queue(range(1, 4))
    self.assertEqual(len(_queue), 3)

    for a, b in zip(_queue, [1, 2, 3]):
        self.assertEqual(a, b)

def test_is_empty(self):
    """Test is empty."""
    _queue = Queue()
    self.assertTrue(_queue.is_empty())

    _queue = Queue([1, 2, 3])
    self.assertFalse(_queue.is_empty())

def test_add(self):
    """Test add."""
    _queue = Queue()
    _queue.add(1)
    self.assertEqual(len(_queue), 1)

    _queue.add(2)
    self.assertEqual(len(_queue), 2)

def test_peek(self):
    """Test peek."""
    _queue = Queue()
    self.assertRaises(IndexError, _queue.peek)

    _queue = Queue([1, 2, 3])
    self.assertEqual(_queue.peek(), 1)

def test_pop(self):
    """Test pop."""
    _queue = Queue([1, 2, 3])
    self.assertEqual(_queue.pop(), 1)
    self.assertEqual(_queue.pop(), 2)
    self.assertEqual(_queue.pop(), 3)

    self.assertRaises(IndexError, _queue.pop)

def test_copy(self):
    """Test copy."""
    _queue = Queue([1, 2, 3])
    queue_copy = _queue.copy()
    self.assertIsNot(_queue, queue_copy)
    self.assertEqual(len(_queue), len(queue_copy))

    for a, b in zip(_queue.iternodes(), queue_copy.iternodes()):
        self.assertEqual(a, b)
        self.assertIs(a, b)

def test_depthcopy(self):
    """Test depthcopy."""
    _queue = Queue([1, 2, 3])
    queue_copy: Queue = _queue.depthcopy()
    self.assertIsNot(_queue, queue_copy)
    self.assertEqual(len(_queue), len(queue_copy))

    for a, b in zip(_queue.iternodes(), queue_copy.iternodes()):
        self.assertEqual(a, b)
        self.assertIsNot(a, b)

def test_clear(self):
    """Test clear."""
    _queue = Queue([1, 2, 3])
    _queue.clear()
    self.assertEqual(len(_queue), 0)

def test_contains(self):
    """Test contains."""
    _queue = Queue([1, 2, 3])
    self.assertTrue(1 in _queue)
    self.assertFalse(4 in _queue)

def test_add_operator(self):
    """Test add operator."""
    _queue = Queue([1, 2, 3])
    _queue += [4, 5, 6]
```

## Queue basada en listas

Para obtener el tamaño, simplemente aplicaría la **función len()** a la lista **self.items**.

```python
self.size = len(self.items)
```

## basics

```python
from typing import List
from typing import Any
from typing import Iterable
from typing import Optional
from node import Node


class Queue:
    """Represents a simple queue."""

    _items: List

    def __init__(self, *items) -> None:
        """Initializes the Queue"""
        self._items = []

        if len(items) > 0 and isinstance(items[0], Iterable):
            items = items[0]

        for item in items:
            self.enqueue(item)

    def clear(self):
        """Clears the queue.

        Time complexity: O(1)
        """

        self._items = []

    def peek(self):
        """Returns the first element of the queue.

        Raises IndexError if the queue is empty.

        Time complexity: O(1)

        Returns:
            Any: First element of the queue.
        """

        if len(self._items) == 0:
            raise IndexError('The queue is empty.')

        return self._items[0]

    def is_empty(self) -> bool:
        """Check if the queue is empty.

        Returns:
            bool: True if the queue is empty. False otherwise.
        """

        return len(self._items) == 0

    def enqueue(self, value: Any) -> None:
        """Adds a new value to the queue.

        Time Complexity: O(n)

        Args:
            value (Any): Value to be added.

        Returns:
            None
        """

        self._items.append(value)

        return None

    def dequeue(self):
        """Removes a value from the queue.

        Raises IndexError if the queue is empty.

        Time complexity: O(n)

        Returns:
            Any: Removed value.
        """

        if len(self._items) == 0:
            raise IndexError('The queue is empty.')

        value = self._items.pop(0)

        return value

    def copy(self):
        """Returns a copy of the current queue.

        Time complexity: O(1)

        Returns:
            Queue: Copy of the current queue.
        """

        new_queue = Queue()

        new_queue._items = self._items

        return new_queue

    def depthcopy(self):
        """Returns a copy of the current queue.

        This function will create a copy of value in the queue.

        Time Complexity: O(n)

        Returns:
            Queue: Copy of the queue.
        """

        new_queue = Queue()

        new_queue._items = [item for item in self]

        return new_queue

    def itervalues(self) -> Iterable[Any]:
        """Generates an iterable of the values in the queue.

        Time complexity: O(n)

        Returns:
            Iterable: Iterable of the values in the queue.
        """

        return iter(self._items)

    def __len__(self) -> int:
        """Returns the length of the queue when using the `len` built-in function.

        Time complexity: O(1)

        Returns:
            int: Number of nodes into the queue.
        """

        return len(self._items)

    def __str__(self):
        """Returns a string representation of the queue.

        Time complexity: O(n)

        Args:
            str: Representation of the queue
        """

        return '->'.join(self._items)

    def __iter__(self) -> Iterable[Any]:
        """Generates an iterable of the values in the queue.

        Time complexity: O(n)

        Returns:
            Iterable: Iterable of the values in the queue.
        """

        return self.itervalues()

    def __contains__(self, value: Any) -> bool:
        """Checks if a value is in the queue.

        Time complexity: O(n)

        Args:
            value: Value to be searched in the queue.

        Returns:
            bool: True if the value is in the queue. False otherwise.
        """

        for item in self:
            if item == value:
                return True

        return False

    def __add__(self, other: Iterable) -> 'Queue':
        """Returns a new queue with the items of both queues.

        Time complexity: O(n)

        Args:
            other (Iterable): Items to be added to the current queue.

        Returns:
            Queue: New queue with the items of both queues.
        """

        new_queue = self.depthcopy()

        new_queue._items += [item for item in other]

        return new_queue
```

## Unit tests

```python
# Unittest
import unittest

# Utilities
from queue_with_list import Queue


classQueueTestCase(unittest.TestCase):
    """Queue test cases."""

    deftest_initialize_empty(self):
        """Test initialize empty queue."""
        _queue = Queue()
        self.assertEqual(len(_queue), 0)

    deftest_intialize_with_list(self):
        """Test initialize with list."""
        _queue = Queue([1, 2, 3])
        self.assertEqual(len(_queue), 3)

        for a, b inzip(_queue, [1, 2, 3]):
            self.assertEqual(a, b)

    deftest_initialize_with_tuple(self):
        """Test initialize with tuple."""
        _queue = Queue((1, 2, 3))
        self.assertEqual(len(_queue), 3)

        for a, b inzip(_queue, [1, 2, 3]):
            self.assertEqual(a, b)

    deftest_initialize_with_set(self):
        """Test initialize with set."""
        _queue = Queue({1, 2, 3})
        self.assertEqual(len(_queue), 3)

        for a, b inzip(_queue, [1, 2, 3]):
            self.assertEqual(a, b)

    deftest_initialize_with_iterable(self):
        """Test initialize with iterable."""
        _queue = Queue(range(1, 4))
        self.assertEqual(len(_queue), 3)

        for a, b inzip(_queue, [1, 2, 3]):
            self.assertEqual(a, b)

    deftest_is_empty(self):
        """Test is empty."""
        _queue = Queue()
        self.assertTrue(_queue.is_empty())

        _queue = Queue([1, 2, 3])
        self.assertFalse(_queue.is_empty())

    deftest_enqueue(self):
        """Test add."""
        _queue = Queue()
        _queue.enqueue(1)
        self.assertEqual(len(_queue), 1)

        _queue.enqueue(2)
        self.assertEqual(len(_queue), 2)

    deftest_peek(self):
        """Test peek."""
        _queue = Queue()
        self.assertRaises(IndexError, _queue.peek)

        _queue = Queue([1, 2, 3])
        self.assertEqual(_queue.peek(), 1)

    deftest_dequeue(self):
        """Test dequeue."""
        _queue = Queue([1, 2, 3])
        self.assertEqual(_queue.dequeue(), 1)
        self.assertEqual(_queue.dequeue(), 2)
        self.assertEqual(_queue.dequeue(), 3)

        self.assertRaises(IndexError, _queue.dequeue)

    deftest_copy(self):
        """Test copy."""
        _queue = Queue([1, 2, 3])
        queue_copy = _queue.copy()
        self.assertIsNot(_queue, queue_copy)
        self.assertEqual(len(_queue), len(queue_copy))

        for a, b inzip(_queue, queue_copy):
            self.assertEqual(a, b)

    deftest_depthcopy(self):
        """Test depthcopy."""
        _queue = Queue([1, 2, 3])
        queue_copy: Queue = _queue.depthcopy()
        self.assertIsNot(_queue, queue_copy)
        self.assertEqual(len(_queue), len(queue_copy))

        for a, b inzip(_queue, queue_copy):
            self.assertEqual(a, b)

    deftest_clear(self):
        """Test clear."""
        _queue = Queue([1, 2, 3])
        _queue.clear()
        self.assertEqual(len(_queue), 0)

    deftest_contains(self):
        """Test contains."""
        _queue = Queue([1, 2, 3])
        self.assertTrue(1in _queue)
        self.assertFalse(4in _queue)

    deftest_add_operator(self):
        """Test add operator."""
        _queue = Queue([1, 2, 3])
        _queue += [4, 5, 6]
        self.assertEqual(len(_queue), 6)

        for a, b inzip(_queue, [1, 2, 3, 4, 5, 6]):
            self.assertEqual(a, b)
```

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/estructuras-datos-lineales-python at queues-basadas-en-listas](https://github.com/platzi/estructuras-datos-lineales-python/tree/queues-basadas-en-listas)

## Queue basada en dos stacks

En el método dequeue(), antes de hacer el pop de self.outbound_stack habría que comprobar si existe algo dentro de ese campo, para que no lance una excepción.

```python
def dequeue(self):
        while self.inbound_stack:
            self.outbound_stack.append(self.inbound_stack.pop())
        return self.outbound_stack.pop(0) if self.outbound_stack else None
```

## Queue basada en nodos

La construcción de queue basadas en nodos tiene una medida de O(1) ya que trabaja unicamente con if, elif y else, mientras que la construcción de un queue basado en un stack, implica un ciclo while, por lo tanto tiene una medida de O(n).

código de la clase

```python
class TwoWayNode():
    def __init__(self, data=None, next=None, previous=None):
        self.data = data
        self.next = next
        self.previous = previous

class Queue:
    def __init__(self) -> None:
        self.head = None
        self.tail = None
        self.count = 0

    def enqueue(self, data):
        new_node = TwoWayNode(data, None, None)
        if self.head is None:
            self.head = new_node
            self.tail = self.head
        else:
            new_node.previous = self.tail
            self.tail.next = new_node
            self.tail = new_node
        
        self.count += 1
    
    def dequeue(self):
        current = self.head

        if self.count == 1:
            self.count -= 1
            self.head = None
            self.tail = None

        elif self.count > 1:
            self.head = self.head.next
            self.head.previous = None
            self.count -= 1

        return current.data
from node_based_queue import Queue

food = Queue()
food.enqueue('eggs')
food.enqueue('ham')
food.enqueue('spam')

print(food.head.data)
print(food.head.next.data)
print(food.tail.data)
print(food.tail.previous.data)
print(food.count)

print(food.dequeue())
print(food.head.data)
eggs
ham
spam
ham
3
eggs
ham
```

[Big O Notation explicada con animaciones](https://www.youtube.com/watch?v=v1SYihb4rcw)

## Reto: simulador de playlist musical

solución al reto.
Utilice la estructura Queue basada en listas.
Creeu na clase MusicPlaylist con 4 methods.

- Un method para agregar canciones mediante una tupla con su nombre, y duración(add_songs)
- Un método para reproducir solo una canción(play_song)
- Un método para reproducir todas las canciones dentro de la playlist
- Y un método para visualizar todas las canciones dentro de la playlist(all_songs)

```python
import time

class ListQueue:
    def __init__(self):
        self.items = []
        self.size = 0

    def enqueue(self, data):
        self.items.insert(0, data)
        self.size += 1

    def dequeue(self):
        if self.size <= 0:
            return 0
        data = self.items.pop()
        self.size -= 1
        return data
        

class MusicPlaylist(ListQueue):
    def __init__(self):
        super().__init__()

    def add_songs(self, song: tuple):
        self.enqueue(song)

    def play_song(self):
        song = self.dequeue()
        if song:
            print(f'Playing: {song[0]} for {song[1]} min')
            time.sleep(song[1])
        else:
            print('There are no more songs')

    def play_songs(self):
        while self.items:
            self.play_song()

    def all_songs(self):
        if self.items:
            print('The songs in the playlist are:')
            for i, song in enumerate(self.items[::-1],start=1):
                print(f'Song {i}: {song[0]}, Time: {song[1]}')
        print(f'There are {self.size} songs')
        return self.size
        



if __name__ == '__main__':
    playlist = MusicPlaylist()

    playlist.add_songs(("REMEMBER", 5.19))
    playlist.add_songs(("comfy vibes", 3.12))
    playlist.add_songs(("Into the blue's", 4.03))
    playlist.add_songs(("E.M.A", 4.16))

    playlist.all_songs()
    print()

    playlist.play_songs()
    print()

    playlist.play_song()
    playlist.all_songs()
```

Outputs

```sh
The songs in the playlist are:
Song 1: REMEMBER, Time: 5.19
Song 2: comfy vibes, Time: 3.12
Song 3: Into the blue's, Time: 4.03
Song 4: E.M.A, Time: 4.16
There are 4 songs

Playing: REMEMBER for 5.19 min
Playing: comfy vibes for 3.12 min
Playing: Into the blue's for 4.03 min
Playing: E.M.A for 4.16 min

There are no more songs
There are 0 songs
```

**music_player**

```python
from random import randint
from node_based_queue import Queue
from time import sleep

class Track:
    def __init__(self, title=None):
        self.title = title
        self.length = randint(5, 6)

class MediaPlayerQueue(Queue):
    def __init__(self):
        super(MediaPlayerQueue, self).__init__()

    def add_track(self, track):
        self.enqueue(track)

    def play(self):
        print(f"count: {self.count}")
        while self.count > 0 and self.head is not None:
            current_track_node = self.dequeue()
            print(f"Now playing {current_track_node.data.title}.")

            sleep(current_track_node.data.length)

track1 = Track("white whistle")
track2 = Track("butter butter")
track3 = Track("Oh black star")
track4 = Track("Watch that chicken")
track5 = Track("Don't go")

""" print(track1.length)
print(track2.length)
print(track3.length) """

media_player = MediaPlayerQueue()

media_player.add_track(track1)
media_player.add_track(track2)
media_player.add_track(track3)
media_player.add_track(track4)
media_player.add_track(track5)
media_player.play()
```



# 6. Próximos pasos

## Más allá de las estructuras lineales

- Crear tu propias estructuras lineales.
- Optimizar tu codigo.
- Optimizar el uso de memoria.