class Node(object):
  def __init__(self, data, next=None):
    self.data = data
    self.next = next

class TwoWayNode(Node):
  def __init__(self, data, next=None, prev=None):
    super().__init__(data, next)
    self.prev = prev