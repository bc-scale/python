<h1>Flask</h1>

<h3>Bernardo Cassina</h3>

<h1>Talbla de Contenido</h1>

- [1. Fundamentos de Flask](#1-fundamentos-de-flask)
  - [Qué aprenderás sobre Flask](#qué-aprenderás-sobre-flask)
  - [¿Cómo funcionan las aplicaciones en internet?](#cómo-funcionan-las-aplicaciones-en-internet)
  - [¿Qué es Flask?](#qué-es-flask)
  - [Instalación de Python, pip y virtualenv](#instalación-de-python-pip-y-virtualenv)
  - [Hello World Flask](#hello-world-flask)
  - [Debugging en Flask](#debugging-en-flask)
  - [Request y Response](#request-y-response)
  - [Ciclos de Request y Response](#ciclos-de-request-y-response)
- [2. Uso de templates y archivos estáticos](#2-uso-de-templates-y-archivos-estáticos)
  - [Templates con Jinja 2](#templates-con-jinja-2)
  - [Estructuras de control](#estructuras-de-control)
  - [Herencia de templates](#herencia-de-templates)
  - [Include y links](#include-y-links)
  - [Uso de archivos estáticos: imágenes](#uso-de-archivos-estáticos-imágenes)
  - [Configurar páginas de error](#configurar-páginas-de-error)
- [3. Extensiones de Flask](#3-extensiones-de-flask)
  - [Flask Bootstrap](#flask-bootstrap)
  - [Configuración de Flask](#configuración-de-flask)
  - [Implementación de Flask-Bootstrap y Flask-WTF](#implementación-de-flask-bootstrap-y-flask-wtf)
  - [Uso de método POST en Flask-WTF](#uso-de-método-post-en-flask-wtf)
  - [Desplegar Flashes (mensajes emergentes)](#desplegar-flashes-mensajes-emergentes)
  - [Pruebas básicas con Flask-testing](#pruebas-básicas-con-flask-testing)
- [4. Proyecto](#4-proyecto)
  - [Planteamiento del proyecto: To Do List](#planteamiento-del-proyecto-to-do-list)
  - [App Factory](#app-factory)
  - [Uso de Blueprints](#uso-de-blueprints)
  - [Blueprints II](#blueprints-ii)
  - [Base de datos y App Engine con Flask](#base-de-datos-y-app-engine-con-flask)
  - [Configuración de Google Cloud SDK](#configuración-de-google-cloud-sdk)
  - [Configuración de proyecto en Google Cloud Platform](#configuración-de-proyecto-en-google-cloud-platform)
  - [Implementación de Firestore](#implementación-de-firestore)
  - [Autenticación de usuarios: Login](#autenticación-de-usuarios-login)
  - [Autenticación de usuarios: Logout](#autenticación-de-usuarios-logout)
  - [Signup](#signup)
  - [Agregar tareas](#agregar-tareas)
  - [Eliminar tareas](#eliminar-tareas)
  - [Editar tareas](#editar-tareas)
  - [Deploy a producción con App Engine](#deploy-a-producción-con-app-engine)
  - [Conclusiones](#conclusiones)

# 1. Fundamentos de Flask

## Qué aprenderás sobre Flask

Es un framework web de Python, integraciones con Bootstrap, GCloud, What The Forms y más.

Flask es sencillo de aprender, tiene una documentación clara y práctica, es rápido a la hora de renderizar puede ser hasta tres veces más rapido que Django. También es fácil de realizar una API REST, la estructura de un proyecto es flexible y es ideal para aprender desarrollo web con un framework de Python.

Conoce todo el potencial de Flask como framework web de Python, integraciones con Bootstrap, GCloud, What The Forms y más.

Flask es sencillo de aprender, tiene una documentación clara y práctica, es rápido a la hora de renderizar **puede ser hasta tres veces más rapido que Django**. También es fácil de realizar una API REST, la estructura de un proyecto es flexible y es ideal para aprender desarrollo web con un framework de Python.

>  Bases de datos con flask debes instalar SQLAlchemy u otro orm para facilitar la conexión y trabajo con la base de datos. Encontré este tutorial que te puede servir para lo que necesitas hacer https://medium.com/@mwesigwafrank/python-flask-mysql-crud-tutorial-part-one-abf66a348bda

> [Flask](https://www.notion.so/Flask-d14c060c8b2342199e3a5f27cf2d13ef)

[Documentación de Flask](https://flask-doc.readthedocs.io/en/latest/).

## ¿Cómo funcionan las aplicaciones en internet?

Cuando utilizas una aplicación web puedes interactuar con ella desde una computadora hasta un dispositivo móvil, pero esto no quiere decir que consume el procesamiento de tu dispositivo. Todo lo contrario, se hace en una red de servidores.

Estos servidores unen su poder de procesamiento con el fin transmitir solicitudes a todo el mundo, a su vez utilizar servidores especializados para almacenar los datos con los cuales se está trabajando, así como los datos de los demás usuarios. Como todo esto sucede sin demora alguna, parecerá que la aplicación se está ejecutando de forma nativa en tu dispositivo.

El servidor procesa la información obtenida por el navegador, luego se realizan los procedimientos necesarios de acuerdo a la lógica de negocio de la aplicación para regresar la información solicitada al cliente.

**Ejemplo:**

Cuando utilizamos **Google Drive** el cual es una aplicación web y abrimos un documento con Google Docs, el navegador se comunica con los servidores para ver y editar el documento.

A medida que vayas editando el documento, tu navegador trabajará de la mano con los servidores para asegurarse que todos los cambios se estén guardando.

**Ventajas:**

- Muchas de las aplicaciones web que existen son gratuitas.
- Puedes acceder a tu información en cualquier momento y lugar.
- No dependes de un dispositivo en específico ya que la aplicación se encuentra almacenada en la web.

**[What is Web App!](https://blog.stackpath.com/web-application/)**

Que interesante este mundo de las webapps, me gustaría agregar que algunos ejempls de Web Apps serian las de google:

- tal como slides, google sheets, google docs.
- Microsoft 365.
- Netflix.
- Notion y evernote.

**Aplicación web**

- Browser→ Servidor Web → Aplicación →BD →Aplicación →Servidor Web →Browser.

## ¿Qué es Flask?

En esta clase el profesor Bernardo Cassina nos explica cómo podemos usar Flask para desarrollar aplicaciones web escritas en Python con este framework.

Flask es un framework minimalista escrito en Python que permite crear aplicaciones web rápidamente y con un mínimo de líneas de código, busca que su infraestructura inicial sea lo más simple posible y pueda personalizarse fácilmente, puedes extender sus funcionalidades con las llamadas Flask Extensions http://flask.pocoo.org/extensions/

que es flask = es un microfamework, framework minimalista escrito en python es flexible es lo mas simple.

pip = librerias para manejar paquetes en pithon

[Flask, el Framework flexible](https://platzi.com/blog/flask-el-framework-flexible/)

## Instalación de Python, pip y virtualenv

### Esta es la guía para configurar nuestro ambiente con Python 3.

Por lo general Mac ya incluye una instalación de Python, la puedes verificar ejecutando los siguientes comandos en una terminal:

```shell
python --version
```

```bash
python3 --version
```

Debemos asegurarnos de tener **python 3**. Para instalar Python puedes seguir el siguiente enlace y después regresar a esta lectura.

https://platzi.com/clases/1378-python/14289-guia-de-instalacion-y-conceptos-basicos/

### Instalación en Windows

Una vez que instalaste **python 3** desde [python.org](https://www.python.org/) vamos a verificar que también incluimos **pip** en esta instalación. Después debes correr el siguiente comando para instalar virtualenv:

```bash
pip install virtualenv
```

El sistema debe haber instalado virtualenv y ahora podemos comenzar con el curso.

### Instalación en Mac

Si ya instalaste python 3 ahora corre el siguiente comando para instalar **pip**:

```bash
sudo easy_install pip
```

Para install virtualenv de manera global corre:

```bash
pip install virtualenv
```

El sistema debe haber instalado virtualenv y ahora podemos comenzar con el curso.

`virtualenv` desde hace muchas versiones ya está incluido en python como un modulo integrado en el comando python, solo hay que hacer

```sh
python -m venv nuestro_virtual_env
```

https://medium.com/analytics-vidhya/deploying-a-machine-learning-model-using-flask-for-beginners-674944714b86

https://towardsdatascience.com/how-to-build-deploy-an-unsupervised-learning-flask-app-from-scratch-afd3a18661a7

https://heartbeat.fritz.ai/deploying-a-text-classification-model-using-flask-and-vue-js-25b9aa7ff048

## Hello World Flask

Estos son los conceptos principales que debes entender antes de hacer un Hello World en Flask:

- **virtualenv:** es una herramienta para crear entornos aislados de Python.
- **pip:** es el instalador de paquetes para Python.
- **requirements.txt:** es el archivo en donde se colocará todas las dependencias a instalar en nuestra aplicación.
- **FLASK_APP:** es la variable para identificar el archivo donde se encuentra la aplicación.

Para que no tengas problemas en videos posteriores **instala la version de flask del profe**. Aquí te dejo el requirements.txt

```txt
flask==1.0.2
click==7.0
jinja2==2.10
itsdangerous==1.1.0
markupsafe==1.1.1
werkzeug==0.14.1
flask-bootstrap==3.3.7.1
flask-wtf==0.14.2
```

Si ya **creaste tu venv inicia uno nuevo** en un nuevo directorio y ejecuta el requirements que te deje arriba.

Pueden crear el archivo “requirements.txt” y al mismo tiempo meter allí la lista de dependencias introduciendo el siguiente comando en terminal:

```bash
pip freeze > requirements.txt
```

Y si les tocara alguna vez instalar en su virtual enviroment los requirements para colaborar en un proyecto que necesita dependencias de pip, usan el siguiente comando:

```bash
pip install -r requirements.txt
```

Algunas veces es necesario instalar versiones anteriores. Para eso se debe especificar la versión de la dependencia a instalar:::

- Instalación con requirement.txt:

1. Creamos un archivo de texto:`$touch 

   ```bash
   requirements.txt
   ```

2. Cargamos nuestras dependencias a instalar:

   ````bash
   flask==1.0.2 
   django==2.1.7 
   sqlalchemy -U
   #Para ultima versión es con -U
   ````

3. Ejecutamos pip install:

   ```bash
   $ pip install –r requirements.txt
   ```

   

   Si quieres que el servidor este visible para cualquier equipo en tu red local, dedes ejecuar el comando flask run con la bandera --host=0.0.0.0 quedaria asi:

   ```sh
   flask run --host=0.0.0.0
   ```

   despues de eso puedes hacer request desde otro equipo en la misma red

**Crear el Entorno Virtual en windows:**

Para crear un nuevo virtualenv, necesitas abrir una terminal command prompt y ejecutar

```bash
python3 -m venv myvenv
```

Se verá así:

```bash
C:\Users\Name\platzi-flask> python3 -m venv myvenv
```

Donde **myvenv** es el nombre de tu **virtualenv**.

Puedes utilizar cualquier otro nombre, pero asegúrate de usar minúsculas y no usar espacios, acentos o caracteres especiales. También es una buena idea mantener el nombre corto. ¡Vas utilizarlo muchas vecesl!

**Inicia el entorno virtual ejecutando:**

```bash
C:\Users\Name\platzi-flask> myvenv\Scripts\activate
```

Exportar nuestro archivo

```bash
export FLASK_APP=main.py

# Correr nuestro servidor
flask run 
```

Otra opción para correr el servidor en modo debug es poner estas lineas al final del archivo `main.py`:

```python
if __name__ == '__main__':
    app.run(debug=True)
```

y correr `main.py` desde la terminal:

```bash
python main.py 
```

Fuente de texto: https://tutorial.djangogirls.org/es/django_installation/

![img](https://www.google.com/s2/favicons?domain=http://flask.pocoo.org//static/favicon.ico)[Welcome | Flask (A Python Microframework)](http://flask.pocoo.org/)

## Debugging en Flask

**Debugging:** es el proceso de identificar y corregir errores de programación.

Para activar el *debug mode* escribir lo siguiente en la consola:

```bash
export FLASK_DEBUG=1
echo $FLASK_DEBUG
```

Logging: es una grabación secuencial en un archivo o en una base de datos de todos los eventos que afectan a un proceso particular.

Se utiliza en muchos casos distintos, para guardar información sobre la actividad de sistemas variados.

Tal vez su uso más inmediato a nuestras actividades como desarrolladores web sería el logging de accesos al servidor web, que analizado da información del tráfico de nuestro sitio. Cualquier servidor web dispone de logs con los accesos, pero además, suelen disponer de otros logs, por ejemplo, de errores.

Los sistemas operativos también suelen trabajar con logs, por ejemplo para guardar incidencias, errores, accesos de usuarios, etc.

A través de el logs se puede encontrar información para detectar posibles problemas en caso de que no funcione algún sistema como debiera o se haya producido una incidencia de seguridad.

variables debug:

```python
if __name__ == '__main__':
    app.run(port = 5000, debug = True)
```

## Request y Response

```python
from flask import Flask, request

app = Flask(__name__)


@app.route('/')
def hello():
  user_ip = request.remote_addr

  return 'Hello World platzi, tu ip es {}'.format(user_ip)
```

## Ciclos de Request y Response

**Request-Response:** es uno de los métodos básicos que usan las computadoras para comunicarse entre sí, en el que la primera computadora envía una solicitud de algunos datos y la segunda responde a la solicitud.

Por lo general, hay una serie de intercambios de este tipo hasta que se envía el mensaje completo.

**Por ejemplo:** navegar por una página web es un ejemplo de comunicación de *request-response*.

*Request-response* se puede ver como una llamada telefónica, en la que se llama a alguien y responde a la llamada; es decir hacemos una petición y recibimos una respuesta.

# 2. Uso de templates y archivos estáticos

## Templates con Jinja 2

**Templates con Jinja 2**

Un templeate -> archivo de HTML -> renderiza informacion: Estatica o DInamica -> por variables -> luego nos muestra en el navegador

Para renderizar un template/plantilla creada con Jinja2 simplemente hay que hacer uso del método **render_template**.

A este método debemos pasarle el nombre de nuestra template y las variables necesarias para su renderizado como parámetros clave-valor.

Flask buscará las plantillas en el directorio **templates** de nuestro proyecto. En el sistema de ficheros, este directorio se debe encontrar en el mismo nivel en el que hayamos definido nuestra aplicación. En nuestro caso, la aplicación se encuentra en el fichero **[hello.py](http://hello.py/)**.

Es hora de crear este directorio y añadir las páginas hello.html, La estructura de nuestro proyecto quedaría del siguiente modo:

```sh
Flask-proyect
|_hello.py
|_ /templeate
    |__ hello.html
```

Ejemplo archivo [hello.py](http://hello.py/)

```python
from flask import Flask, request, make_response, redirect, render_template

app = Flask(__name__)

@app.route('/')
def index():
    user_ip = request.remote_addr
    response = make_response(redirect('/hello_world'))
    response.set_cookie('user_ip', user_ip)

    return response

@app.route('/hello_world')
def hello_world():
    #creamos nueva variable de la ip que detectamos en el browser
    user_ip = request.cookies.get('user_ip')

    return render_template('hello_world.html', user_ip= user_ip)
# metodo es render_template con jinja2 y la variable es user_ip.
```

[Welcome | Jinja2 (The Python Template Engine)](http://jinja.pocoo.org/)

## Estructuras de control

**Iteración:** es la repetición de un segmento de código dentro de un programa de computadora. Puede usarse tanto como un término genérico (como sinónimo de repetición), así como para describir una forma específica de repetición con un estado mutable.

Un ejemplo de iteración sería el siguiente:

```jinja2
{% for key, segment in segment_details.items() %}
        <tr>
                <td>{{ key }}td>
                <td>{{ segment }}td>
        tr>
{% endfor %}  
```

En este ejemplo estamos iterando por cada *segment_details.items()* para mostrar los campos en una tabla `{{ key }}` `{{ segment }}` de esta forma dependiendo de cuantos *segment_details.items()* haya se repetirá el código.

## Herencia de templates

**Macro:** son un conjunto de comandos que se invocan con una palabra clave, opcionalmente seguidas de parámetros que se utilizan como código literal. Los Macros son manejados por el compilador y no por el ejecutable compilado.

Los macros facilitan la actualización y mantenimiento de las aplicaciones debido a que su re-utilización minimiza la cantidad de código escrito necesario para escribir un programa.

En este ejemplo nuestra *macro* se vería de la siguiente manera:

```jinja
{% macro nav_link(endpoint, text) %}
    {% if request.endpoint.endswith(endpoint) %}
        <li class="active"><a href="{{ url_for(endpoint) }}">{{text}}</a></li>
    {% else %}
        <li><a href="{{ url_for(endpoint) }}">{{text}}</a></li>
    {% endif %}
{% endmacro %}
```

Un ejemplo de uso de macros en Flask:

```jinja2
{% from "macros.html" import nav_link with context %}
<!DOCTYPE html>
<html lang="en">
    <head>
    {% block head %}
        <title>My application</title>
    {% endblock %}
    </head>
    <body>
        <ul class="nav-list">
            {{ nav_link('home', 'Home') }}
            {{ nav_link('about', 'About') }}
            {{ nav_link('contact', 'Get in touch') }}
        </ul>
    {% block body %}
    {% endblock %}
    </body>
</html>
```

Como podemos observar en la primera línea estamos llamando a *macros.html* que contiene todos nuestros *macros*, pero queremos uno en específico así que escribimos `import nav_link` para traer el *macro* deseado y lo renderizamos de esta manera en nuestro menú `{{ nav_link('home', 'Home') }}`.

## Include y links

**Cuando usas el keywords `extends`** para llamar a una plantilla, **esta plantilla que has llamado toma el control de tu vista**. Tomemos el ejemplo con el que venimos trabajando.

Contenido del archivo **`hello.html`** (reducido para el ejemplo):

```jinja
01. {% extends 'base.html' %}
02. 
03. {% block title %} 
04.    {{ super() }}
05.    Bienvenida 
06. {% endblock %}
07. 
08. {% block content %}
09.    {% if user_ip %}
10.        <h1>Hello World Platzi, tu IP es {{ user_ip }}</h1>
11.   {% else %}
12.        <a href="{{ url_for('index') }}">Ir a inicio</a>
13.    {% endif %}
14. {% endblock %}
```

**Nota que llamar a `base.html` hace que ahora todo el contenido de `hello.html` gira en torno al primero**. Puedes notar esto, específicamente en la línea #4, dónde necesita utilizar el **método `super()` para traer contenido de la plantilla PADRE**.

Adicionalmente puedes ver en la línea #8 que se crea un bloque `content` que contiene (dependiendo del condicional) un Título o un Enlace. **Este contenido SOLO se mostrará si nombra a este bloque `content` en `base.html`**. Por lo que si revisas el **código de `base.html` podrás ver la llamada del PADRE al HIJO**:

El código que copio de `base.html` ha sido adaptado para el ejemplo:

```jinja2
<!DOCTYPE html>
<html lang="en">
<head>
    <title> {% block title %} Flask Platzi | {% endblock %} </title>
</head>
<body>
    <!-- Aquí puedes ver la llamada del padre al hijo -->
    {% block content %}
    {% endblock %}    
</body>
</html>
```

Sin embargo, **cuando usas el keyword `import` lo único que haces es enlazar una plantilla con otra y será `hello.html` quien tendrá el control de hacer la llamada al contenido de la otra plantilla cuando lo requiera**. Puedes ver lo aquí:

El código que copio de `hello.html` ha sido adaptado para el ejemplo:

```jinja2
01. {% import 'macros.html' as macros %}
02. 
03. {% block content %}
04.     <ul>
05.         {% for todo in todos %}
06.             {{ macros.render_todo(todo) }}
07.         {% endfor %}
08.     </ul>
09. {% endblock %}
```

Como puedes ver en la línea #6 es `hello.html` quien decide llamar y usar el macro, **inclusive podrías importar `macros.html` y no usarlo. Cosa que no podría pasar con `extends`**.

[Tutorial flask spanish](https://j2logo.com/tutorial-flask-espanol/)

## Uso de archivos estáticos: imágenes

Ejemplo de estructura:

```jinja2
    {% if CONDITION %}
        <p>Bloque de contenido</p>
    {% else %}
        <p>Bloque de contenido</p>
    {% endif %}
```

Ejemplo de uso:

```jinja2
    {% if user_ip %}
        <h1>Hello World Platzi, tu IP es {{ user_ip }}</h1>
    {% else %}
        <p>No tienes una IP</p>
    {% endif %}
```

<h2>Uso de bucles</h2>

Una vez se envie una variable al elemento(plantilla html), es posible hacer recorridos, siguiendo la sintaxis:

```jinja2
    {% for ITEMS in COLECTION_ELEMENTS %}
        <p>Item</p>
    {% endfor %}
```

Ejemplo:

```jinja2
    {% for description in array_descriptions %}
        <p>{{description}}</p>
    {% endfor %}
```

#### Uso de url_for

La funcion **url_for** se puede utilizar para consumir archivos estaticos y redirecionar a otras páginas dentro del proyecto.

<h1>Uso archivos estativos</h1>

Se llaman a traves de la funcion **url_for**, tiene 2 parámetros:

1.- **path** = 'static’
2.- **filename** = [Ruta del archivo]

Ejemplo:

```jinja2
    <link rel="stylesheet" href="{{ url_for(path = 'static', filename = 'css/main.css')}}">
    <img src="{{url_for(path = 'static', filename = 'images/platzi.png')}}" alt="Platzi logo">
```

<h1>Uso de redireccionamiento a paginas</h1>

Se realiza el redirecciónamiento a traves de la funcion **url_for**, en el primer
atributo, se explifica el **path** del archivo(ruta), ejemplo:

```jinja2
    <li><a href="{{url_for(path = 'index')}}">Inicio</a></li>
```

### Uso Macro(funciones en plantillas)

Para crear componentes(funciones y código) se define un tipo(macro), el nombre de la funcion y los pámetros.

<h2>Ejemplo de estructura:</h2>

```jinja2
    {% macro NAME_FUNCTION(PARAM_1, PARAM_2) %}
        <p>Bloque de contenido<p>
    {% endmacro %}
```

### Ejemplo de uso:

Se define un archivo **render_item_list.html**

```jinja2
    {% macro render_item(todo) %}
        <li>Descripción: {{todo}} </li>
    {% endmacro %}
```

Se define un archivo **home.html**

```jinja2
    {% import 'render_item_list.html' as macros %}

    {% for description in arrDescriptions %}
        {{ macros.render_item(description) }}
    {% endfor %}
```

### Uso de plantillas(Block)

Ejemplo de archivo padre:

```jinja2
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            {% block title %} Flask Platzi | {% endblock %}
        </title>
    </head>
    <body>
        {% block content %}
        {% endblock %}
    </body>
    </html>
```

Ejemplo de archivo hijo(file.html)

```jinja2
    {% extends './base.html' %}
    {% block title %}
        {{super()}}
        Bienvenido
    {% endblock %}

    {% block content %}
        <p>Bloque de contenido del archivo hijo </p>
    {% endblock %}
```

> Chrome ‘cacheaba’ las imágenes por lo que tenia que ponerles un hash para que cada que cambiaba una imagen se mostrara correctamente y no decirles a mis usuarios que pulsaran ctrl + F5

```jinja2
  <img src="{{ url_for('static',filename='images/platzi.png')}}" alt="platzi logo">
```

## Configurar páginas de error

### Códigos de error:

**100:** no son errores sino mensajes informativos. Como usuario nunca los verás, sino que entre bambalinas indica que la petición se ha recibido y se continúa el proceso.

**200:** estos códigos también indican que todo ha ido correctamente. La petición se ha recibido, se ha procesado y se ha devuelto satisfactoriamente. Por tanto, nunca los verás en tu navegador, pues significan que todo ha ido bien.

**300:** están relacionados con redirecciones. Los servidores usan estos códigos para indicar al navegador que la página o recurso que han pedido se ha movido de sitio. Como usuario, no verás estos códigos, aunque gracias a ellos una página te podría redirigir automáticamente a otra.

**400:** corresponden a errores del cliente y con frecuencia sí los verás. Es el caso del conocido error 404, que aparece cuando la página que has intentado buscar no existe. Es, por tanto, un error del cliente (la dirección web estaba mal).

**500:** mientras que los códigos de estado 400 implican errores por parte del cliente (es decir, de parte tuya, tu navegador o tu conexión), los errores 500 son errores desde la parte del servidor. Es posible que el servidor tenga algún problema temporal y no hay mucho que puedas hacer salvo probar de nuevo más tarde.

El código en Python lo haría algo así:

```python
@app.errorhandler(500)
def internal_server_error(error):
  return render_template('500.html')
```

Y el HTML me quedaría algo así:

```jinja2
{% extends 'base.html' %}

{% block title %}
{{ super() }}
	500 Internal Server Error
{% endblock %}

{% block content %}
<h1>Lo sentimos</h1>
<p>Ocurrió un error, estamos trabajando en ello</p>
{% endblock %}
```

`main.py`

```python
@app.errorhandler(500)
def server_error(error):
    return render_template('500.html', error=error)
```

`500.html`

```jinja2
{% extends 'base.html' %}
{% block title %}
{{ super() }}
    500
{% endblock %}

{% block content %}
    <h1>Error del servidor, intenta más tarde</h1>
    <p>{{ error }}</p>
{% endblock %}
```



# 3. Extensiones de Flask

## Flask Bootstrap

![Bloques-Bootstrap.png](https://static.platzi.com/media/user_upload/Bloques-Bootstrap-c4bb85f6-a4a4-4132-9b48-3471c101ddc3.jpg)

**Framework:** es un conjunto estandarizado de conceptos, prácticas y criterios para enfocar un tipo de problemática particular que sirve como referencia, para enfrentar y resolver nuevos problemas de índole similar.

`navbar`

```jinja2
<div class="navbar navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button"
                    class="navbar-toggle"
                    data-toggle="collapse"
                    data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url_for('index') }}">
                <img src="{{ url_for('static', filename='images/platzi.png') }}"
                     style="max-width: 48px"
                     alt="Platzi logo">
            </a>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ url_for('index') }}">Inicio</a></li>
                <li><a href="https://platzi.com" target="_blank">Platzi</a></li>
            </ul>
        </div>
    </div>
</div>
```

Soporta Bootstrap4 lo que hay que hacer, especificar a la hora de hacer pip install

```sh
pip install Flask-Bootstrap4
```

```sh
pip install Flask-BS4
```

bloques de botstrap/base.html para que sepan que bloques pueden manipular:

```jinja2
{% block doc -%}
<!DOCTYPE html>
<html{% block html_attribs %}{% endblock html_attribs %}>
{%- block html %}
  <head>
    {%- block head %}
    <title>{% block title %}{{title|default}}{% endblock title %}</title>

    {%- block metas %}
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {%- endblock metas %}

    {%- block styles %}
    <!-- Bootstrap -->
    <link href="{{bootstrap_find_resource('css/bootstrap.css', cdn='bootstrap')}}" rel="stylesheet">
    {%- endblock styles %}
    {%- endblock head %}
  </head>
  <body{% block body_attribs %}{% endblock body_attribs %}>
    {% block body -%}
    {% block navbar %}
    {%- endblock navbar %}
    {% block content -%}
    {%- endblock content %}

    {% block scripts %}
    <script src="{{bootstrap_find_resource('jquery.js', cdn='jquery')}}"></script>
    <script src="{{bootstrap_find_resource('js/bootstrap.js', cdn='bootstrap')}}"></script>
    {%- endblock scripts %}
    {%- endblock body %}
  </body>
{%- endblock html %}
</html>
{% endblock doc -%}
```

[Flask-Bootstrap4 4.0.2](https://pypi.org/project/Flask-Bootstrap4/)

[Flask-Bootstrap — Flask-Bootstrap 3.3.7.1 documentation](https://pythonhosted.org/Flask-Bootstrap/)

[![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com/docs/3.3//favicon.ico)Bootstrap · The world's most popular mobile-first and responsive front-end framework.](https://getbootstrap.com/docs/3.3/)

## Configuración de Flask

Para activar el *development mode* debes escribir lo siguiente en la consola:

```bash
export FLASK_ENV=development
echo $FLASK_ENV
```

**SESSION:** es un intercambio de información interactiva semipermanente, también conocido como diálogo, una conversación o un encuentro, entre dos o más dispositivos de comunicación, o entre un ordenador y usuario.

---

**Automatiza** el levantar tu entorno virtual de desarrollo (venv) con un script.

```sh
#!/bin/bash

pip install -r requierements.txt
source venv/bin/activate

export FLASK_APP=main.py
export FLASK_DEBUG=1
export FLASK_ENV=development

flask run
```

1. Crea un archivo nuevo en la raíz del proyecto y copia el script de arriba.
2. Guárdalo con extension .sh
3. Ejecútalo en consola: $ source file_name.sh
4. You welcome!

---

![Objetos-Flask.png](https://static.platzi.com/media/user_upload/Objetos-Flask-618e24d3-6fbb-4290-96c7-fb97b376d36f.jpg)

Como aporte en python existen varias formas de generar strings seguros.

Esta es una de ellas:

```python
import os, binascii
binascii.b2a_hex(os.urandom(20))
```

Que como resultado daria algo así:

```sh
'b6026f861fd41a94c3389d54293de9d04bde6f7c'
```

Otra forma es usando el modulo secrets que segun su propia documentación indica que se utiliza para generar números aleatorios criptográficamente fuertes, adecuados para administrar datos como contraseñas, autenticación de usuarios, tokens de seguridad y secretos relacionados, el codigo seria este:

```python
import secrets
secrets.token_hex(20)
```

Que como resultado daría algo así:

```sh
'ccaf5c9a22e854856d0c5b1b96c81e851bafb288'
```

Por ultimo se puede usar tambíen la función token_urlsafe con este codigo:

```python
secrets.token_urlsafe(20)
```

Obteniendo como resultado algo así:

```sh
'dxM4-BL1CPeHYIMmXNQevdlsvhI'
```

Una opción para agregar la configuración dentro del main:

![Captura.PNG](https://static.platzi.com/media/user_upload/Captura-7840be4c-a7ed-4843-9b69-308c2ca02370.jpg)
Correr la aplicación, con el famoso if **name** == ‘**main**’:
![Captura.PNG](https://static.platzi.com/media/user_upload/Captura-ff6c05b7-9930-46ea-a998-72748a4aa806.jpg)
Completo
![Captura.PNG](https://static.platzi.com/media/user_upload/Captura-4bc45961-d69f-45af-af01-ef6cb54c56de.jpg)

windows, para el powershell:

```sh
$env:FLASK_ENV="development"
$env:FLASK_APP="nombredeapp.py"
```

## Implementación de Flask-Bootstrap y Flask-WTF

En la documentacion de [WTForms](https://wtforms.readthedocs.io/en/2.3.x/validators/) nos hablan de los validadores estos son

- DataRequired(message=None) y podemos pasarle el parámetro message, este por defecto es None. Este validador revisa que el campo contenga Data, si esto es falso puedes enviar un mensaje de alerta a través del parámetro message.

- Email() Valida que el campo contenga un email, tiene distintos parámetros que puedes configurar, uno de ellos es el mensaje.

- EqualTo() este necesita el argumento del nombre del campo con el que va ser comparado, este validador nos dice si los dos campos tienen la misma información, también podemos modificar el mensaje que envie.

- InputRequired() Este verifica que exista información en el campo donde se está aplicando

- IPAddress() Por defecto valida direcciones IPv4, pero puedes modificarlo a IPv6 con el parametro IPAddress(IPv6=True)

- Length() Valida el tamaño de un string necesita los argumentos min, max si no se especifica alguno no lo tomará en consideración.

- MacAddress Verifica que sea una dirección Mac

- NumberRange() Valida un rango entre un mínimo y un máximo, el máximo es inclusive. Si falta alguno no será verificado.

- Optional() permite que hayan campos vacíos

- URL() Verifica que sea una URL la que se está pasando.

  Existen otros validadores e inclusive puedes hacer tus validadores propios, esto es explicado en la misma documentación con un ejemplo.

  ![awesome_flask_page.JPG](https://static.platzi.com/media/user_upload/awesome_flask_page-2189bd9d-c67f-4282-ba29-20a48cd0de46.jpg)

[Flask-WTF — Flask-WTF 0.14](https://flask-wtf.readthedocs.io/en/stable/)

## Uso de método POST en Flask-WTF

Flask acepta peticiones **GET** por defecto y por ende no debemos declararla en nuestras rutas.

Pero cuando necesitamos hacer una petición **POST** al enviar un formulario debemos declararla de la siguiente manera, como en este ejemplo:

```python
@app.route('/platzi-post', methods=['GET', 'POST'])
```

Debemos declararle además de la petición que queremos, **GET**, ya que le estamos pasando el parámetro *methods* para que acepte solo y únicamente las peticiones que estamos declarando.

De esta forma, al actualizar el navegador ya podremos hacer la petición **POST** a nuestra ruta deseada y obtener la respuesta requerida.

`main.py`

```python
from flask import Flask, request, make_response, redirect, render_template, session, url_for
...
@app.route('/hello', methods=['GET', 'POST'])
def hello():

  user_ip = session.get('user_ip')
  login_form = LoginForm()
  username = session.get('username')

  context = {
    'user_ip': user_ip, 
    'todos': todos,
    'login_form': login_form,
    'username': username
  }
  if login_form.validate_on_submit():
    username = login_form.username.data
    session['username'] = username
    return redirect(url_for('index'))

  return render_template('hello.html', **context)

```

`hello.html`

```jinja2
...
{% block content %}
  {% if username %}
    <h1>Bienvenido, {{ username | capitalize }}</h1>
  {% endif %}
 ...
```

## Desplegar Flashes (mensajes emergentes)

`main.py`

```python
from flask import Flask, request, make_response, redirect, render_template, session, url_for, flash

...
    flash('Nombre de usuario: registrado con Exito')
...
```

`base.html`

```jinja2
  % block body %} 
  {% block navbar %} 
    {% include 'navbar.html' %} 
  {% endblock %} 
  {% comment %} messages {% endcomment %}
  {% for message in get_flashed_messages() %}
    <div class="alert alert-success alert-dismissible">
      <button type="button" data-dismiss="alert" clase="close">&times;</button>
      {{ message}}
    </div>
  {% endfor %}
  {% block content %} 
  {% endblock %} 

{% comment %} bootstrap {% endcomment %}

  {% block scripts %}
    {{ super() }}
  {% endblock %}

{% endblock%}
```

Al usar categorias, pueden darle los nombres de las clases de bootstrap.

```python
flash(f’Nombre registrado {username}’, ‘danger’)
flash(f’Nombre registrado {username}’, ‘success’)
```

## Pruebas básicas con Flask-testing

La etapa de pruebas se denomina *testing* y se trata de una investigación exhaustiva, no solo técnica sino también empírica, que busca reunir información objetiva sobre la calidad de un proyecto de software, por ejemplo, una aplicación móvil o un sitio web.

El objetivo del *testing* no solo es encontrar fallas sino también aumentar la confianza en la calidad del producto, facilitar información para la toma de decisiones y detectar oportunidades de mejora.

```sh
pip install flask-test flask-testing
```

`main.py`

```python
import unittest

...
@app.cli.command():
def test():
  tests = unittest.TestLoader().discover('tests')
  unittest.TextTestRunner().run(tests)
```

ejecutar el test

```sh
export FLASK_APP=main.py
flask test
```

`test_base.py`

```python
from flask_testing import TestCase
from flask import current_app, url_for

from main import app

class MainTest(TestCase):
  def create_app(self):
    app.config['TESTING'] = True
    app.config['WTF_CSRF_ENABLED'] = False
    return app

  def test_app_exists(self):
    self.assertIsNotNone(current_app)
    # self.assertIsNone(current_app)


  def test_app_in_test_mode(self):
    self.assertTrue(current_app.config['TESTING'])


  def test_index_redirects(self):
    response = self.client.get(url_for('index'))

    self.assertRedirects(response, url_for('hello'))

  
  def test_hello_get(self):
    response = self.client.get(url_for('hello'))

    self.assert200(response)

  
  def test_hello_post(self):
    fake_form = {
      'username': 'fake',
      'password': 'fake-password'
    }
    response = self.client.post(url_for('hello'), data=fake_form)

    self.assertRedirects(response, url_for('index'))
```



# 4. Proyecto

## Planteamiento del proyecto: To Do List

**Que es un blueprint:** Un Blueprint es una forma de organizar un grupo de vistas relacionadas y otro código. En lugar de registrar vistas y otro código directamente con una aplicación, se registran con un plano. Luego, el plano se registra con la aplicación cuando está disponible en la función de fábrica.

- [Flask Documentation](https://flask.palletsprojects.com/en/1.1.x/)
- [Flask Documentation](https://flask.palletsprojects.com/en/1.1.x/)
- [Blueprints](https://flask.palletsprojects.com/en/1.1.x/tutorial/views/)
- [flask-login](https://flask-login.readthedocs.io/en/latest/)

## App Factory

![estructura_carpetas.PNG](https://static.platzi.com/media/user_upload/estructura_carpetas-203e5935-fb26-4503-875d-ff88afc8d239.jpg)

```sh
app
├── config.py
├── forms.py
├── __init__.py
├── __pycache__
│   ├── config.cpython-37.py
│   ├── forms.cpython-37.pyc
│   └── __init__.cpython-37.pyc
├── static
│   ├── css
│   │   └── main.css
│   └── images
│       └── platzi-logo.jpg
└── templates
    ├── 404.html
    ├── 500.html
    ├── base.html
    ├── hello.html
    ├── macros.html
    └── navbar.html

```

## Uso de Blueprints

`app.auth/__init__.py`

```python
from flask import Blueprint

auth = Blueprint('auth', __name__, url_prefix='/auth')

from . import views
```

`app.auth/views.py`

```python
from flask import render_template
from app.forms import LoginForm

from . import auth

@auth.route('/login')
def login():
	context = {
		'login_form': LoginForm()
	}
	return render_template('login.html', **context)
```

`template/login.html`

```jinja2
{% extends 'base.html' %}
{% import 'bootstrap/wtf.html' as wtf %}
{% block title %}
    {{ super() }}
    404
{% endblock %}

{% block content %}
    <div class="container">
        {{ wtf.quick_form(login_form) }}
    </div>
{% endblock %}
```

## Blueprints II

`app/auth/views.py`

```python
from flask import render_template, session, redirect, flash, url_for

from app.forms import LoginForm

from . import auth

@auth.route('/login', methods=['GET', 'POST'])
def login():
	login_form = LoginForm()
	context = {
		'login_form': login_form
	}

	if login_form.validate_on_submit():
		username = login_form.username.data
		session['username'] = username

		flash('Nombre de usario registrado con éxito!')

		return redirect(url_for('index'))

	return render_template('login.html', **context)
```

Gente esta clase esta algo pesada les comparto este tutorial para entender un poco mas a fondo lo que es y hacen los blueprints:

- [Explicación de los Blueprints en Flask(Python)](https://www.youtube.com/watch?v=3Yz6QanCSaA)

> El Blueprint es la viva expresión de Programación Orientada a Objetos.

## Base de datos y App Engine con Flask

- **Bases de Datos SQL:** su composición esta hecha con bases de datos llenas de tablas con filas que contienen campos estructurados. No es muy flexible pero es el más usado. Una de sus desventajas es que mientras más compleja sea la base de datos más procesamiento necesitará.
- **Base de Datos NOSQL:** su composición es no estructurada, es abierta y muy flexible a diferentes tipos de datos, no necesita tantos recursos para ejecutarse, no necesitan una tabla fija como las que se encuentran en bases de datos relacionales y es altamente escalable a un bajo costo de hardware.

![Datastore-Firestore-SQL.png](https://static.platzi.com/media/user_upload/Datastore-Firestore-SQL-5c0624ef-b3a5-4a5b-beeb-e1504216e77a.jpg)

- Flask no tiene un ORM por defecto.
- Podemos implementar la lógica para usar la BD que queramos
- Podemos extender un ORM SQL: https://flask-sqlalchemy.palletsprojects.com/en/2.x/
- Usaremos Firestore: NoSQL - Grupo de colecciones - documentos->row, field->Column, document ID-> primary key

Tutorial flask-SQLAchemy: https://platzi.com/tutoriales/1540-flask/7112-conectando-aplicacion-con-base-de-datos-relacional/

Un ORM es un modelo de programación que permite mapear las estructuras de una base de datos relacional (SQL Server, Oracle, MySQL, etc.), en adelante RDBMS (Relational Database Management System), sobre una estructura lógica de entidades con el objeto de simplificar y acelerar el desarrollo de nuestras aplicaciones.

Las estructuras de la base de datos relacional quedan vinculadas con las entidades lógicas o base de datos virtual definida en el ORM, de tal modo que las acciones CRUD (Create, Read, Update, Delete) a ejecutar sobre la base de datos física se realizan de forma indirecta por medio del ORM.

![img](https://cipsa.net/wp-content/uploads/01-img-Que-es-un-ORM-y-cuando-emplearlo.png.webp)

[Google Cloud Platform](https://console.cloud.google.com/)

[<img src="https://flask-sqlalchemy.palletsprojects.com/en/2.x/_images/flask-sqlalchemy-title.png" alt="_images/flask-sqlalchemy-title.png" style="zoom:25%;" />](https://flask-sqlalchemy.palletsprojects.com/en/2.x/)

## Configuración de Google Cloud SDK

Ahora vamos a instalar el Google Cloud SDK. Simplemente debemos descargar un ejecutable desde alguno de estos enlaces:

Para Windows dirígete a https://cloud.google.com/sdk/docs/quickstart-windows
Para MacOS dirígete a link https://cloud.google.com/sdk/docs/quickstart-macos
Para Linux dirígete a https://cloud.google.com/sdk/docs/quickstart-linux

Una vez que corrimos el instalador, podemos verificar que instalamos correctamente el SDK corriendo en una terminal el siguiente comando:

```sh
which gcloud
```

Ahora inicializamos *gcloud* y hacemos *login* con:

```sh
gcloud init
```

```sh
gcloud auth login
```


## Configuración de proyecto en Google Cloud Platform


## Implementación de Firestore


## Autenticación de usuarios: Login


## Autenticación de usuarios: Logout


## Signup


## Agregar tareas


## Eliminar tareas


## Editar tareas


## Deploy a producción con App Engine


## Conclusiones