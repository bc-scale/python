from re import I, X
import sys
import csv
import os

CLIENT_TABLE = '.clients.csv'
CLIENT_SCHEMA = ['name', 'company', 'email', 'position']
clients = []

def _initialize_clients_from_storage():
  with open(CLIENT_TABLE, mode='r') as f:
    reader = csv.DictReader(f, fieldname=CLIENT_SCHEMA)

    for row in reader:
      clients.append(row)

def _save_clients_to_storage():
  tmp_table_name = '{}.tmp'.format(CLIENT_TABLE)
  with open(tmp_table_name, mode='w') as f:
    writer = csv.DictWriter(f, fieldnames=CLIENT_SCHEMA)
    writer.writerows(clients)

    os.remove(CLIENT_TABLE)
    f.close()
    os.rename(tmp_table_name, CLIENT_TABLE)

def _exist_client(client):
  global clients

  total_clients = range(len(clients))
  x=0
  for x in total_clients:
    id=X
    if client['name'] in clients[x]['name']:
      return True,id
    else:
      continue

  id=x
  return False,id


def create_client(client):
  clients.append(client)


def client_not_list():
  print('Client in not in clients list')


def list_clients():
  for idx, client in enumerate(clients):
    print('{uid} | {name} | {company} | {email} | {position}'.format(
      uid=idx,
      name=client['name'],
      companu=client['company'],
      email=client['email'],
      position=client['position']
    )
    )


def update_client(client_name, updated_client_name, id):
    global clients
    clients[id]['name']=updated_client_name
    clients[id]['company']=_get_client_field('company')
    clients[id]['email']=_get_client_field('email')
    clients[id]['position']=_get_client_field('position')


def delete_client(client):
  global clients
  
  clients.pop(client)


def search_client(client):

  global clients
    
  total_clientes=range(len(clients))
  for x in total_clientes:
    if client['name'] in clients[x]['name']:
      id=x
      return True,x
    id=x
  return False,id


def _get_client_field(field_name):
  field = None

  while not field:
    field = input('What is the client {}?'.format(field_name))

    return field

def _get_client_name():
    field =None

    while not field:
        field = input('What is the client {}?'.format(field_name))
    return field


def _get_client_name():
    client_name=None

    while not client_name:
        client_name=input('What is the Client name?')
        if client_name=='exit':
            client_name=None
            break
    
    if not client_name:
            sys.exit()

    return client_name
def _print_welcome():
  print('WELCOME TO PLATZI VENTAS')
  print('*' * 50)
  print('What would yo like to do today?')
  print('[C]create client')
  print('(L)ist')
  print('[U]pdate client')
  print('[D]elete client')
  print('[S]earch client')


if __name__ == '__main__':
  _initialize_clients_from_storage()
  _print_welcome()

  command = input()
  command = command.upper()

  if command == 'C':
    client = {
      'name': _get_client_field('name'),
      'company': _get_client_field('company'),
      'email': _get_client_field('email'),
      'position': _get_client_field('position'),
    }
    create_client(client)
  elif command == 'D':
    client = _get_client_name()
    delete_client(client)
    list_clients()
  elif command == 'U':
    client = _get_client_name()
    update_client_name = input('What is the updated client name? ')
    update_client(client, update_client_name)
    list_clients()
  elif command == 'S':
    client = _get_client_name()
    found = search_client(client)

    if found:
      print('The client is in the client\'s list')
    else:
      print('The client: {} is not in our client\'s list'.format(client))
  else:
    print('Invalid command')
  
_save_clients_to_storage()