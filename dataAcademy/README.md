# Segunda Semana

## Explorando Python: operadores aritméticos

**OPEREADORES ARITMÉTICOS**

Suma 5 + 5
Restar 5 – 5
Multiplicar 2 * 5
Dividir 21 / 5
División euclidiana (Cociente) 21//5
Módulo (Resto de división) 21 % 5
Potencia 2 ** 2

**RAÍZ CUADRADA**

El cálculo de la raíz cuadrada de un número requiere importar una librería (math). Para hacerlo en la consola interactiva mediante una sola línea de código, hacemos potencia a 1/2
16 ** (0,5)

**PRECEDENCIA DE OPERACIONES**

1. Paréntesis
2. Potenciación
3. Multiplicación y división
4. Suma y resta

![Sin título.png](https://static.platzi.com/media/user_upload/Sin%20t%C3%ADtulo-7e5e14f5-1a89-489c-9c31-d62fe4226085.jpg)

1. usamos math.sqrt() para ello debemos importar la función

import math
math.sqrt(4)

me dará el reslutado 2

1. con mis compis aqui en los comentarios aprendí que para usar una sola línea de comando : hacemos la potencia de la fracción

> 4 ** 1/2

me da resultado 2

Utilice 

```python
math.sqrt() 
```

para calcular la raíz cuadrada de un número en Python
Utilice la función 

```python
pow() 
```

para calcular la raíz cuadrada de un número en Python
Utilice el operador 

```python
** 
```

para calcular la raíz cuadrada de un número en Python
Utilice 

```python
cmath.sqrt() 
```

para calcular la raíz cuadrada de un número en Python
Utilice 

```python
numpy.sqrt() 
```

para calcular la raíz cuadrada de un número en Python

[![img](https://www.google.com/s2/favicons?domain=https://www.w3schools.com/python/python_ref_keywords.asp/favicon.ico)Python Keywords](https://www.w3schools.com/python/python_ref_keywords.asp)

## ¿Qué es una variable?

Una variable es una pieza contenedora de información, la cual posee un nombre como identificador. Una variable puede contener elementos de distintos tipos.

Existen ciertas normas que se deben cumplir para declarar una variable en python:

- El nombre de la variable NO debe iniciar con un numero.
- El nombre debe estar en minúsculas.
- Si el nombre de la variable posee varias palabras se deben separar con “_” ya que no esta permitido el uso de espacio.

>  Dos investigadores han tenido la paciencia de comprobar empíricamente si es más preciso y legible el código que utiliza palabras clave y variables escritas como **PalabrasJuntas** o **espacios_subrayados** (CamelCase Vs. snake_case).
> ***Ganan los que reemplazan los espacios por «subrayados»\*** (guiones bajos o underscores). El estudio tiene sentido pues aspira a entender cuál de los dos estilos es óptimo en cuanto a comprensión y velocidad de lectura.
>
> ¿Entonce se podría decir que es mejor usar el snake_case siempre que se pueda? Estas nomenclaturas estan tambien en el curso de algoritmos y pensamiento logico.

![referencia_python-08967f82-4367-4e5c-b532-da74f02bada5.jpg](https://static.platzi.com/media/user_upload/referencia_python-08967f82-4367-4e5c-b532-da74f02bada5-b6516c75-8dd2-4098-9f4a-011d956150d1.jpg)

> _Una variable es un espacio de memoria en el que podemos guardar valores que cambian durante la ejecución del programa y al que asignamos un nombre, idealmente indicativo de su contenido_. 

## Los primitivos: tipos de datos sencillos

**Hay una premisa existente en python y es que todo es un objeto.**

Los cuatro tipos de datos clásicos o base son:

- Números enteros (Integer)
- Números flotantes (Float) (Decimales)
- Textos (String) Cadena de caracteres
- Booleanos (Boolean) Verdadero - Falso (True - False)

Las variables de **Texto** las podemos guardar utilizando comillas simples o comillas dobles. Todo lo que esté dentro de comillas en python es considerado un texto.

Los **textos** pueden ser concatenados a través del símbolo **+**, si los multiplicamos utilizando el operador * podemos repetir nuestro texto x cantidad de veces.

Los **números decimales (flotantes)** se separan con punto, ya que python es un lenguaje de programación basado en el idioma inglés donde las partes no exactas se separan con puntos y no con comas como lo hacemos en español.jjj

> _**4 Tipos de datos clásicos en Python:**_
>
> 1. Enteros (int) : 1,2,3,4,89
> 2. Decimales (float): 0.5, 9.89
> 3. Texto (string): “Hola Platzi”
> 4. Booleanos o falso-verdadero (bool): True, False
>
> _La concatenación es la unión de los textos_

## Convertir un dato a un tipo diferente

- Con la función ***int(x)*** podemos convertir lo que esté dentro de la ***variable x*** a entero.
- Con la función ***str(x)*** podemos convertir lo que esté dentro de la ***variable x*** a texto

Convertir un dato a otro tipo de dato es una operación común en los lenguajes de programación, esta operación se conoce como parseo (‘parse’ en inglés), siendo lo que se realiza es un parseo de la variable.
En python existen diversos métodos nativos para los tipos de datos primitivos.

- int(x) retorna la variable como un entero
- float(x) retorna la variable como un flotante
- str(x) retorna la variable como un string
- bool(x) retorna la variable como un booleano, cabe destacar que solo retornara falso a los valores falsy , en python son: `0`, `None`, `0.0`, `0j`, `False`

```python
numero1 = int(input("Escribe un numero: "))
numero2 = int(input("Escribe un numero:  "))

 resultado = numero1 + numero2
 
print(f'El resutlado de la suma es: {resultado}')
```

## Operadores lógicos y de comparación

El operador ***and*** evalúa y devuelve ***True (Verdadero)*** solamente si **TODOS** los argumentos o condiciones son verdaderos, de lo contrario retornará ***False (Falso)***

El operador ***or*** evalúa y devuelve ***True (Verdadero)*** si **CUALQUIERA** de los argumentos o condiciones son verdaderos, solo retorna ***False (Falso)*** cuando **TODOS** los argumentos sean falsos.

El operador ***not*** niega, o contradice la premisa inmediatamente siguiente.

Los operadores de comparación son los siguientes

- Igual: ***==***
- Diferente: ***!=***
- Mayor que: ***>***
- Menor que: ***<***
  Es posible combinarlos y para generar los siguientes
- Mayor o Igual ***>=***
- Menor o Igual ***<=***

Operadores lógicos en Python:

```python
AND
OR
NOT
```

```python
AND
V + V = V
V + F = F
F + V = F
F + F = F

	OR
V + V = V
V + F = V
F + V = V
F + F = F 	
```

Operadores de comparación en Pyhton:

```python
==
!=
<
>
<=
>=
```

## Tu primer programa: conversor de monedas

![Conversor2.PNG](https://static.platzi.com/media/user_upload/Conversor2-2e6c2553-dbaf-4dcc-9d3b-5436c9016020.jpg)

## Construyendo el camino de un programa con condicionales

![img](https://controlautomaticoeducacion.com/wp-content/uploads/condicionales-anidados.png)

> Es importante recordar que la identacion en python es parte de la sintaxis del mismo 😇

```python
menu = “”"
Hola! 😊 Estás a punto de cambiar tu futuro. Cuéntame de qué país eres:

1- Argentina
2- Colombia
3- Peru
4- Chile
5- Ecuador
6- Venezuela
7- Bolivia
8- Uruguay
9- Paraguay

Elige tu país: “”"

opcion = input(menu)

if opcion == ‘1’:
platzi_price = 190
pesos_argentinos =104.52
platzi_plan = platzi_price * pesos_argentinos
print(‘Adquiere nuestro Plan Expert, válido por un año a solo $’+ str(platzi_plan) + ’ pesos’)
elif opcion == ‘2’:
platzi_price = 190
pesos_colombianos =3945.74
platzi_plan = platzi_price * pesos_colombianos
print(‘Adquiere nuestro Plan Expert, válido por un año a solo $’+ str(platzi_plan) + ’ pesos’)
elif opcion == ‘3’:
platzi_price = 190
soles_peruanos =3.84
platzi_plan = platzi_price * soles_peruanos
print(‘Adquiere nuestro Plan Expert, válido por un año a solo S/.’+ str(platzi_plan) + ’ soles’)
elif opcion == ‘4’:
platzi_price = 190
pesos_chilenos =807.88
platzi_plan = platzi_price * pesos_chilenos
print(‘Adquiere nuestro Plan Expert, válido por un año a solo S/.’+ str(platzi_plan) + ’ pesos’)
elif opcion == ‘5’:
platzi_price = 190
print(‘Adquiere nuestro Plan Expert, válido por un año a solo $USD ‘+ str(platzi_price) + ’ dólares’)
elif opcion == ‘6’:
platzi_price = 190
bolivares = 46009784
platzi_plan = platzi_price * bolivares
print(‘Adquiere nuestro Plan Expert, válido por un año a solo ‘+ str(platzi_plan) + ’ VES’)
elif opcion == ‘7’:
platzi_price = 190
bolivianos =6.90
platzi_plan = platzi_price * bolivianos
print(‘Adquiere nuestro Plan Expert, válido por un año a solo S/.’+ str(platzi_plan) + ’ BOB’)
elif opcion == ‘8’:
platzi_price = 190
pesos_uruguayos =44.51
platzi_plan = platzi_price * pesos_uruguayos
print(‘Adquiere nuestro Plan Expert, válido por un año a solo S/.’+ str(platzi_plan) + ’ UYU’)
elif opcion == ‘9’:
platzi_price = 190
guaranies =6999.21
platzi_plan = platzi_price * guaranies
print(‘Adquiere nuestro Plan Expert, válido por un año a solo S/.’+ str(platzi_plan) + ’ PYG’)
else:
print(‘Elige uno de los países del listado’)
```

![platz.png](https://static.platzi.com/media/user_upload/platz-54abe4d8-bd4e-4159-b619-57a82421a4f0.jpg)

## Aprendiendo a no repetir código con funciones

Las funciones las creamos usando

```python
def nombre_funcion(parámetro):
	Código de la función
```

Entre los paréntesis podemos agregar los parámetros que corresponden a los elementos que estarán cambiando dentro de nuestra función.

La modularidad es, en programación, la propiedad que permite subdividir una aplicación en partes más pequeñas (llamadas módulos), cada una de las cuales debe ser tan independiente como sea posible de la aplicación en sí y de las restantes partes.
	Este concepto lo aplicaras mucho en funciones, y entre más independientes sean mejor asi las podemos reutilizar en otros proyectos / scripts / programas

## Modularizando nuestro conversor de monedas

### ¿Qué es una función?

Una función es un bloque de código que se ejecuta solo cuando es invocada. Una función puede retornar un dato como un resultado

#### ¿Para qué se usa una función?

- Se usan para evitar repetir código
- Seccionar un programa en módulos. Cada función puede ser un módulo.

#### ¿Cómo se define una función?

1. Definir función: En Python, se usa la palabra reservada *def* para definir una función, seguido del nombre de la función, parentesis ( ) y “:” ->

   ```python
   def nombre_funcion( ):
   ```

   

2. Lógica de la funcion. Es la expresión o lógica que se desea ejecutar. Se indenta, dejando 4 espacios ->

   ```python
   print(“Esto es una función”)
   ```

   

3. Invocación de la función ->

   ```python
   nombre_funcion( )
   ```

```python
def nombre_funcion():
    print("Esto es una función") # línea indentada -> 4 espacios

nombre_funcion() # invocacion de la funcion
```

#### Parámetros y Argumentos

1. Las funciones pueden recibir variables listadas dentro del paréntesis en la definición de la función. A estas variables se les conoce como parámetros.
2. Los valores que se le asigna a esas variables, se les conoce como argumentos

```python
defsuma(a,b):# variables "a" y "b". Son Parámetros
    return a + b

print(suma(45,78)) # valores 45 asignado a "a" y 78 asignado a "b". Son argumentos
```

## Trabajando con texto: cadenas de caracteres

Un método es una función asociada a un tipo de dato en especial.

Algunos de los métodos que podemos utilizar en variables de texto son:

**.upper()** convierte en mayúsculas la cadena de caracteres.

- ```python
  variable.upper()
  ```

  **.capitalize()** convierte en mayúsculas la primera letra de la cadena de caracteres

- ```python
  variable.capitalize()
  ```

  **.strip()** Elimina los espacios basura que puedan estar al comienzo o al final de la cadena de caracteres.

- ```python
  variable.strip()
  ```

**.lower()** convierte el texto de la cadena de caracteres en minúsculas.

- ```python
  variable.lower()
  ```

**.replace(x,y)** reemplaza los caracteres iguales a ‘x’ con el caracter ingresado en ‘y’

- ```python
  variable.replace(x,y)
  ```

**len()** nos devuelve el número de caracteres con los que cuenta una cadena de texto.

- ```python
  len(variable)
  ```

  ![operacionescadenas.JPG](https://static.platzi.com/media/user_upload/operacionescadenas-0cf0187f-9c5e-46ba-8f4c-c5a95586882a.jpg)

**Metodos de Formato**

![Formato1.JPG](https://static.platzi.com/media/user_upload/Formato1-4693c65c-6ba1-420e-b720-6303151c07b3.jpg)

![Formato2.JPG](https://static.platzi.com/media/user_upload/Formato2-fe0f784a-c510-4360-b3d6-045ba799f550.jpg)

![Formato3.JPG](https://static.platzi.com/media/user_upload/Formato3-37140c8e-59ac-4398-8b32-90f2453a3854.jpg)

![Formato4.JPG](https://static.platzi.com/media/user_upload/Formato4-62c45d08-1189-4077-b86e-4735242de3eb.jpg)

![T Functions.png](https://static.platzi.com/media/user_upload/T%20Functions-af89400d-2be2-4f37-9e68-fb34a6d96d54.jpg)

![Métodos de Strings](https://i.imgur.com/9YB9l64.jpg)

### Tipos de funciones

- `Built-in`: son las funciones proporcionadas por el lenguaje. Por ejemplo print(), input(), son funciones integradas que no se requieren definirlas.
- `User-defined`: son funciones definidas por el usuario, escritas por los desarrolladores.

## Trabajando con texto: slices

**Slices**

- ```python
  - example[0:2] devuelve ‘ex’
  - example[:4] devuelve ‘exam’
  - example[1:5] devuelve ‘xamp’
  - example[:] devuelve ‘example’
  - example[1:5:2] devuelve ‘ap’
  - example[::2] devuelve ‘xml’
  - example[::-1] devuelve ‘elpmaxe’
  ```

  **Slices o rebanadas en Python [ ]**

  - Los slices, traducidos al español como “rebanadas”, nos permiten dividir los caracteres de un string de múltiples formas.
  - **`[indice inicial: indice final: pasos]`** Se pueden omitir indice final y pasos

  ![Slices.JPG](https://static.platzi.com/media/user_upload/Slices-9846a7c7-5140-4de3-a7c5-abf739d6f4a6.jpg)

![SLICES.JPG](https://static.platzi.com/media/user_upload/SLICES-582cb6cb-d82e-4d88-9847-936bd3afd32f.jpg)

```python
nombre = "Francisco"
nombre
"Francisco"
nombre[0 : 3)
       # Arranca desde el primer índice hasta llegar antes del 3° índice.
"Fra"
nombre[:3] 
       # Va desde el principio hasta antes de llegar del 3° índice. Cómo no hay ningún parámetro en el primer lugar, se interpreta que arranca desde el principio.
"Fra"
nombre[1:7] 
       # Arranca desde el índice 1 hasta llegar antes del 7.
"rancis"
nombre[1:7:2]
       # Arranca desde el índice 1 hasta llegar antes del 7, pero pasos de 2 en 2, ya que eso es lo que nos indica el 3! parámetro, el cual es 2.
nombre[1::3] # Arranca desde el índice 1 hasta el final del string (al no haber ningún 2° parámetro, significa que va hasta el final), pero en pasos de 3 en 3.
"rcc"
       
nombre[::-1] # Al no haber parámetro en los 2 primeros lugares, se interpreta que se arranca desde el inicio hasta el final, pero en pasos de 1 en 1 con la palabra al revés, porque es -1.
"ocsicnarF"
```

## Proyecto: palíndromo

```python
def palindromo(palabra):
    texto=palabra.replace(" ","")#quitar espacios
    texto=texto.lower()#por si hay mayúsculas
    reves=texto[::-1]
    if texto==reves:
        return True
    else:
        return 


def run():
    palabra = input("Escribe una frase: ")
    es_palindromo = palindromo(palabra)
    if es_palindromo == True:
        print("Es palíndromo")
    else:
        print("No es palíndromo")

if __name__ == '__main__': #punto de entrada de un archivo python
    #una vez que se ejecuta un archivo python se ejecuta esto
    run() 
```

> El manejo de Strings, es algo que se suele ver mucho en las entrevistas para trabajos de programador, y el de esta clase es un caso comun.

> Básicamente 
>
> ```python
> if __name__ == “__main__”:
>     name_function
> ```
>
>  se utiliza cuando queremos que un código se ejecute únicamente en el módulo en el que estamos (módulo principal), y que no sea posible ejecutarlo desde otro módulo o desde fuera (modulo secundario); así tendremos mas control sobre nuestras aplicaciones y podemos hacerlas mas seguras.

# Tercera Semana

## Aprendiendo bucles

Un bucle o ciclo, en programación, es una secuencia de instrucciones de código que se ejecuta repetidas veces, hasta que la condición asignada a dicho bucle deja de cumplirse.

Los tres bucles más utilizados en programación son el bucle while, el bucle for y el bucle do-while.

Los 3 bucles existen y son programables en Python

![bucles.png](https://static.platzi.com/media/user_upload/bucles-24297743-e1a5-4e73-b28e-850ab645d412.jpg)

> Los bucles son indispensables en la repetición que apreciamos en la automatización de procesos, por ejemplo, un PLC (controlador lógico programable) el cual desempeña una tarea de manera repetitiva según las necesidades de producción.

## El ciclo while

"**ENTRY POINT**"
que es este👇

```python
if __name__ == '__main__:
	run()
```

El profesor Facundo nos menciona que ya lo habíamos visto en la clase de palíndromos, y básicamente un “**ENTRY POINT**” se traduce al español como punto de entrada y nos sirve para definir como programa o aplicación principal que queremos ejecutar !

**TIP:** Para definir constantes, se deben poner los nombres en mayúscula.

```python
print("Numbers to the power ")

def power():
    i=1
    v = 2
    while v <= 1000:
        print("Number 2 to the power of " + str(i) + " is equal to " + str(v))
        i += 1
        v = 2 ** i


if __name__ == "__main__":
    power()
```

Pueden usar el formato en el print, utilizando el siguiente codigo

```python
print(f"2 elevado a {contador} es igual a: {potencia_2}")
```

## Explorando un bucle diferente: el ciclo for

```python
# print(1)
contador = 1
print(contador)
while contador < 1000:
  contador += 1
  print(contador)
```

**while:**

For es un ciclo cuyo final es definido, siempre sabes cuando va a terminar, le programas los parámetros y el ciclo no pasará de ellos

While es un ciclo indeterminado, porque no sabes cuando va a terminar, puede terminar tras una iteración o tras 1000 iteraciones, lo único que haces es ponerle una condición de quiebre.

La función tiene 3 parámetros

```python
range(inicio, fin, saltos)
```

donde el primer número indica en que número se comienza la lista, el segundo es para saber dónde acaba la lista y el tercero es para saber de cuanto en cuanto va, por ejemplo si pongo

```python
range(4, 11, 2)
```

La lista resultante sería 4, 6, 8, 10 donde comienza en 4 termina en el 10 y va de 2 en 2

> Adicional hay instrucciónes que podemos usar:
> **BREAK**, rompe el bucle.
> **CONTINUE**, continua el bucle.
> **PASS**, ingnora lo evaluado.
> Todas suele ir posterior a una condicional

**Multiplicar**

```python
n = int(input('''
¡Hola!
Yo te ayudaré a repasar la tabla de multiplicar. :)
Escribe de qué número quieres repasar la tabla: '''))
for i in range(1, 13):
    print(str(n)+' por '+str(i)+' es '+str(n*i))
```

## Recorriendo un string con for

> Si deseas imprimir sin saltos de línea:
>
> ```python
> print(caracter.upper(), end='') 
> ```

Una forma de recorrer el string y obtener sus índices es:

```python
for i, letter in enumerate(text):
    print(f'índice {i}: letra {letter}')
```

Salida:

```sh
índice 0: letra p
índice 1: letra l
índice 2: letra a
índice 3: letra t
índice 4: letra z
índice 5: letra i
índice 6: letra n
índice 7: letra a
índice 8: letra u
índice 9: letra t
índice 10: letra a 
```

![bucledaniela.png](https://static.platzi.com/media/user_upload/bucledaniela-f58d5b60-3643-4939-b9fb-de1951eb3402.jpg)

## Interrumpiendo ciclos con break y continue

solución del reto, hice un pequeño sistema pomodoro, si quieres escuchar la alarma usa PowerShell para ejecutarlo 😉

```python
<'''Pomodoro es un sistema para que administres el tiempo mientras trabajas,
consiste en colocar un temporizador de 25 minutos y descansar 5 minutos,
repetir el ciclo 4 veces y posterior aumentar el tiempo de descanso o directamente parar'''  
import time 
import winsound

def contador(segundos):
    while segundos > 0:
        m,s = divmod(segundos, 60) #con divmod obtenemos el cociente y el residuo de la división de los valores ingresados, ambos lo guardamos en las variables m y s.
        formato_minuto_segundo = '{:02d}:{:02d}'.format(m,s)#''.format() es una función que usa como formato lo que este en las comillas y pone los valores de las variables ingresadas en el parentesis, lo que esta en las comillas en este caso es un mini lenguaje de python enfocado en los strings, aqui su documentación https://docs.python.org/2/library/string.html#formatspec
        print(formato_minuto_segundo)
        time.sleep(1)#(Segundos), le decimos al ciclo que duerma 1 segundo usando nuestro modulo time y la funcion sleep()
        segundos -= 1


def alarma():
    sonido = 0
    while True:#Comando para hacer un while infinito
        DURATION = 500  # Milisegundos
        FREQ = 450  # Hz
        winsound.Beep(FREQ, DURATION)
        sonido += 1
        if sonido < 15:
            continue
        else: 
            break

def run():
    menu = '''¿Vas a empezar tu tiempo de trabajo?
    Presiona 1 si quieres empezar tu cuenta pomodoro o enter si caiste por error y quieres cerrar el programa: '''
    menu = bool(input(menu))
    if menu:
        pomodoros =  0
        while True:
            contador(25*60)
            alarma()
            print('Descansa 5 minutos.')
            contador(5*60)
            alarma()
            print('Vuelve a trabajar')
            pomodoros += 1
            if pomodoros < 4:
                continue
            else:
                print('Tu ciclo pomodoro esta completo, toma un respiro ;)')
                break
    else:
        pass


if __name__ == '__main__':
    run()
```

`Code`

```python
import random

def run():
    presupuesto = int(input("Ingrese su presupuesto de compra: s/."))   //# Se ingresa el presupuesto para la compra, con ello se comprarán tantos productos se pueda.
    canasta = 0                         //# Valor inicial de la canasta
    producto  = 1                      // # Auxiliar numérica para la impresión de la cantidad de productos comprados
    precio_prod = random.randint(5,30)  # El precio de cada producto estará entre 5 y 30 soles peruanos
    while presupuesto >= 0:             # Mientras el presupuesto sea mayor o igual a 0,
        presupuesto = presupuesto - precio_prod   # Se restará el precio de cada producto al presupuesto
        canasta += precio_prod              # Se suma cada precio de producto a la canasta
        print("Producto " + str(producto) + ": s/." + str(precio_prod))  # Se imprime el precio de cada producto
        producto += 1                       # Aumenta en una unidad la cantidad de productos
        precio_prod = random.randint(5,30)  # El precio de cada producto estará entre 5 y 30 soles peruanos
        if precio_prod > presupuesto:       # Si el precio del producto es mayor que el presupuesto, termina la compra
            break
    print("------------------")
    print("El total a pagar es s/." + str(canasta))
    print("------------------")
    print("Te quedó s/." + str(presupuesto))

if __name__ == "__main__":
    run()
```

![2.JPG](https://static.platzi.com/media/user_upload/2-92d151b8-e3d2-46ff-a5ec-3622edef1bf6.jpg)

`Tabla_multiplicacion.py`

```python
# Desafio: Hacer uso de las palabras clave break y continue en un ciclo while
tabla = int(input("""
    Programa que te muestra las tablas de multiplicar pares
    Ingrese la tabla de multiplicar que quiere ver:
"""))

contador = 1
while contador < 20:
    contador += 1
    
    if contador == 15:
        break

    if contador %2 != 0:
        continue
    print('Las tablas pares son:')
    print(str(tabla) + ' x ' + str(contador) + ' = ' + str(contador * tabla))
```

## Almacenar varios valores en una variable: listas

Aquí vimos sobre las listas, pero las listas pertenecen a un universo de cosas interesantes: las estructuras de datos.

![TreeStructure-Data-Structures-in-Python-Edureka1.png](https://static.platzi.com/media/user_upload/TreeStructure-Data-Structures-in-Python-Edureka1-d29e23c7-dbd0-4473-ae51-e37ff2dc6dae.jpg)

Métodos para las listas:
`append()` -> agrega al final de la lista
`extend(iterable)` -> agrega todo el iterable a la lista
`pop()` -> lleva índice como parámetro
`insert(i, x)` -> El primer parametro es la posicion donde quiero insertar, el segundo es el contenido a insertar.
`remove(x)` -> remueve x la primera vez que aparezca en la lista.
`clear()` -> Limpia la lista. La deja vacia
`reverse()` -> Invierte el orden de los elementos de la lista.

Hay mas pero me parecieron los mas destacados.

Las listas se pueden remover.

✨ Listas:
Almacenan distintos tipos de datos: Se pueden agregar números, strings(caracteres de texto), booleanos(True or false). Los elementos van entre corchetes " [ ] "

> Ejemplo:
> lista1 = [2,4,8,‘Hola’, True]

Para acceder a sus elementos de las listas se usan los índices según el lugar que ocupan, el primer lugar empieza con el número 0.

> Ejemplo: lista1 = [2,4,8,‘Hola’, True]
> lista1[0]
> ➡️ 2

Agregar elementos a una lista ya existente: usamos el método " nombrelista.append(valor_a_agregar)"

> Ejemplo:
> lista1.append(20)
> lista1
> ➡️[2,4,8,‘Hola’, True,20] (Se agregó el 20)

Borrar elementos de la lista: usamos el método: "nombrelista.pop(#índice)

> Ejemplo:
> lista.1pop(5)
> 20
> ➡️lista1 = [2,4,8,‘Hola’, True] (el #20 se eliminó ya que ocupaba el índice 5)

Además las listas son el equivalente un Array en JavaScript o C++

![listas.JPG](https://static.platzi.com/media/user_upload/listas-6ab399e5-823b-424c-8519-c86a66791eb9.jpg)

## Entendiendo cómo funcionan las tuplas

> Una pregunta muy recurrente en entrevistas para posiciones de desarrollador en Python, es ¿Cuál es la diferencia entre lista y tupla? R.- Las Tuplas son inmutables y las listas pueden añadir nuevos elementos y la otra diferencia es que el procesamiento de una tupla es más rápido que el de una lista.

Las tuplas son usadas para almacenar varios datos en una sola variable.

La tupla es una de las cuatro estructuras de datos nativas en Python, todas sirven para almacenar grupos de datos

Las otras 3 son Listas/ List, Conjuntos/Set, Diccionarios/Dict todas con diferentes cualidades y usos.

La tupla/Tuple es una colección o agrupación que esta ordenada y es inmutable

Te recomiendo [esta pagina](https://www.w3schools.com/python/python_tuples.asp) para aprender claro y directo todos estos temas

> TUPLA y String: Inmutables (No pueden agregar o quitar elementos)

***Tuplas\***

En Python, una tupla es un conjunto ordenado e inmutable de elementos del mismo o diferente tipo. Conjunto de elemtos que **nunca van a cambiar**

<aside>
💡 Tuplas ≠ Listas (Ya que tuplas no se pueden modificar, es **inmutable)**

</aside>

Las tuplas se representan escribiendo los elementos entre paréntesis y separados por comas.

```python
mi_tupla=(1,2,3,4,5,6)
print(mi_tupla)
>>>
(1, 2, 3, 4, 5, 6)

for i in mi_tupla:
    print(i, end=' ')
>>>
1 2 3 4 5 6

for i in range(len(mi_tupla)):
    print(mi_tupla[i])
>>>
1 2 3 4 5 6
```

***Caracteristicas Tuplas\***

- Las tuplas se **almacenan en un solo bloque de memoria** (las listas usan 2 bloques o más) Como se almacenan en 1 solo bloque son más rápidas de procesar(creación) e indexar que las listas porque usa menos punteros de memoria.
- Las tuplas utilizan menos memoria a diferencia de las listas (que utilizan más).
- Las tuplas se pueden utilizar en un diccionario como Llaves, esto no se puede hacer con listas.
- Las tuplas son Inmutables (no se pueden cambiar elementos una vez creadas).
- Son ideales para usarlas como parámetros en funciones que no queremos sean modificados

![tupla.JPG](https://static.platzi.com/media/user_upload/tupla-3d2d5b23-6ce0-4be9-a3f5-1357b363c8b8.jpg)

## ¿Qué son los diccionarios?

Tres funciones o métodos clave al tratar con diccionarios:

- ***.keys()*** : Nos permite acceder a las claves
- ***.values()*** : Nos permite acceder a los valores asignados a cada clave.
- ***.items()*** : Nos permite acceder a ambos valores de manera simultánea.

***Diccionario***

- Un Diccionario es una estructura de datos y un tipo de dato en Python con características especiales que nos permite almacenar cualquier tipo de valor como enteros, cadenas, listas e incluso otras funciones.
- Estos diccionarios nos permiten además **identificar** cada elemento por una **clave (Key)**. Se escriben entre { }.

***💡 A Diferencia de las listas el índice del diccionario es cualquier llave (Key) que le agregar, son \**mutables\** (Se pueden editar)***

```python
mi_diccionario = {
    'llave1': 1,
    'llave2': 2,
    'llave3': 3,
}
print(mi_diccionario)
>>>
{'llave1': 1, 'llave2': 2, 'llave3': 3}

print(mi_diccionario['llave1']) >>> 1
print(mi_diccionario['llave2']) >>> 2
print(mi_diccionario['llave3']) >>> 3
```

***Operaciones mas habituales con Diccionarios***

***keys()*** —> Retorna las llaves del diccionario.

```
salario_dicc = {
    'gerente': 5000000,
    'director': 450000,
    'coordinador': 370000,
    'programador': 3500000,
    'auxiliar': 1700000,
    'operador': 1500000,
}

for cargo in salario_dicc.keys():
    print(cargo, end=' ')
>>>
gerente director coordinador programador auxiliar operador
```

***values()*** —> Retorna los valores o elementos del diccionario

```
salario_dicc = {
    'gerente': 5000000,
    'director': 450000,
    'coordinador': 370000,
    'programador': 3500000,
    'auxiliar': 1700000,
    'operador': 1500000,
}

for cargo in salario_dicc.values():
    print(cargo, end=' ')
>>>
5000000 450000 370000 3500000 1700000 1500000
```

***items()*** —> Devuelve (primero la clave o llave y luego el valor)

```
población_paises = {
	'Argentina': 44_938_712,
	'Brasil': 210_147_125,
	'Colombia': 50_372_424
}

for pais, poblacion in población_paises.items():
    print(f'{pais} tiene {poblacion} habitantes')

>>>
Argentina tiene 44938712 habitantes
Brasil tiene 210147125 habitantes 
Colombia tiene 50372424 habitantes
```

- **`clear()`** —> Elimina todos los ítems del diccionario

  ```python
  salario_dicc = {
      'gerente': 5000000,
      'director': 450000,
      'coordinador': 370000,
      'programador': 3500000,
      'auxiliar': 1700000,
      'operador': 1500000,
  }
  
  salario_dicc.clear()
  print(salario_dicc)
  >>>
  {}
  ```

- **`pop(“n”)`** —> remueve específicamente una clave de diccionario y devuelve valor correspondiente. Lanza una excepción KeyError si la clave no es encontrada.

  ```python
  salario_dicc = {
      'gerente': 5000000,
      'director': 450000,
      'coordinador': 370000,
      'programador': 3500000,
      'auxiliar': 1700000,
      'operador': 1500000,
  }
  
  print(salario_dicc)
  salario_dicc.pop('director')
  print(salario_dicc)
  
  >>>
  {'gerente': 5000000, 'director': 450000, 'coordinador': 370000, 'programador': 3500000, 'auxiliar': 1700000, 'operador': 1500000}
  {'gerente': 5000000, 'coordinador': 370000, 'programador': 3500000, 'auxiliar': 1700000, 'operador': 1500000}
  ```

- **`get()`** —> Recibe como parámetro una clave, devuelve el valor de la clave. Si no lo encuentra, devuelve un objeto none

  ```python
  salario_dicc = {
      'gerente': 5000000,
      'director': 450000,
      'coordinador': 370000,
      'programador': 3500000,
      'auxiliar': 1700000,
      'operador': 1500000,
  }
  
  print(salario_dicc.get('director'))
  print(salario_dicc.get('vigilante'))
  
  >>>
  450000
  None
  ```

## ¿En qué lugares programar para ciencia de datos?

Example 

> Mi mayor motivación es incentivar a las empresas o al equipo de trabajo a tomar decisiones estratégicas a partir de los datos, de esta manera se fortalece el conocimiento del negocio y permite detectar oportunidades de crecimiento o de mejora. Adicionalmente, me interesa adquirir conocimientos y experiencia en el manejo de las herramientas de big data y Business Analytics.

**Apuntes ¿En qué lugares programar para ciencia de datos? Notebooks vs Scripts_**

- Se recomienda usar algún Sistema Operativo basado en **UNIX** usando **Linux, MacOS** o **WSL** (Windows Subsystem for Linux)

- **NoteBooks**

  Son un **entorno eficiente** y moderno que combina código, visualizaciones y mapas al vuelo y herramientas de **datos**. En el editor de **notebooks**, puede escribir, documentar y ejecutar código de Python en un único lugar.

- **Scripts**

  Un **script**, secuencia de comandos o guion es un término informal que se usa para designar a un programa relativamente simple. → Es el **codigo del programa** escrito en **texto plano**

- **NoteBook Vs Script**

  - Ambos son útiles, aunque los Scripts son mas directos,
  - Notebooks te permiten ver lo que haces, a medida de que lo haces, en estos puedes encargarte de experimentar y hacer el prototipado de tu script y finalmente pasarlo a un Script cuando ya este listo y estés seguro de que todo funciona como es esperado

![CienciaDatos.JPG](https://static.platzi.com/media/user_upload/CienciaDatos-8c565b5e-f870-4bc8-bf8b-e223449487af.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://jupyter.org//favicon.ico)Project Jupyter | Home](https://jupyter.org/)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)https://research.google.com/colaboratory/](https://research.google.com/colaboratory/) 

## Google Colab: primeros pasos

[CPU–GPU–TPU](https://leslysandra.medium.com/cpu-gpu-tpu-e4a686d3dbc9)

CPU: proviene de Central Processing Unit, o Unidad Central de Procesamiento, es el encargado de ejecutar todos los programas, desde el sistema operativo hasta las aplicaciones de usuario; solo ejecuta instrucciones en lenguaje binario, realizando operaciones aritméticas y lógicas simples, tales como sumar, restar, multiplicar, dividir, las lógicas binarias y accesos a memoria.

​	GPU: viene de Graphics Processor Unit / Unidad de Procesamiento Gráfico es un coprocesador dedicado al procesamiento de gráficos u operaciones de coma flotante, para aligerar la carga de trabajo del procesador/CPU central en aplicaciones como los videojuegos o aplicaciones 3D interactivas. De esta forma, mientras gran parte de lo relacionado con los gráficos se procesa en la GPU, la unidad central de procesamiento (CPU) puede dedicarse a otro tipo de cálculos (como la inteligencia artificial o los cálculos mecánicos en el caso de los videojuegos).

​	TPU: proviene de Tensor Processing Unit / Unidad de Procesamiento ensorial, es un circuito integrado de aplicación específica y acelerador de IA (ASIC, AI accelerator application-specific integrated circuit) desarrollado por Google para el aprendizaje automático con redes neuronales artificiales y más específicamente optimizado para usar TensorFlow, la biblioteca de código abierto para aprendizaje automático.

***Google Colab: primeros pasos***

- Es una herramienta basada en la nube que te permite trabajar en notebooks, y se guardan en tu cuenta de Google Drive 😃.

- **Nube vs local**: Ambas son muy útiles, pero se diferencian en:

  - Configuración de entornos, ya que en la nube ya están precargadas (Librerias, Herramientas Data Science), y de local tienes que configurarlo manualmente.
  - Tiempo de ejecución (Mejor en Nube cuando los datos son muy grandes)
  - Escalabilidad: la nube tiene más poder porque puedes rentarlo!. 💸

- **Que es Google Colab**:

  - Servicio en la nube basado en Jupyter Notebooks,
  - No requiere configuración
  - Tiene un trabajo a nivel de archivo (el notebook es la base del proyecto).
  - Tiene uso de gratuito de GPUs y TPUs para correr modelos grandes. ☁️ (Poder de computo y ejecucion)

- **Caracteristicas Google Colab**

  - Puedes acceder a Google Colab desde tu drive o desde el navegador.

  - Para aprender **Markdown**. [Markdown Guide](https://www.markdownguide.org/) 📚

  - Las variables son persistentes (se conservan) entre celdas de código!. 🔥

  - Para llamar a la 

    línea de comandos

    , debemos usar primero un signo de admiración `!`

     y luego un comando válido, por ejemplo

    - `!pwd` o `!pip install session-info`.

- **Instalar Librerias Google Colab**

  1. Instalamos la libreria con el comando

     ```shell
     !pip install + <Library Name>
     ```

     

  2. Importamos la libreria al Nottebook con `import + <Library Name>`

  > No aparece > Google Colaboratory
  > Estos son los pasos para que se muestre:
  >
  > 1. Seleccionen “New” -> “More” -> “Connect More Apps”
  > 2. En la pantalla que se muestra dentro de la barra de busqueda escriban “Colaboratory” y ENTER
  > 3. Se muestra la aplicacion, seleccionenla y posterior presionen el boton INSTALL, pedira a que cuenta y LISTO!!!

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)LaTeX - A document preparation system](https://www.latex-project.org/)

[![img](https://www.google.com/s2/favicons?domain=https://ssl.gstatic.com/images/branding/product/1x/drive_2020q4_32dp.png)introduccion_colab.ipynb - Google Drive](https://drive.google.com/file/d/1ZNrGJdIHb0mvnwkNbdyaoV4uIzYBuRmJ/view?usp=sharing)

[![img](https://www.google.com/s2/favicons?domain=https://colab.research.google.com/notebooks//img/favicon.ico?vrz=colab-20220127-060057-RC01_424656678)Google Colab](https://colab.research.google.com/notebooks/)

[![img](https://www.google.com/s2/favicons?domain=https://www.markdownguide.org//favicon.ico)Markdown Guide](https://www.markdownguide.org/)

## Google Colab: ciencia de datos

***Herramientas de Google Colab: para ciencia de datos\***

Puedes cargar archivos a tu notebook desde tu computadora, pero se borrarán una vez cierres tu notebook. También puedes vincular tu google drive para que tome los archivos desde ahí y de esta forma conservarlos.

**Conectar Archivos de Drive con Notebook**

- Seleccionar `Mount drive` desde el gestor de archivos del Notebook

- Colab creara un fracmento de codigo para acceder a archivos de drive

  ```python
  from google.colab import drive
  drive.mount('/content/drive')
  ```

- Se ejecuta comando `!cat + <Ruta>` para acceder a info del archivo

  - Ejemplo **`!cat drive/MyDrive/iris.csv`**

- **Librerías para Ciencia de Datos instaladas en Colab**

  Colab está enfocado a trabajar con Python (también puede usar otros lenguajes) y ya trae librerías de ciencia de datos precargadas como:

  - 📊 **matplotlib**: Generación de gráficos a partir de listas o arrays.
  - 🧑‍💻 **numpy**: Cómputo científico para la manipulación de vectores.
  - 🧑‍💻 **pandas**: Manipulación y análisis de datos de tablas y series temporales.
  - 🧑‍💻 **scipy**: Herramientas y algoritmos matemáticos.
  - 📊 **seaborn**: Visualización de datos estadísticos.

  ```python
  # Como Importar estas librerias en Colab
  
  import matplotlib.pyplot as plt
  import numpy as np
  import pandas as pd
  import scipy as sc
  import seaborn as sns
  ```

- **Codigo Pre-Escrito en Colab**

  - Colab también tiene fragmentos de código (parecido a la herramienta para insertar funciones de Excel) que te facilita la programación.
  - Desde la seccion de codigo se pueden encontrar frangemntos de codigo pre-escrito que facilitan la labor de analisis de datos

- **Shortcuts Atajos de teclado Colab**

  - **`Ctrl + Shift + P`** abres la paleta de comandos, si escribes shortcuts o atajos de teclado te mostrará una lista de todos los atajos que puedes ejecutar en Colab.
  - **`Ctrl + M + H`** Muestra Shortcuts mas usados Colab y permite Set Shortcuts

  Herramientas de Google Collab en Ciencia de datos:

  - Subir archivos (Al almacenamiento de sesión o Montar Drive)
  - Librerias para CD:
    a. matplotlib (Generar gráficos)
    b. numpy (Manipulación de vectores)
    c. pandas (Análisis de datos)
    d. scipy (Herramientas)
    e. seaborn (Visualización)
  - Code Snippets (Ejemplos de código que puedes utilizar)
  - Keyboard shortcuts (Atajos de teclado)

[![img](https://www.google.com/s2/favicons?domain=https://colab.research.google.com/notebooks//img/favicon.ico?vrz=colab-20220127-060057-RC01_424656678)Google Colab](https://colab.research.google.com/notebooks/)

[![img](https://www.google.com/s2/favicons?domain=https://ssl.gstatic.com/images/branding/product/1x/drive_2020q4_32dp.png)introduccion_colab.ipynb - Google Drive](https://drive.google.com/file/d/1ZNrGJdIHb0mvnwkNbdyaoV4uIzYBuRmJ/view?usp=sharing)

## Utilizar Deepnote

Que gran herramienta deepnote, especialmente para:

- hacer un portafolio y poder compartir con terceros…
- o trabajar colaborativamente con un equipo de trabajo.
- Puede integrarse con mas aplicaciones.
- Tiene mas tipos de blockes para agregar (Text, Inputs, Other[Chart])

> Tip de ayuda:
> DeepNote es “gratis” (Tiene sus limitaciones, pero es equivalente al Pro Plan) para estudiantes, claro con un correo

***Utilizar DeepNote***

- Deepnote es un servicio en la nube basado en Jupyter Notebooks. No requiere configuración previa y tiene un trabajo a nivel de **proyecto**. Tiene colaboración en tiempo real, integración con múltiples Apps y tiene acceso a una terminal o línea de comandos integrada 😎.
- Tiene también **variables de entorno** y permite publicar proyectos (para construir portafolio). 🎉
- Podemos correr y agregar lo mismo que en Colab, pero además podemos subir archivos que se quedan siempre en el proyecto. → Es un Colab con asteroides
- Permite previsualizar los archivos CSV de manera muy bonita 😄.
- Parte de lo poderoso de Deepnote es que podemos integrar muchas cosas 🔥.
- No solo podemos agregar celdas de código y de texto, si no que en la opción de *Bloque* vienen muchos más tipos, como input, chart, dataframe sql, etc 🤯. Puede crear gráficas de manera automática sin código!
- Para acceder a los atajos de teclado usamos *`Ctrl + i`*.
- También es importante resaltar que tenemos una terminal integrada 🤖.

[![img](https://www.google.com/s2/favicons?domain=https://deepnote.com//static/apple-touch-icon.png)Deepnote - Data science notebook for teams](https://deepnote.com/)

# Cuarta Semana

## Tutorial: primeros pasos con NumPy

![numpy-1-1-.png](https://static.platzi.com/media/user_upload/numpy-1-1--6dffad86-a55f-4d6b-8b6d-cc8ed040bd6a.jpg)

¡Te doy la bienvenida a este tutorial!

> **Instrucciones iniciales:** para seguir este tutorial ve a [Deepnote ](https://deepnote.com/)y crea una nueva notebook. Allí escribe y ejecuta el código que se te muestre aquí para que lo pongas en práctica.

> Te comparto [una notebook](https://deepnote.com/project/Tutoriales-Datacademy-7caL-o30R6SGIoYOu5Mf6Q/%2FTutorial NumPy.ipynb) donde encontrarás todo el contenido de este tutorial para que puedas consultarlo si tienes dudas y lo tengas a la mano cuando lo necesites. Crea una copia para usarlo desde tu cuenta de Deepnote. Descárgala [aquí](https://deepnote.com/project/Tutoriales-Datacademy-7caL-o30R6SGIoYOu5Mf6Q/%2FTutorial NumPy.ipynb).

Muy bien, comencemos.

### ¿Qué es NumPy?

NumPy o **Num**erical **Py**thon es una librería para manejo de números de Python.

Su principal uso es al manejar estructuras de datos matriciales y funciones matemáticas de alto nivel.

Es muy utilizada en data science y en el mundo científico y matemático.

### ¿Por qué usar NumPy?

Una de sus ventajas es que tiene una estructura de datos que permite representar información compleja como archivos de imágenes, videos, sonidos, etc. NumPy permite trabajar con estos datos de forma númerica para que puedan ser analizados y usados en algoritmos de machine learning.

NumPy también utiliza estructuras de datos como sus arrays que ocupan menor espacio que una lista común en Python. Esto ayuda a que sean más rápidas de procesar y que podamos utilizar cantidades de datos más grandes.

### Comenzar a usar NumPy

Para empezar a utilizar NumPy dentro de Jupyter Notebooks en la nube, como Google Colab o Deepnote, no necesitas hace ninguna instalación. Simplemente escribe la siguiente línea en una celda de código y ejecútala.

```python
import numpy as np
```

### Arrays de NumPy

Como vimos arriba, en NumPy usamos una estructura de datos llamada **array**. Estos son arreglos numéricos parecidos a listas en Python. De hecho tienen propiedas muy parecidas como ser mutables y poder hacer slicing.

Los arrays son **n-dimensionales**. Esto quiere decir que pueden ser de muchas dimensiones dependiendo si tienen varias filas y columnas.

- Un array **unidimensional** es una sola columna o fila de alguna tablar, justo como una lista de Python
- Un array **bidimensional** es lo que conocemos como matriz y es la forma en la que podemos representar tablas de datos en NumPy.
- Un array de **tres dimensiones o más** es una matriz de matrices, mejor conocida como tensor.

![arrays.png](https://static.platzi.com/media/user_upload/arrays-a314b21d-5262-49a3-991f-1a3f82f31982.jpg)

Una característica muy importante de cualquier array es que solo puede tener **datos de un mismo tipo**.

Es decir que si tiene números enteros, todos sus elementos deben ser enteros. Si son números flotantes todos deberán ser flotantes.

Para crear un array con NumPy utilizamos el método `np.array(tu_lista)`.

**Nota:** dentro de los paréntesis puede ir más de una lista para así crear arrays de dos o más dimensiones.

```python
vector = np.array([1, 2, 4, 8, 16])
print(vector)
>>> [ 1  2  4  8 16]

matrix = np.array([[5, 10], [15, 30]])
print(matrix)
>>> [[ 5 10]
 [15 30]]
```

Dentro del método `np.array()` también pueden ir variables que contengan las listas con las que crearemos los arrays.

```python
lista_1 = [100, 200, 300, 400]
lista_2 = [3, 4, 5, 6]

vector_2 = np.array(lista_1)
print(vector_2)
>>> [100 200 300 400]

matrix_2 = np.array([lista_1, lista_2])
print(matrix_2)
>>> [[100 200 300 400]
 [  3   4   5   6]]
```

Por último, utilicemos el método `type()` de Python sobre nuestros arrays para ver que efectivamente son `ndarray` de NumPy.

```python
type(vector)
>>> numpy.ndarray
```

### Reto: practica crear nuevos arrays

Dentro de la notebook en la que estás trabajando crea dos nuevos vectores y dos nuevas matrices.

Puedes utilizar variables o listas directamente en ellas.

### Tamaño de los arrays

Dentro de NumPy también existen funciones que nos sirven para conocer las dimensiones de un array y su tipo de datos.

### .shape

`.shape` nos da las dimensiones del array.

```python
vector.shape
>>> (5,)

matrix.shape
>>> (2, 2)

matrix_2.shape
>>> (2, 4)
```

- Para vectores el único valor es la cantidad de elementos en él.
- Para matrices el primer valor son las filas y el segundo las columnas.
- Para un tensor el primer valor es la “profundidad”, el segundo las filas y el tercero las columnas.

### .dtype

`.dtype` devuelve el tipo de datos que hay en un array.

```python
vector.dtype
>>> dtype('int64')

matrix.dtype
>>> dtype('int64')

vector_4 = np.array([1.4, 5.6, 6.8])
vector_4.dtype
>>> dtype('float64')
```

### Reto: conoce la forma de tus arrays

Nuevamente crea nuevos vectores y matrices dentro de tu notebook.

Utiliza los métodos `.shape` para conocer su forma y `.dtype` conocer su tipo de dato.

Comparte tus resultados en los comentarios de la clase.

------

Con esto hemos llegado al final de esta primera interacción con la librería de NumPy.

## Tutorial: primeros pasos con Pandas

![pandas.png](https://static.platzi.com/media/user_upload/pandas-d92f9b77-a350-4e11-bc9d-dc1d7318c71c.jpg)

¡Te doy la bienvenida a este nuevo tutorial!

Este tutorial de Pandas lo encontrarás dividido en tres clases donde paso a paso comenzarás a utilizar esta librería de Python.

> Instrucciones iniciales: para seguir este tutorial ve a [Deepnote ](https://deepnote.com/)y crea una nueva notebook. Allí escribe y ejecuta el código que se te muestre aquí para que lo pongas en práctica.

> Te comparto una [notebook ](https://deepnote.com/project/Tutoriales-Datacademy-7caL-o30R6SGIoYOu5Mf6Q/%2FTutorial Pandas.ipynb)donde encontrarás todo el contenido de este tutorial para que puedas consultarlo si tienes dudas y lo tengas a la mano cuando lo necesites. Crea una copia para usarlo desde tu cuenta de Deepnote. Descárgala [aquí](https://deepnote.com/project/Tutoriales-Datacademy-7caL-o30R6SGIoYOu5Mf6Q/%2FTutorial Pandas.ipynb).

Ahora sí, comencemos con Pandas. 🐼

### ¿Qué es Pandas?

Pandas es una librería de Python que se usa para manipular datos de alto nivel. Está construida sobre NumPy.

Principalmente funciona con una estructura de datos que guarda la información de manera tabular llamada **DataFrame**. Esencialmente esto es una tabla como las que veríamos en bases de datos SQL o archivos de Excel.

### ¿Por qué usar Pandas?

Para analizar datos es muy común tener que trabajar con ellos en forma tabular y con Pandas podemos hacerlo.

Una diferencia que tiene Pandas a trabajar con hojas de cálculo en Excel es que es mucho más rápida y potente, podemos trabajar con cantidades de datos más grandes.

### Comenzar a usar Pandas

Para empezar a utilizar Pandas dentro de Jupyter Notebooks en la nube, como Google Colab o Deepnote, no necesitas hacer ninguna instalación. Simplemente escribe la siguiente línea en una celda de código y ejecútala.

```python
import pandas as pd
```

### DataFrames y Series

Pandas trabaja con **DataFrames** que son la estructura de datos principal con la que almacena y manipula datos tabulados.

Cada fila dentro de un DataFrame lleva un índice que indica su posición.

![dataframe.PNG](https://static.platzi.com/media/user_upload/dataframe-8bf1861a-6a28-4c36-a625-f22efcb4b02a.jpg)

Otras estructuras de datos comunes en Pandas son las **Series**. Son de una sola dimensión como una lista de Python o un NumPy array. Esencialmente son una columna de datos y un índice que indica su posición.

![series.PNG](https://static.platzi.com/media/user_upload/series-8fbedd52-6bb6-484d-bc93-78882156df42.jpg)

### Crear un DataFrame

Existen diferentes maneras de crear DataFrames. Veamos algunas.

### Diccionario de listas

```python
students_dict = {
    "name": ["Miguel", "Juan David", "Carmen", "Facundo", "Romina"],
    "age": [29, 19, 24, 22, 25],
    "career path":  ["Data Analyst", "Data Scientist", "Data Analyst", "Data Engineer", "ML Engineer" ],
}

students_df = pd.DataFrame(students_dict)
students_df
```

Esto nos creará este DataFrame.

![df1.PNG](https://static.platzi.com/media/user_upload/df1-47ddd5d9-7118-4851-b241-9d2df83670d1.jpg)

### Lista de diccionarios (fila por fila)

```python
students_list = [
    {"name": "Miguel", "age": 29, "career path": "Data Analyst"},
    {"name": "Juan David", "age": 19, "career path": "Data Scientist"},
    {"name": "Carmen", "age": 24, "career path": "Data Analyst"},
]

students_df_2 = pd.DataFrame(students_list)
students_df_2
```

Esto nos creará este segundo DataFrame.

![df2.PNG](https://static.platzi.com/media/user_upload/df2-45109c16-32b9-41ee-9d3a-ec5dd1f135db.jpg)

Utilizando el método `.dtype` podemos conocer cuál es el tipo de dato que hay dentro de cada columna del DataFrame.

Utiliza ese método en cada uno de los DataFrames que creamos.

```python
students_df.dtypes
>>> name           object
age             int64
career path    object
dtype: object

students_df_2.dtypes
>>> name           object
age             int64
career path    object
dtype: object
```

### Reto: crea nuevos DataFrames

1. Crea otro diccionario con listas y con él crea un nuevo DataFrame. Utiliza los datos que quieras.
2. También crea otra lista con diccionarios y crea otro nuevo DataFrame con los mismos datos que tenías en tu diccionario.

## Tutorial: cargar archivos CSV con Pandas

### Importar datos de archivos con Pandas

En Pandas es común importar datos de archivos CSV, JSON, Excel y SQL para crear DataFrames con ellos.

Para esto utilizamos el método `pd.read_{file_type}('path')`. Donde `file_type` es el tipo de archivo que puede ser CSV. `'path'` se refiere a la dirección que tiene el archivo desde la computadora local o dentro del entorno en la nube que utilicemos.

### Importar datos desde CSV con Pandas

Conozcamos la forma más común de crear DataFrames con archivos CSV.

Primero descarga [aquí](https://static.platzi.com/media/public/uploads/studentsperformance_f29e92e5-811f-4cf9-9504-91644b0ecc35.csv) un dataset de desempeño de estudiantes en exámenes. Este dataset es un archivo CSV que viene de este [proyecto en Kaggle](https://www.kaggle.com/spscientist/students-performance-in-exams).

Una vez que tengas descargado el archivo CSV, súbelo dentro de tu proyecto en Deepnote para que puedas utilizarlo. Como recordaras, esto se hace en la zona izquierda dentro de Deepnote y deberá verse similar a esto:

![subircsvdeepnote.PNG](https://static.platzi.com/media/user_upload/subircsvdeepnote-000b387d-7068-49ba-857b-c9abf0460af8.jpg)

Ya que tengas el archivo cargado en tu proyecto de Deepnote, copia su dirección como se muestra a continuación dando clic en los tres puntos del archivo y seleccionando la opción **Copy path to clipboard**:

![copiarpath.PNG](https://static.platzi.com/media/user_upload/copiarpath-88ad7cb1-3d70-4b92-9dd0-1150dd4cecee.jpg)

Este path pégalo dentro del método `pd.read_csv('path')` como se muestra a continuación:

```python
df = pd.read_csv('/work/StudentsPerformance.csv')
df
```

Esto nos creará el siguiente DataFrame.

![df_students.PNG](https://static.platzi.com/media/user_upload/df_students-f9331b82-e5f8-451a-8aac-771cabaa07a0.jpg)

Excelente, has creado tu primer DataFrame desde un archivo CSV. 💪

Para conocer qué hay dentro del DataFrame solemos utilizar el método `.head()`.

```python
df.head()
```

Esto nos mostrará lo siguiente:

![df_head.PNG](https://static.platzi.com/media/user_upload/df_head-67d86be7-1543-4975-a17a-e4f5b605ceeb.jpg)

Este método nos permite conocer las 5 primeras filas del DataFrame y nos da una mirada inicial a lo que hay dentro de él.

## Tutorial: inspección de DataFrames con Pandas

### Explorar un DataFrame

Para analizar datos dentro de un DataFrame primero debemos hacer una inspección. Esto nos sirve para conocer su forma, tener algunas estadísticas descriptivas, ver sus últimos o primeros registros, etc.

Para ello existen métodos de Pandas que conoceremos a continuación.

### .head()

`df.head()` lo conociste en la clase anterior y retorna las 5 primeras filas. También podemos darle un entero como parámetro y devolverá esa cantidad de primeras filas dentro del notebook.

```python
df.head(10)
```

Esto nos muestra lo siguiente:

![head10.PNG](https://static.platzi.com/media/user_upload/head10-83fc2118-41a4-4096-923d-978ad27ceef7.jpg)

### .tail()

`df.tail()` retorna las 5 últimas filas. También podemos darle un entero como parámetro y devolverá esa cantidad de últimas filas.

```python
df.tail()
```

Esto nos muestra lo siguiente:

![tail.PNG](https://static.platzi.com/media/user_upload/tail-72e24bb6-9c12-417d-980c-37408090eef0.jpg)

### .sample()

`df.sample()` retorna filas como los dos métodos anteriores, pero toma muestras al azar del DataFrame y es necesario espeficiar en el parámetro la cantidad de filas.

```python
df.sample(7)
```

![sample.PNG](https://static.platzi.com/media/user_upload/sample-ed87ea9a-31f4-4620-9e0b-1f9415259500.jpg)

### .shape

`df.shape` retorna la cantidad de filas y columnas que tiene el DataFrame en ese orden.

```python
df.shape
>>> (1000, 8)
```

### .size

`df.size` multiplica las filas y columnas y te da el total de datos del DataFrame.

```python
df.size
>>> 8000
```

### .info()

`df.info()` da la cuenta de valores no nulos, el tipo de dato de cada columna (recuerda que solo puede haber un único tipo de dato por columna) y el uso de memoria.

```python
df.info()
```

Esto nos muestra lo siguiente:

![info.PNG](https://static.platzi.com/media/user_upload/info-1a515d4b-f25a-4052-b797-16739c5d1e83.jpg)

### .describe()

`df.describe()` calcula algunas estadísticas descriptivas para cada columna.

```python
df.describe()
```

Esto nos muestra lo siguiente:

![describe.PNG](https://static.platzi.com/media/user_upload/describe-ccc65b3f-d85d-4568-b77e-dd89f5ebc094.jpg)

### Seleccionar datos de una sola columna

Para trabajar con datos de una sola columna utilizaremos código como `df['nombre_columna']`.

Para ver las columnas de un DafaFrame usamos el método `df.columns`.

```python
df.columns
>>> Index(['gender', 'race/ethnicity', 'parental level of education', 'lunch',
       'test preparation course', 'math score', 'reading score',
       'writing score'],
      dtype='object')
```

Usemos la columna `'math score'` y veamos sus datos.

```python
df['math score']
```

Esto nos muestra lo siguiente:

![column_math.PNG](https://static.platzi.com/media/user_upload/column_math-20ebe73f-f1d6-4694-a779-0f20a847da2a.jpg)

Sobre esta columna podemos usar métodos donde uno a uno podemos ver estadísticas descriptivas solamente de esa columna.

```python
df['math score'].mean()
>>> 66.089

df['math score'].median()
>>> 66.0

df['math score'].std()
>>> 15.16308009600945
```

Más adelante en las clases de estadística descriptiva profundizaremos en estos métodos y su aplicación.

## Tutorial: primeros pasos con Matplotlib

### ¿Qué es Matplotlib?

Matplotlib es una librería de Python para crear visualizaciones estáticas, animadas e interactivas. Con Matplotlib es sencillo comenzar a graficar tus datos para analizarlos de manera visual.

### ¿Por qué usar Matplotlib?

Al realizar análisis de datos necesitas visualizarlos. Matplotlib es la manera más sencilla y rápida de hacerlo cuando estás explorando tus datos. Con Matplotlib podrás crear esas gráficas dentro de tus Jupyter Notebooks e incluso compartirlas cargadas dentro de ellas.

### Comenzar a usar Matplotlib

Para empezar a utilizar Matplotlib dentro de Jupyter Notebooks en la nube, como Google Colab o Deepnote, no necesitas hacer ninguna instalación. Simplemente escribe la siguiente línea en una celda de código y ejecútala.

```python
import matplotlib.pyplot as plt
```

### Gráficas de líneas

La manera más sencilla de empezar a utilizar Matplotlib es creando una gráfica de línea.

Para ello necesitaremos de una variable `x` y otra `y`. Las crearemos en dos listas que representan años en el eje de las x e ingresos en las y.

```python
x = [2015, 2016, 2017, 2018, 2019, 2020, 2021]
y = [1, 2, 5, 10, 21, 40, 54]
```

Ya que tenemos nuestros datos, graficamos estas dos variables con el método `plt.plot(x, y)` para crear nuestra gráfica de línea y con el método `plt.show()` que siempre usaremos para mostrar nuestra gráfica.

```python
plt.plot(x, y)

plt.show()
```

![gráfica1.PNG](https://static.platzi.com/media/user_upload/gr%C3%A1fica1-e89d7054-04e7-483e-a7af-c60d4c8f555c.jpg)

A nuestra gráfica podemos agregarle más detalles para mejorar la visualización de los datos.

En este caso agregaremos lo siguiente:

- `marker` para marcar cada punto de dato…
- `linestyle` que define el estilo de línea.
- `color` para definir el color de la línea.

```python
plt.plot(x, y, marker='o', linestyle='--', color='g')

plt.show()
```

![grafica2.PNG](https://static.platzi.com/media/user_upload/grafica2-71b8e3e0-b21e-4724-b76a-9764ad7196fe.jpg)

Con los métodos `plt.xlabel()` y `plt.ylabel()` le agregamos nombre a cada uno de los ejes.

Con `plt.title()` se indica el título de la gráfica.

Para ello dentro de los paréntesis se escribe un string con ese texto, como se ve a continuación.

```python
plt.plot(x, y, marker='o', linestyle='--', color='g')
plt.xlabel('Years')
plt.ylabel('Revenue')
plt.title('Revenue per year')

plt.show()
```

![grafica3.PNG](https://static.platzi.com/media/user_upload/grafica3-42060380-b54f-449d-b59e-5c2a1d89c226.jpg)

Listo, has creado tu primera gráfica con Matplotlib.

Pero esta es un solo tipo. Conozcamos algunas más.

### Gráfica de barras - Bar plot

Un segundo tipo de gráficas que se puede crear con Matplotlib son las gráficas de barras.

Nuevamente crearemos una variable `x` que representará áreas de trabajo en una empresa multinacional y otra variable `y` que indica la cantidad de empleados de esa área.

Finalmente usamos el método `plt.bar(x,y)` para crear esta gráfica.

```python
x = ['Data Science', 'Web Development', 'Mobile Development']
y = [400, 500, 250]

plt.bar(x, y)
plt.show()
```

![grafica4.PNG](https://static.platzi.com/media/user_upload/grafica4-60a72efd-e8ac-4568-aa70-91fc32bad33e.jpg)

### Histograma

Otro tipo de gráfica muy usado son los histogramas.

Para nuestro ejemplo creamos una variable `prices` con precios de algún producto. También creamos los `bins` que son los contenedores o bloques en los que separaremos los datos de nuestra variable a graficar.

Usamos el método `plt.hist(x, bins)` para crear esta gráfica.

```python
prices = [1000, 2300, 3001, 3450, 4200, 4780, 5100, 5500, 6000, 7500]
bins = [1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000]

plt.hist(prices, bins)
plt.show()
```

![grafica5.PNG](https://static.platzi.com/media/user_upload/grafica5-33a4d62e-e427-407a-ae95-e771b7b12864.jpg)

### Gráfica de dispersión - Scatter plot

Las gráficas de dispersión nos sirven para comparar dos variables y ver si tienen alguna relación entre ellas.

Para este ejemplo repetiremos nuestra variables `x`y `y` de los años e ingresos en una compañía.

Usamos el método `plt.scatter(x, y)` para crear este tipo de gráfica.

```python
x = [2015, 2016, 2017, 2018, 2019, 2020, 2021]
y = [1, 2, 5, 10, 21, 40, 54]

plt.scatter(x, y)

plt.show()
```

![grafica6.PNG](https://static.platzi.com/media/user_upload/grafica6-592ad9ce-f1ea-4897-aacc-f2cd82df6dc2.jpg)

### Gráfica con Pandas

Hasta este punto hemos creado variables con datos para graficar, sin embargo, esta no es la manera en la que comúnmente usaremos Matplotlib. Lo que buscaremos hacer será graficar datos provenientes de algún dataset con el que estemos trabajando.

Para demostrar esto usaremos el mismo dataset de desempeño de estudiantes que usamos en las clases de Pandas.

Importa Pandas y carga el dataset como se muestra a continuación.

```python
import pandas as pd
df = pd.read_csv('/work/StudentsPerformance.csv')
df.head()
```

Esto nos cargará el dataset y nos mostrará las primeras cinco filas como observamos a continuación.

![dfmatplotlib.PNG](https://static.platzi.com/media/user_upload/dfmatplotlib-ff526c4c-988e-47f9-b8cd-a52f525cbdc5.jpg)

Usamos `df.columns` para tener a la mano los nombres de las columnas del dataset.

```python
df.columns
>>> Index(['gender', 'race/ethnicity', 'parental level of education', 'lunch',
       'test preparation course', 'math score', 'reading score',
       'writing score'],
      dtype='object')
```

La primera gráfica que crearemos será un histograma de las calificaciones de matemáticas, es decir con la columna `math score`.

Crearemos los bins del 0 al 100 en saltos de 10 para separar nuestros datos de esa manera.

```python
bins=[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
plt.hist(df['math score'], bins)
plt.xlabel('Math score')
plt.ylabel('Number of students')

plt.show()
```

![grafica7.PNG](https://static.platzi.com/media/user_upload/grafica7-dfe21b63-1618-4d98-b54c-a7ba4fa2dd2b.jpg)

Lo importante de crear gráficas es encontrar información de valor en nuestros datos de manera sencilla.

Dentro de esta gráfica lo más notable es que la mayoría de los estudiantes tienen una calificación entre 60 y 80 en matemáticas.

¿Qué otro detalle observas? Déjalo en la sección de comentarios.

Para nuestra segunda gráfica crearemos un diagrama de dispersión de las columnas `math score` y `reading score` para ver qué tanto se pueden relacionar.

```python
y = df['math score']
x = df['reading score']

plt.scatter(x, y)
plt.xlabel('Math score')
plt.ylabel('Reading score')

plt.show()
```

![grafica8.PNG](https://static.platzi.com/media/user_upload/grafica8-5a29b1b8-2750-4fd8-82a0-4c5532a7e7ce.jpg)

En esta gráfica podemos ver una mejor aplicación de la gráfica de dispersión. Vemos cómo es que están dispersos los datos y a simple vista se observa que estás dos variables tienen una correlación positiva, es decir que entre más alta sea una, más alta será la otra.

## Estadística descriptiva vs. inferencial

<h5>Estadística descriptiva</h5>

Foco principal de este curso

Se enfoca en resumir un historial de datos.

Una de las desventajas que tiene es que **se puede mentir con estadística**, esto se hace al destacar solo la información o métricas que nos convienen.

Las mediciones o métricas de los datos que nos dan información de ellos se conocen como **estadísticos descriptivos**. un ejemplo de esto es la cantidad de goles de un jugador en un campeonato.

> "Con frecuencia construimos un caso estadístico con datos imperfectos, como resultado hay numerosas razones por las cuales individuos intelectuales respetables pueden no estar de acuerdo sobre los resultados estadísticos."  _**-Naked Statistics (Charles Wheelan). **_

**Ejemplo: **

Resumir historial deportivo de un jugador.

<h5>Estadística inferencial: predecir con datos</h5>

¿Por qué aprender estadística?
Resumir grandes cantidades de información para tomar mejores decisiones.
Responder preguntas con relevancia social.
Reconocer patrones en los datos.

**Ejemplo: **

Predecir desempeño de un jugador de futbol en el futuro.

#### Por qué aprender estadística?

- resumir grandes cantidades de información
- Tomar mejores decisiones (o peores)
- Responder preguntas con relevancia social
- Reconocer patrones en los datos
- Descubrir a quienes usan estas herramientas con fines nefastos.

## Flujo de trabajo en data science 👾

**Primera Etapa:** **Ingesta de datos - Validacion**: transformaciones que se deben hacer a los datos para que queden limpios y adaptados a lo que necesita el modelo para resolver un problema puntual de negocio.

**Segunda Etapa:** **Preparacion - Entrenamiento del modelo**: hacer un analisis exploratorio de los datos con estasdistica descriptiva, entender si hay correlacion entre los datos y hacer posibles reducciones de datos.

**Tercera Etapa: Evaluacion del modelo - Modelo de produccion - Interaccion usuario final.** Se utiliza la probabilidad y la inferencia.

La estadistica descriptiva esta mas asociada a la primera y segunda etapa del flujo. Toma mayor relevancia en La Ingesta y Validacion de los datos asi como tambien en la Preparacion y Entrenamiento del modelo.

![Capture.png](https://static.platzi.com/media/user_upload/Capture-2b53aadf-6a1c-4331-a537-3d0bdd86127f.jpg)

Preprocesamiento : Ingesta de datos y validación

Análisis Exploratorio: Preparación y Entrenamiento de modelo

Probabilidad e Inferencia : Evaluación de modelo

Test de hipóteis: Modelo en producción

## Plan del curso y cómo se involucra la estadística en el flujo de data science

#### Primer bloque del proceso

- Tipos de datos
- Pipeline de procesamiento

Estadísticos para la **Ingesta de datos** y **Procesamiento de datos/ Validación**

#### Segundo bloque del proceso

- Analisis exploratorio
- Estadística descriptiva
- Correlaciones
- Reducción de datos

Estadísticos para la **analítica de datos** y **exploración de datos**

![estadisticas - modelo descrip y diferencia- ruta.png](https://static.platzi.com/media/user_upload/estadisticas%20-%20modelo%20descrip%20y%20diferencia-%20ruta-1c7a1374-7473-4cfa-a02f-a5896c6c4d3e.jpg)

**NUESTRAS DOS FILOSOFÍAS MATEMÁTICAS DESCRIPTIVAS**
Dos casos de uso:

- Uso de la estadística para hacer la ingesta y procesamiento de los datos
- Uso de la estadística para hacer la analítica y aplicación necesaria para pasar a Machine Learning Engineer o sacar valor de los datos

## Tipos de datos

Datos categóricos --> Ordinales (tiene. cierta relación de orden o jerarquía) o nominales
Numéricos --> Discretos (Enteros) o Continuos (Decimales o una variable tipo float)

Los datos se dividen en dos grupos principales:

**datos categóricos:** (genero, categoría de película, método de pago)

- ordinales (existe una relación de orden en entre las categorías)
- nominales

**datos numéricos:** (edad, altura, temperatura)

- discretos (datos enteros como la edad)
- continuos (datos flotantes como la altura o la temperatura)

![12.jpg](https://static.platzi.com/media/user_upload/12-1044fa29-c2a6-423b-a7dd-5cf17b0798d4.jpg)

conjunto completo de estadisticos descriptivos

df.describe() es de gran ayuda en esta grafica que nos genera al ponenre df.describe() nos da una información super detallada y fácil de comprender

[![img](https://www.google.com/s2/favicons?domain=https://deepnote.com//static/apple-touch-icon.png)Deepnote - Data science notebook for teams](https://deepnote.com/)

[![img](https://www.google.com/s2/favicons?domain=https://www.kaggle.com/lepchenkov/usedcarscatalog/static/images/favicon.ico)Used-cars-catalog | Kaggle](https://www.kaggle.com/lepchenkov/usedcarscatalog)

[![img](https://www.google.com/s2/favicons?domain=https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.describe.html../../_static/favicon.ico)pandas.DataFrame.describe — pandas 1.4.0 documentation](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.describe.html)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)Qué es Deepnote y cómo usarlo en Data Science](https://platzi.com/blog/deepnote/)

# Quinta Semana

## Medidas de tendencia central

Las medidas de tendencia central son una forma de resumir nuestra información.

Las medidas de tendencia central nos entregan información respecto a alrededor de qué puntos (números) se encuentran distribuidos nuestros datos. La desventaja principal de las medidas de tendencia central es que pueden ser afectadas por los puntos extremos o valores atípicos.

Las medidas de tendencia central principales son:

- Media (promedio)
- Mediana (El dato central o de la mitad de la muestra)
- Moda (el dato que más se repite, el más común)

Media --> Noción centralizada de los datos. Se puede llegar a ver afectada por algún dato atípico.
Mediana --> Dato que esta en la mitad. Tener en cuenta si el conjunto de datos esta en la mitad o no.
Moda --> Dato que mas se repite.(No aplica para variables continuas

## Metáfora de Bill Gates en un bar

La metáfora de Bill Gates es muy utilizada en conceptos de evaluación social, y para evidenciar los problemas de evaluar el avance o bienestar social a través de medidas como el PIB Per Cápita.

En la metáfora descubrimos que los puntos atípicos pueden generar distorsiones (sesgos) importantes sobre estadísticos como la media. Entonces, aprendemos a utilizar otros recursos como la mediana para evitar ser afectados en por esos sesgos.

#### ¿Cómo se calcula la media?

Se suman todos los valores del conjunto de datos y el resultado se divide entre la cantidad de elementos.

![Screenshot 2022-02-14 114837.jpg](https://static.platzi.com/media/user_upload/Screenshot%202022-02-14%20114837-0881b994-8979-470f-a823-139440c208f0.jpg)

Donde **N** representa el número de elementos dentro del dataset.

#### ¿Cómo calcular la mediana?

Hay 2 formas de calcular la mediana que depende si el número de elementos del dataset es par o impar.

- **Par:** se toma a los 2 valores que están en el centro y se calcula su promedio.
  ![Screenshot 2022-02-14 114838.jpg](https://static.platzi.com/media/user_upload/Screenshot%202022-02-14%20114838-b6444111-a65e-49fc-99bc-f89ff26ab6f2.jpg)
- Impar: simplemente se toma al valor que esta exactamente en el centro del dataset.
  ![Screenshot 2022-02-14 114839.jpg](https://static.platzi.com/media/user_upload/Screenshot%202022-02-14%20114839-739326fc-792a-4909-8365-2328a008252f.jpg)

**Nota:** Para calcular la mediana el conjunto de datos tiene que estar ordenado.

![Captura de pantalla 2022-02-15 a las 19.51.27.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202022-02-15%20a%20las%2019.51.27-8a6c9e4b-3d24-4953-a019-ed501e79298d.jpg)

## Medidas de tendencia central en Python

> _“La estadística descriptiva no son solo números, también son visualizaciones”_

![Captura de pantalla de 2022-02-15 19-40-05.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%20de%202022-02-15%2019-40-05-686b5823-bcb9-47fe-9f6d-6269f9ab9cf3.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://seaborn.pydata.org/tutorial/distributions.html../_static/favicon.ico)Visualizing distributions of data — seaborn 0.11.2 documentation](https://seaborn.pydata.org/tutorial/distributions.html)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)Tutorial desde cero para dominar Pandas [Python\]](https://platzi.com/blog/pandas/)

## Medidas de dispersión

#### Dispersion en una distribución

- Rango
- Rango Inter cuartil
- Desviación estándar

Dado el grafico

![cuadrantes.png](https://static.platzi.com/media/user_upload/cuadrantes-156ee8f7-f51b-4555-b750-eeb0cd301ad4.jpg)

En el **histograma** se ve la distribución de datos visualizando la cantidad de datos referente a cada valor y podemos ver que en este caso es una distribución normal.

#### Rango

**El Rango** es la distancia o intervalo entre el valor máximo y el valor mínimo de la distribución, el cual está dado por la resta del valor máximo menos el mínimo siendo este la diferencia.

#### Cuartiles

**La Rango Inter cuartil** son los puntos que divide los datos en 4 partes iguales:

Siendo el cuartil 2 es la Mediana
El cuartil 1 es la mitad entre la mediana y el valor mínimo
El cuartil 3 es la mitad entre la mediana y el valor máximo

Y la distancia entre el cuartil 3 y el cuartil 1 se conoce como rango Inter cuartil (IQR).

Los cuartiles separan los datos en 4 partes iguales

Los cuartiles son valores que dividen una muestra de datos en cuatro partes iguales

**1er cuartil (Q1):** 25% de los datos es menor que o igual a este valor.
**2do cuartil (Q2):** La mediana. 50% de los datos es menor que o igual a este valor.
**3er cuartil (Q3):** 75% de los datos es menor que o igual a este valor.
**Rango Inter cuartil:** La distancia entre el primer 1er cuartil y el 3er cuartil (Q3-Q1); de esta manera, abarca el 50% central de los datos.

#### Diagrama de caja o box plot

representa gráficamente una serie de datos numéricos a través de sus cuartiles. De esta manera, el diagrama de caja muestra a simple vista la mediana y los cuartiles de los datos. También puede representar los valores atípicos de estos.

En la visualización del rango, el rango Inter cuartil y los cuartiles, se muestra en un **diagrama de caja** que es la gráfica por excelencia para mostrar los datos con respecto a **la mediana** en particular.

> _Las medidas de dispersión son iguales a las velas japonesas que se usan en el trading 👾_

## Desviación estándar

La desviación estándar de una variable aleatoria, población estadística, conjunto de datos o distribución de probabilidad es la raíz cuadrada de su varianza. … Además de expresar la variabilidad de una población, la desviación estándar se usa comúnmente para medir la fiabilidad de las conclusiones estadísticas

![desviacion estandar](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Standard_deviation_diagram.svg/450px-Standard_deviation_diagram.svg.png)

Es una medida de dispersion de qué tanto estan separados los datos respecto al valor promedio. En tal sentido, **la Deviacion Estandar** se define como la raiz cuadrada de la varianza.
![desviacion_estandar.png](https://static.platzi.com/media/user_upload/desviacion_estandar-ac03ca39-deb5-48aa-aa9b-ca18dbd52561.jpg)

La varianza se define como la suma de todas las distancias cuadraticas de los datos respecto al valor promedio (miu) entre el numero de datos (n):
![varianza.png](https://static.platzi.com/media/user_upload/varianza-236b76eb-906a-4315-8dbd-87d94340b90e.jpg)

En estadistica se define la Desviacion Estandar de la Poblacion y la **Desviacion Estandar de la Muestra**. En el caso de esta ultima, se emplea un factor de corrección que consiste en reemplazar n por n-1.

**La Distribución Normal** es un tipo de distribucion simetrica conocida como Campana de Gauss que se caracteriza por **el valor de la mediana que coincide con la media.**

**En un diagrama de caja** los brazos extremos se ajustan de manera que excluyan los valores anomalos conocidos como outliers.

**La teoria de la distribucion normal** nos indica que existe un rango que contempla el 99.72% de la totalidad de los datos. El extremo superior de dicho rango se calcula al sumar 3 veces la desviacion estandar al valor promedio y el extremo inferior se calcula al restar 3 veces la desviacion estandar al promedio.
![t_distribucion_normal.png](https://static.platzi.com/media/user_upload/t_distribucion_normal-4af1bb9a-e685-4158-a438-e6f170d6ada6.jpg)

Los datos fuera de ese rango ya no corresponde a la distribucion normal ni contribuyen a la misma. Se consideran pequeñas contribuciones de otra distribucion **(outliers)** que estan muy lejos del promedio.

Para el caso del diagrama de caja, podemos **ajustar los brazos** al **recalcular los valores de q1 y q3**. El minimo y maximo del nuevo rango se calcula como sigue:

*min* = q1 - 1.5IQR

*max* = q3 + 1.5IQR

Donde IQR es el Rango Inter Cuartil. Los datos fuera de la nueva barra calculada se descartan ya que corresponden a datos anomalos. Este se conoce como el **Metodo de Deteccion de Outliers con el Rango Inter Cuartil**
![Rango_intercuartil.png](https://static.platzi.com/media/user_upload/Rango_intercuartil-524fd8a3-f542-4241-b2e1-0ba06c9819ed.jpg)

Los valores *min y max* corresponden con el rango de la desviacion estandar para la Distribucion Normal por lo que contempla la mayoria de los datos.

**Cuando la distribucion no es simetrica**,es decir, los cuartiles no estan separados de manera uniforme y el promedio no coincide con la mediana, **no aplica el calculo de min y max como lo sugiere el Metodo de Deteccion de Outliers**. En ese caso se procede como sigue:

*min* = q1 - 1.5f(IQR)

*max* = q3 + 1.5g(IQR)

donde f y g son funciones que representan el cesgo de la distribucion asimetrica y que permiten calcular la barra en el diagrama de caja que se ajuste a la distribucion real.
![Rango_intercuartil2.png](https://static.platzi.com/media/user_upload/Rango_intercuartil2-716038d1-5cf8-4465-9e40-5d41141e6fff.jpg)

La desviacion estandar tiene mas sentido en una distribucion simetrica ya que se basa en la distancia cuadratica de los diferentes puntos respecto al promedio lo cual nos hace perder informacion de si la distribucion tiene un cesgo hacia la izquierda o la derecha. En tal caso, es preferible trabajar con los cuartiles ya que si no estan distribuidos de manera uniforme nos indica que la distribucion tiene un cesgo y podemos calcular el rango que contiene la mayor parte de los datos (99%) y que coincide con el rango de la desviacion estandar.

## Medidas de dispersión en Python

La mediana también se puede calcular con:

```python
Q2 = df['price_usd'].quantile(q=0.5)
```

> *Los diagramas de caja permiten una mejor visualizacion de los datos de tipo categorico*

![boxplot_cars.PNG](https://static.platzi.com/media/user_upload/boxplot_cars-a6d0bbe6-e165-4e80-81a1-c8bb15e95856.jpg)

## Exploración visual de los datos

> ***No se trata solamente de tener una imagen de los datos, sino una buena imagen***

![data-visualization.jpg](https://static.platzi.com/media/user_upload/data-visualization-284ee65e-f97f-4dd2-865f-d43180938b6c.jpg)

![Data-Visualization_Featured-Image-1.png](https://static.platzi.com/media/user_upload/Data-Visualization_Featured-Image-1-ccee59cb-04e9-4b6e-a26b-2eb37ae4c7a8.jpg)

>  "Una imagen vale más que mil palabras. Pero una buena imagen vale mucho más."

Una imagen vale más que mil palabras, en el caso de la data no cualquier imagen puede expresar con claridad lo que los números nos quieren decir, es por esto que es necesario evaluar cuál es la visualización perfecta para representar los datos y la página datavizproject tendrás una muestra de diversas visualizaciones y ejemplos de uso.

[![img](https://www.google.com/s2/favicons?domain=https://datavizproject.com//apple-touch-icon-57x57.png)Data Viz Project | Collection of data visualizations to get inspired and finding the right type.](https://datavizproject.com/)

## Diagramas de dispersión en el análisis de datos

 Título al gráfico utilizando seaborn, entonces descubrí lo siguiente:

- Es necesario guardar el gráfico de seaborn en una variable.
- Aplicar a la variable creada el método ***.set_title(“TITULO”)***

**Comparto mi visualización:**

![Captura de pantalla 2022-02-14 104745.jpg](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202022-02-14%20104745-be40e7e0-0f53-44fd-b102-e5fab531c880.jpg)

>  Diagrama de dispersión es una manera de visualizar todos los datos en el mismo espacio.

1.- Promedio de la variable ***sepal.length\*** y el diagrama de Barra de ***sepal.length\*** vs ***variety\***![diagrama_barra.png](https://static.platzi.com/media/user_upload/diagrama_barra-53587848-d03f-4806-a472-0a07cbd3dfcc.jpg)
Podemos notar que el valor del promedio esta dentro del rango que muestra la Grafica de barra el cual va de 5 a 6.5 aproximadamente.

------

2.- Diagrama de Dispersion de la variable ancho del sepalo (***sepal.width\***) vs largo del petalo (***petal.length\***)
![diagrama_dispersion.png](https://static.platzi.com/media/user_upload/diagrama_dispersion-d4d610c7-586c-455f-8ce0-a88de02a69f5.jpg)
**

**Este grafico muestra claramente que la variedad **Setosa** se caracteriza por tener una longitud del petalo diferenciable del resto (va de 0 hasta aproximadamente 2). Mientras que las otras dos variedades poseen una mayor longitud de petalo (de 2.8 aproximadamente hasta 7)

[![img](https://www.google.com/s2/favicons?domain=http://seaborn.pydata.org/generated/seaborn.scatterplot.html../_static/favicon.ico)seaborn.scatterplot — seaborn 0.11.2 documentation](http://seaborn.pydata.org/generated/seaborn.scatterplot.html)

[![img](https://www.google.com/s2/favicons?domain=http://seaborn.pydata.org/generated/seaborn.boxplot.html../_static/favicon.ico)seaborn.boxplot — seaborn 0.11.2 documentation](http://seaborn.pydata.org/generated/seaborn.boxplot.html)

[![img](https://www.google.com/s2/favicons?domain=http://seaborn.pydata.org/generated/seaborn.jointplot.html../_static/favicon.ico)seaborn.jointplot — seaborn 0.11.2 documentation](http://seaborn.pydata.org/generated/seaborn.jointplot.html)

[![img](https://www.google.com/s2/favicons?domain=http://seaborn.pydata.org/generated/seaborn.barplot.html../_static/favicon.ico)seaborn.barplot — seaborn 0.11.2 documentation](http://seaborn.pydata.org/generated/seaborn.barplot.html)

## Crea tu proyecto del reto: análisis de datos de retail

### 0. Contexto: tu trabajo como analista de datos en retail

Imagina que acabas de iniciar tu primer trabajo como Data Analyst para una nueva empresa de retail en Estados Unidos. Managers de ventas quieren conocer ciertos aspectos de otras empresas y te piden analizar datos de otras 25 compañías muy exitosas en el país.

![pexels-angela-roma-7319110.jpg](https://static.platzi.com/media/user_upload/pexels-angela-roma-7319110-24512d80-3258-4bc6-a660-b15205ee356f.jpg)
*Photo por Angela Roma de [Pexels](https://www.pexels.com/photo/similar-white-and-red-shopping-bags-on-brick-counter-7319110/)*

Como dato, [retail](https://es.wikipedia.org/wiki/Retail) es el sector económico de empresas que se especializan en comercialización masiva de productos o servicios uniformes a grandes cantidades de clientes. Por ejemplo Walmart, Amazon, Target, Home Depot, Best Buy, etc.

### 1. Descarga los datos

Trabajaremos con un pequeño dataset que justamente tiene los datos de esas 25 compañías. Este dataset es una versión simplificada del dataset [Largest US Retailers](https://www.kaggle.com/yamqwe/largest-us-retailers-2015e) de [Gary Hoover](https://data.world/garyhoov).

⬇ Para descargar esta versión simplificada del dataset en CSV haz clic [aquí](https://static.platzi.com/media/public/uploads/largest_us_retailers_9b00dc73-a938-46cd-af17-fcb2bd67301f.csv). ⬇

**Importante:** utiliza esa versión simplificada del dataset.

Dentro de este dataset encontrarás las siguientes variables/columnas:

- Company (compañía)
- Sales (ventas en millones de dólares)
- Stores (cantidad de tiendas físicas)
- Sales/Avg. Store (promedio de venta por tienda física)
- Category (categoría)

### 2. Notebook en Deepnote

Para crear tu proyecto es necesario utilizar Deepnote, ya que nos facilitará compartirlo con el mundo y entregarlo.

1. Ve a [Deepnote](https://deepnote.com/) y crea un nuevo proyecto dentro de tu espacio de trabajo personal. Llámalo **Proyecto Datacademy**.

![dashboard_deepnote.PNG](https://static.platzi.com/media/user_upload/dashboard_deepnote-dd510fdf-7b69-4afb-9bd9-511d7328d550.jpg)

![new_project.PNG](https://static.platzi.com/media/user_upload/new_project-4aad0c46-d13a-4030-a44a-43ee52f13d0a.jpg)

1. Dentro del proyecto sube [este notebook](https://deepnote.com/project/Tutoriales-Datacademy-7caL-o30R6SGIoYOu5Mf6Q/%2Ftemplate_proyecto_datacademy.ipynb) que incluye este template para resolver el proyecto.

⬇ Descarga el template de notebook [aquí](https://deepnote.com/project/Tutoriales-Datacademy-7caL-o30R6SGIoYOu5Mf6Q/%2Ftemplate_proyecto_datacademy.ipynb) ⬇

1. Dentro de la notebook que subiste a tu proyecto carga el CSV de los retailers que descargaste en el paso 1.

![csv_cargado.PNG](https://static.platzi.com/media/user_upload/csv_cargado-70cf4ee5-8d07-46d7-a530-89c2dfc6b456.jpg)

1. Importa estas librerías en tu notebook:

```python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
```

1. Carga tus datos del CSV con Pandas y pon manos a la obra.

![dataframe_cargado.PNG](https://static.platzi.com/media/user_upload/dataframe_cargado-fe0e665b-8362-40b0-b991-7d617e4d376e.jpg)

### 3. Responde preguntas del negocio

Imagina que los managers te hacen ciertas preguntas clave. Tu labor será responderlas buscando esos insights analizando los datos.

Para ello usa las cuatro librerías que acabas de importar que has conocido a lo largo de las clases del reto.

No existe una única sola forma de llegar al resultado, siéntete libre de explorar los datos como mejor te parezca con estas herramientas. Lo que importa al final es llegar a un resultado que sea de valor para el negocio.

La estructura que seguirás dentro del notebook será la siguiente. Para verla plantemos una pregunta sencilla que es **¿cuál empresa vendió más?**

### a) Escribe tu código dentro de celdas de código

Ejecuta el código que necesites para llegar al resultado que necesitas.

Por ejemplo, para esta información podemos utilizar el método `df.sort_values('x', ascending=0)`. Servirá para poner hasta arriba del DataFrame la compañía que más ventas tuvo.

```
df_sorted = df.sort_values('Sales', ascending=0)
df_sorted
```

Esto nos entregará el DataFrame de esta manera:

![df_venta_mayor.PNG](https://static.platzi.com/media/user_upload/df_venta_mayor-7eb06b66-f419-4fa4-baf7-3e6d4af8538d.jpg)

Claramente, podemos ver que **Walmart US** es el que más ventas ha tenido con **$658,119 millones de dólares**.

Parece que con esto será suficiente, pero una visualización será de mayor valor y será más fácil de interpretar. Vamos a ello.

### b) Comparte insights con visualizaciones

Con ayuda de Matplotlib o Seaborn crea las gráficas que necesites para dar a conocer tu resultado e incluso enriquecerlo.

Para este caso vamos a enriquecerlo mostrando los datos de ventas de las cinco compañías que más vendieron. Lo haremos de la siguiente manera:

```python
x = df_sorted['Company'][0:5] #Aplicamos slicing como en una lista de Python.
y = df_sorted['Sales'][0:5]

plt.bar(x, y)
plt.title('Top 5 retailers')
plt.xlabel('Company')
plt.ylabel('Sales')
plt.xticks(rotation='vertical') #Método que se usa para rotar el texto de los puntos en X para que no se amontonen.
plt.show()
```

Obtendremos la siguiente gráfica:

![top_5_retailers.PNG](https://static.platzi.com/media/user_upload/top_5_retailers-912b4f59-203e-4940-a1a0-d6ebc9659db2.jpg)

Esta gráfica, además de mostrarnos que Walmart US es el que más ventas ha generado, también nos muestra que está dominando el mercado de retail superando al segundo lugar por cinco veces más. Esto es un insight interesante y justo escribiremos nuestras conclusiones en el siguiente paso.

### c) Conclusiones

Debajo de tus resultados escribe conclusiones de lo que necesites resaltar de acuerdo a lo que observes. Utiliza esa intuición de los datos y dominio del negocio. Hazlo en una celda de texto en tu notebook.

![conclusiones.PNG](https://static.platzi.com/media/user_upload/conclusiones-74d55f70-9c89-48f3-9495-0f27bd6e52ad.jpg)

------

Esto lo tendrás que hacer para cada una de las siguientes preguntas:

**1. ¿Cuál es el promedio de ventas sin contar a la compañía dominante?**
Pista: slicing.

**2. ¿Cuánto dinero en ventas generó la mayoría de las compañías?**
Pista: hist.

**3. ¿Cuántas tiendas tiene la mayoría de las compañías?**
Pista: hist.

**4. ¿La cantidad de tiendas está relacionada con la cantidad de ventas? Es decir, ¿si una empresa tiene más tiendas tendrá más dinero de ventas?**
Pista: scatter.

**5. ¿Cuál es el rango que existe entre las ventas?**
Pista: max y min

**6. ¿Cuáles son las 5 empresas que más tiendas físicas tienen? ¿Cuáles de ellas están dentro de las 5 empresas que más ventas tuvieron?**
Pista: pregunta de ejemplo

Nota: puede que en algunos casos una visualización no sea necesaria, pero siempre puedes buscar como enriquecer tu resultado con ella.

Responde estas preguntas dentro de la primera sección de tu notebook.

### 4. Preguntas opcionales

Para elevar la dificultad del proyecto tenemos preguntas opcionales. Para resolverlas es probable que tengas que investigar más métodos de Pandas, NumPy y Matplotlib.

Si decides aceptarlas recuerda que tenemos dos tutoriales que podrán ayudarte:

- [Guía definitiva para dominar Pandas](https://platzi.com/blog/pandas/) 🐼
- [Guía definitiva para dominar NumPy](https://platzi.com/blog/numpy/) ➕
- [Guía definitiva para dominar Matplotlib](https://platzi.com/blog/matplotlib/) 📊

Preguntas:

**7. ¿Qué categoría de compañía generó más ventas?**

**8. ¿Cuál es la compañía que en relación con su cantidad de tiendas físicas genera más ventas?**

**9. ¿Cuáles compañías venden de forma exclusivamente online? ¿Sus ventas destacan sobre las que también tienen tiendas físicas?**

De estas tres preguntas responde las que decidas aceptar dentro de la segunda sección de tu notebook con el mismo formato que las primeras. ¡Espero puedas resolverlas todas! 💪

### 5. Tus propias preguntas

De la misma manera como has resuelto las preguntas anteriores, es momento de hacerle tus propias preguntas a los datos.

Comparte en la última sección de tu notebook de 1 a 3 preguntas de aspectos que te parezcan interesantes y relevantes de los datos.

------

### 🏆 Mentoría para los mejores 10 proyectos

Las personas que entreguen los 10 mejores proyectos participarán en una mentoría con personas expertas en data science del team Platzi.

Aspectos a evaluar:

- Responder la mayor cantidad de preguntas.
- Valor y veracidad de la información encontrada con las preguntas.
- Valor y veracidad de la información que entregues con los resultados de tus propias preguntas.
- Storytelling: visualizaciones fáciles de entender que muestren información valiosa, claridad y valor de tus conclusiones para cada hallazgo.

------

¡Listo, has iniciado el camino para completar este primer proyecto de análisis de datos!

En la siguiente clase conocerás cómo entregar tu proyecto.
