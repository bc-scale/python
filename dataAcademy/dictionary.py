def run():
  mi_dictionary = {
    'llave1': 1,
    'llave2': 2,
    'llave3': 3,
  }
  # print(mi_dictionary['llave1'])
  # print(mi_dictionary['llave2'])
  # print(mi_dictionary['llave3'])

  poblacion_paises = {
    'Argentina': 44938712,
    'Brazil': 210147125,
    'Colombia': 50372424,
  }

  # print(poblacion_paises['Colombia'])

  # for pais in poblacion_paises.keys():
    # print(pais)

  # for pais in poblacion_paises.values():
    # print(pais)

  for pais, poblacion in poblacion_paises.items():
    print(pais + ' tine ' + str(poblacion) + ' habitantes')

if __name__ == '__main__':
  run()